FROM python:3.7
ENV PYTHONUNBUFFERED 1

RUN sed -i 's@deb.debian.org@mirrors.aliyun.com@g' /etc/apt/sources.list \
    && apt-get update \
    && apt-get -y install vim \
        supervisor \
    && mkdir -p /home/project \
    && cd /home/project/ \
    && mkdir static \
    && mkdir media \
    && mkdir media/tmp \
    && rm -r /var/lib/apt/lists/*

WORKDIR /home/project

COPY ./requirements.txt /home/project

RUN pip install -r /home/project/requirements.txt -i https://mirrors.aliyun.com/pypi/simple/ \
    && rm -rf ~./cache/pip \
    && rm -rf /tmp

RUN mkdir -p /tmp/supervisord

COPY ./ /home/project

CMD ["/usr/bin/supervisord"]