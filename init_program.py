import django
import os


def __initialize__():
    """系统初始化"""
    os.environ.setdefault(
        "DJANGO_SETTINGS_MODULE",
        "server.settings"
    )
    django.setup(set_prefix=False)


if __name__ == '__main__':
    __initialize__()
