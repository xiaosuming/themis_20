from django.utils.deprecation import MiddlewareMixin
from apps.chat.models import User


class GrantUserMiddleware(MiddlewareMixin):
    """调试阶段授予请求虚拟用户"""
    def process_request(self, request):
        log_user = User.objects.get(username='admin')
        request.user = log_user