from django.utils.deprecation import MiddlewareMixin
from django.http import JsonResponse
import logging
import traceback
from server.settings import DEBUG


class PrettyExceptionMiddleWare(MiddlewareMixin):
    """以JSON格式返回异常，并追溯错误代码"""
    def process_exception(self, request, exception):
        if DEBUG:
            traceback.print_exc()
        else:
            logger = logging.getLogger(__name__)
            logger.error(traceback.format_exc())

        if hasattr(exception, 'code'):
            data = {
                'code': 1,
                'msg': 'ERROR',
                'details': str(exception.error) if hasattr(exception, 'error') else exception.message
            }
            return JsonResponse(
                status=exception.code,
                data=data,
            )
        else:
            data = {
                'code': 1,
                'msg': 'ERROR',
                'details': str(exception)
            }
            return JsonResponse(
                status=500,
                data=data,
            )
