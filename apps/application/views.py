from django.contrib.auth.models import Group
from rest_framework import viewsets, status
from rest_framework.permissions import BasePermission
from rest_framework.decorators import action
from rest_framework.response import Response
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi
from django.utils.decorators import method_decorator
from django.core.exceptions import ObjectDoesNotExist
from apps.application.serializers import ApplicationSerializer, SystemSerializer, CommentSerializer
from apps.application.models import Application, System, Comment
from apps.oauth.serializers import OAuthAppSerializer
from apps.oauth.models import OAuthApplication
from apps.oauth.utils.rsa_key import generate_key_pair


# @class SystemViewSet 应用系统视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	应用系统视图
#   method:GET, POST, UPDATE, DELETE
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2020/04/12 11:17:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------
class IsDeveloperOrReadOnly(BasePermission):
    SAFE_METHODS = ('GET', 'HEAD', 'OPTIONS')

    def has_permission(self, request, view):
        return bool(
            request.method in self.SAFE_METHODS or
            request.user and
            request.user.is_authenticated
        )


@method_decorator(name='list', decorator=swagger_auto_schema(
    tags=['应用商店-应用系统'], operation_summary='返回用户可见的应用系统(默认10条每页)',
    manual_parameters=[
        openapi.Parameter('name', openapi.IN_QUERY, description='系统名称',
                          type=openapi.TYPE_STRING),
        openapi.Parameter('developer', openapi.IN_QUERY, description='开发者名称',
                          type=openapi.TYPE_STRING),
        openapi.Parameter('limit', openapi.IN_QUERY, description='每页返回的数量',
                          type=openapi.TYPE_INTEGER),
        openapi.Parameter('offset', openapi.IN_QUERY, description='从第几条返回',
                          type=openapi.TYPE_INTEGER),
    ]
))
@method_decorator(name='create', decorator=swagger_auto_schema(
    tags=['应用商店-应用系统'], operation_summary='创建应用系统'
))
@method_decorator(name='retrieve', decorator=swagger_auto_schema(
    tags=['应用商店-应用系统'], operation_summary='返回单个应用系统详情'
))
@method_decorator(name='update', decorator=swagger_auto_schema(
    tags=['应用商店-应用系统'], operation_summary='更新应用系统'
))
@method_decorator(name='partial_update', decorator=swagger_auto_schema(
    tags=['应用商店-应用系统'], operation_summary='更新应用系统指定的属性'
))
@method_decorator(name='destroy', decorator=swagger_auto_schema(
    tags=['应用商店-应用系统'], operation_summary='删除应用系统'
))
class SystemViewSet(viewsets.ModelViewSet):
    queryset = System.objects.all()
    serializer_class = SystemSerializer
    search_field = ('name', 'developer')
    pagination_class = None

    def perform_create(self, serializer):
        login_user = self.request.user
        serializer.save(developer=login_user)


# @class ApplicationViewSet 应用视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	应用视图
#   method:GET, POST, UPDATE, DELETE
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2020/04/12 11:17:22|张志鹏|创建类|完成|
# |2020/04/17 15:31:22|张志鹏|增加应用评论功能|完成|
# -----------------------------------------------------------------------------------------------------
@method_decorator(name='list', decorator=swagger_auto_schema(
    tags=['应用商店-应用'], operation_summary='返回用户可安装/修改的应用(默认10条每页)',
    manual_parameters=[
        openapi.Parameter('name', openapi.IN_QUERY, description='应用名称',
                          type=openapi.TYPE_STRING),
        openapi.Parameter('developer', openapi.IN_QUERY, description='开发者名称',
                          type=openapi.TYPE_STRING),
        openapi.Parameter('limit', openapi.IN_QUERY, description='每页返回的数量',
                          type=openapi.TYPE_INTEGER),
        openapi.Parameter('offset', openapi.IN_QUERY, description='从第几条返回',
                          type=openapi.TYPE_INTEGER),
        openapi.Parameter('ordering', openapi.IN_QUERY, description='排序方式',
                          type=openapi.TYPE_STRING, enum=['installed_cnt', 'avg_score', None]),
    ]
))
@method_decorator(name='create', decorator=swagger_auto_schema(
    tags=['应用商店-应用'], operation_summary='注册应用'
))
@method_decorator(name='retrieve', decorator=swagger_auto_schema(
    tags=['应用商店-应用'], operation_summary='返回单个应用的详情'
))
@method_decorator(name='update', decorator=swagger_auto_schema(
    tags=['应用商店-应用'], operation_summary='更新应用'
))
@method_decorator(name='partial_update', decorator=swagger_auto_schema(
    tags=['应用商店-应用'], operation_summary='更新应用指定的属性'
))
@method_decorator(name='destroy', decorator=swagger_auto_schema(
    tags=['应用商店-应用'], operation_summary='移除应用'
))
class ApplicationViewSet(viewsets.ModelViewSet):
    queryset = Application.objects.all().prefetch_related('oauthapplication')
    serializer_class = ApplicationSerializer
    search_field = ('name', 'developer')
    pagination_class = None

    def get_queryset(self):
        order_type = self.request.GET.get('ordering')
        if order_type == 'installed_cnt':
            app_set = Application.objects.prefetch_related('authorized_apps'). \
                all().order_by('-authorized_apps')
        elif order_type == 'score':
            app_set = Application.objects.all().order_by('-avg_score')
        else:
            app_set = Application.objects.all().order_by('created_date')
        return app_set

    def perform_create(self, serializer):
        login_user = self.request.user
        serializer.save(developer=login_user)

    @swagger_auto_schema(method='post', operation_summary='创建应用评论',
                         tags=['应用商店-应用'], responses={'200': CommentSerializer})
    @swagger_auto_schema(method='delete', operation_summary='删除应用评论',
                         tags=['应用商店-应用'])
    @action(detail=True, methods=['POST', 'DELETE'],
            serializer_class=CommentSerializer)
    # FIXME 需要权限校验
    def comments(self, request, pk, commit_id=None):
        # 获取评论附属的应用
        db_app = self.get_object()
        login_user = request.user
        # 添加评论
        if request.method == 'POST':
            comment_data = {
                "user": login_user,
                "application": db_app,
                "score": request.data.get("score"),
                "content": request.data.get("content")
            }
            db_comment = Comment.objects.create(**comment_data)
            serializer = CommentSerializer(db_comment, context={'request': request})
            return Response(data=serializer.data, status=status.HTTP_200_OK)
        elif request.method == 'DELETE':
            db_app.commit_set.filter(id=commit_id).delete()
            return Response(status=status.HTTP_200_OK)

    @swagger_auto_schema(method='get', operation_summary='获得应用OAuth信息',
                         tags=['应用商店-应用'])
    @action(detail=True, methods=['GET', 'PATCH'],
            serializer_class=OAuthAppSerializer)
    def oauth(self, request, pk):
        """根据应用id获取其oauth注册信息"""
        db_application = self.get_object()
        # try:
        #     db_oauth_app = OAuthApplication.objects.get(application=db_application)
        # except ObjectDoesNotExist:
        #     db_oauth_app = OAuthApplication.o

        db_oauth_app, new_create = OAuthApplication.objects.get_or_create(
            application=db_application)
        if new_create:
            print("新建应用OAuth信息")
            db_oauth_app.public_key, db_oauth_app.private_key = generate_key_pair(db_application.id)
            db_oauth_app.name = db_application.name
            db_oauth_app.save()
        # 获取oauth信息
        if request.method == 'GET':
            serializer = OAuthAppSerializer(db_oauth_app, context={'request': request})
        else:
            data = request.data
            serializer = OAuthAppSerializer(
                db_oauth_app, data=data, partial=True, context={'request': request})
            if serializer.is_valid(raise_exception=True):
                serializer.save()
        return Response(serializer.data)
