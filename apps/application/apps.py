from django.apps import AppConfig


class ApplicationConfig(AppConfig):
    name = 'apps.application'

    def ready(self):
        from django.db.models.signals import post_save, post_delete, pre_delete
        from .signal import install_default_app, cal_avg_score, create_or_update_app_manager, \
            delete_app_manager

        post_save.connect(install_default_app, sender='application.Application',
                          dispatch_uid='install_default_app')

        post_save.connect(create_or_update_app_manager, sender='application.Application',
                          dispatch_uid='create_or_update_app_manager')

        pre_delete.connect(delete_app_manager, sender='application.Application',
                           dispatch_uid='delete_app_manager')

        post_save.connect(cal_avg_score, sender='application.Comment',
                          dispatch_uid='app_comment')
