from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
from functools import partial
from utils.sendfile import return_download_uri
import os
import hashlib


class AppBase(models.Model):
    name = models.CharField(max_length=64, verbose_name='名称', help_text='名称', unique=True)
    introduction = models.TextField(verbose_name='介绍', help_text='介绍')
    developer = models.ForeignKey(to=User, on_delete=models.SET_NULL, null=True,
                                  verbose_name='开发者', help_text='开发者')
    created_date = models.DateTimeField(auto_now_add=True, verbose_name='创建时间', help_text='创建时间')
    update_date = models.DateTimeField(auto_now=True, verbose_name='更新时间', help_text='更新时间')

    class Meta:
        abstract = True

    def __str__(self):
        return self.name


class System(AppBase):
    class Meta:
        verbose_name = '应用系统'
        verbose_name_plural = verbose_name


def hash_file(file, block_size=65536):
    hasher = hashlib.md5()
    for buf in iter(partial(file.read, block_size), b''):
        hasher.update(buf)
    return hasher.hexdigest()


def upload_logo(instance, filename):
    """
    返回应用实例logo的存储路径
    :param instance: Application实例
    :param filename: 文件
    :return: application/logo/_app1.png
    """
    instance.logo.open()
    logo_ext = filename.rsplit('.')[1]
    logo_path = os.path.join('application', 'logo',
                             '{}.{}'.format(hash_file(instance.logo), logo_ext))
    return logo_path


class Application(AppBase):
    english_name = models.CharField(max_length=64, null=True, blank=True,
                                    verbose_name='英文名称', help_text='英文名称')
    entry_url = models.URLField(max_length=256, null=True, verbose_name='入口地址',
                                help_text='入口地址')
    logo = models.FileField(upload_to=upload_logo, max_length=256,
                            verbose_name='应用logo', help_text='应用logo')
    title_pic = models.FileField(upload_to=upload_logo, max_length=256,
                                 verbose_name='应用广告图', help_text='应用广告图')
    use_iframe = models.BooleanField(default=True, verbose_name='是否适用iframe打开')
    allow_resize = models.BooleanField(default=True, verbose_name='是否允许修改iframe大小')
    system = models.ForeignKey(
        to=System, null=True, on_delete=models.SET_NULL, default=None,
        verbose_name='所属系统', help_text='所属系统', related_name='applications')
    is_default = models.BooleanField(default=False, verbose_name='是否默认应用',
                                     help_text='是否默认应用')
    avg_score = models.SmallIntegerField(verbose_name='平均评分', help_text='平均评分',
                                         default=5)
    is_official = models.BooleanField(default=False, verbose_name='是否官方应用',
                                      help_text='是否官方应用')

    def get_app_secret(self):
        return os.path.join(settings.MEDIA_ROOT, 'application', str(self.id), 'secret')

    def get_logo(self):
        logo_url = return_download_uri(self.logo.path)
        return logo_url

    def get_title_pic(self):
        title_pic_url = return_download_uri(self.title_pic.path)
        return title_pic_url

    def get_installed_cnt(self):
        return self.authorized_apps.count()


class Comment(models.Model):
    """应用评分模型类"""
    application = models.ForeignKey(Application, on_delete=models.CASCADE,
                                    verbose_name='应用', help_text='应用')
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True,
                             verbose_name='评论用户', help_text='评论用户')
    content = models.TextField(verbose_name='评论内容', help_text='评论应用')
    score = models.FloatField(verbose_name='评分', help_text='评分', default=3)

    class Meta:
        verbose_name = '应用评分'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.application.name
