# Generated by Django 2.2 on 2020-07-22 16:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('application', '0002_auto_20200515_1333'),
    ]

    operations = [
        migrations.AlterField(
            model_name='application',
            name='name',
            field=models.CharField(help_text='名称', max_length=64, unique=True, verbose_name='名称'),
        ),
        migrations.AlterField(
            model_name='system',
            name='name',
            field=models.CharField(help_text='名称', max_length=64, unique=True, verbose_name='名称'),
        ),
    ]
