from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from django.conf import settings
from apps.application.models import System, Application, Comment
from apps.oauth.models import OAuthApplication
from apps.oauth.utils.rsa_key import generate_key_pair, cat_download_path
from django.db.transaction import atomic


class CommentSerializer(serializers.ModelSerializer):
    """应用评分模型序列"""
    username = serializers.CharField(source='user.username', allow_null=True,
                                     read_only=True, default='匿名用户', help_text='评分用户')

    def validate_score(self, value):
        if (1 > value | value > 5) | value % 0.5 != 0:
            raise ValidationError(detail='评分只能在1-5之间')

    class Meta:
        model = Comment
        fields = ('id', 'username', 'score', 'content')


class ApplicationSerializer(serializers.ModelSerializer):
    """应用模型序列"""
    developer = serializers.ReadOnlyField(source="developer_username", help_text='开发者姓名')
    installed = serializers.BooleanField(read_only=True, help_text='是否已安装该应用',)
    comments = CommentSerializer(many=True, source='comment_set', read_only=True, help_text='评论',
                                 allow_empty=True)
    installed_cnt = serializers.IntegerField(help_text='安装人数', read_only=True)
    desktop_app_id = serializers.IntegerField(min_value=0, max_value=23, read_only=True,
                                              allow_null=True, help_text='用户安装应用id')
    client_id = serializers.CharField(
        max_length=100, source='oauthapplication__client_id', read_only=True, allow_null=True)
    client_secret = serializers.CharField(
        max_length=255, source='oauthapplication_client_secret', read_only=True,
        allow_null=True)
    public_key = serializers.CharField(
        max_length=128, source='oauthapplication_public_key', read_only=True,
        allow_null=True)
    client_type = serializers.CharField(
        source='oauthapplication_client_type', required=False, max_length=32,
        help_text='应用类型', default=None,)
    authorization_grant_type = serializers.CharField(
        source='oauthapplication_authorization_grant_type', required=False,
        allow_null=True, max_length=32, help_text='授权类型', default=None,
    )
    redirect_uris = serializers.CharField(
        source='oauthapplication_redirect_uris', max_length=256, required=False,
        allow_null=True, help_text='跳转路由', default=None,
    )
    skip_authorization = serializers.BooleanField(
        source='oauthapplication_skip_auhtorization', default=True, allow_null=True,
        help_text='跳过授权校验')

    class Meta:
        model = Application
        fields = ('id', 'url', 'name', 'english_name', 'introduction', 'entry_url', 'developer',
                  'logo', 'title_pic', 'is_default', 'is_official', 'system', 'created_date', 'update_date',
                  'installed', 'desktop_app_id', 'comments', 'avg_score', 'installed_cnt',
                  'client_id', 'client_secret', 'client_type', 'redirect_uris',
                  'public_key', 'authorization_grant_type', 'skip_authorization', 'use_iframe',
                  'allow_resize')
        read_only_fields = ('client_id', 'client_secret', 'public_key', 'private_key')
        partial_update_fields = ('name', 'english_name', 'introduction', 'entry_url', 'developer',
                                 'logo', 'title_pic', 'is_default', 'is_official', 'system',)

    def to_representation(self, instance):
        login_user = self.context['request'].user
        instance.installed = instance.authorized_apps.filter(user=login_user).count()
        instance.installed_cnt = instance.get_installed_cnt()
        instance.avg_score = instance.avg_score // 0.5 * 0.5
        authorized_app = instance.authorized_apps.filter(user=login_user).first()
        instance.desktop_app_id = authorized_app.id if authorized_app else None

        attribute = super().to_representation(instance)
        attribute["logo"] = instance.get_logo()
        attribute["title_pic"] = instance.get_title_pic()
        return attribute

    def create(self, validate_data):
        client_type = validate_data.pop('oauthapplication_client_type')
        redirect_uris = validate_data.pop('oauthapplication_redirect_uris')
        authorization_grant_type = validate_data.pop('oauthapplication_authorization_grant_type')
        skip_authorization = validate_data.pop('oauthapplication_skip_auhtorization')
        with atomic():
            db_application = super().create(validate_data)
            if not db_application.is_default:
                user = db_application.developer
                name = db_application.name
                db_oauth_app = OAuthApplication.objects.create(
                    application=db_application,
                    user=user, name=name, skip_authorization=skip_authorization,
                    authorization_grant_type=authorization_grant_type,
                    redirect_uris=redirect_uris, client_type=client_type)
                public_key, private_key = generate_key_pair(db_oauth_app.id)
                db_oauth_app.public_key = public_key
                db_oauth_app.private_key = private_key
                db_oauth_app.save()
        return db_application


class SystemSerializer(serializers.ModelSerializer):
    """应用系统模型序列"""
    applications = ApplicationSerializer(many=True, default=[], read_only=True)
    developer = serializers.ReadOnlyField(source="developer_username")

    class Meta:
        model = System
        fields = ('id', 'url', 'name', 'introduction', 'applications', 'developer')
