from django.db.transaction import atomic
from guardian.shortcuts import assign_perm

from apps.console.models import GroupDetail
from apps.desktop.models import InstalledAPP
from apps.desktop.serializers import get_available_page_seq
from django.contrib.auth.models import User, Group
from django.db.models import Avg

from server.log import s_logger


def install_default_app(instance, **kwargs):
    """
    新建默认应用时，为所有用户安装该默认应用
    :param instance: Application对象
    :param kwargs:
    :return:
    """
    if instance.is_default:
        for user in User.objects.all():
            installed_app, created = InstalledAPP.objects.get_or_create(user=user, application=instance)
            if created:
                page, seq = get_available_page_seq(user)
                installed_app.page = page
                installed_app.seq = seq
                installed_app.save()


def cal_avg_score(instance, **kwargs):
    """
    计算应用平均评分
    :param instance: Commits实例
    :param kwargs:
    :return:
    """
    application = instance.application
    avg_score = application.comment_set.aggregate(avg_score=Avg('score')).get('avg_score')
    application.avg_score = avg_score
    application.save()


def create_or_update_app_manager(instance, created, **kwargs):
    """
    创建或修改应用时修改对应管理器
    :param instance: application实例
    :param created: 是否被创建
    :param kwargs:
    :return:
    """
    if not instance.is_default:
        if created:
            with atomic():
                # 1.用户管理
                super_admin = User.objects.get(username="admin")
                app_admin = User.objects.create_user(
                    username="{}@admin".format(instance.name),
                    first_name="{}@admin".format(instance.name),
                    is_staff=True,
                    password="12345678"
                )

                # 2.群组管理
                # 2.1.创建群组
                admin_group = Group.objects.create(name="{}@应用管理账户".format(instance.name))
                manage_group = Group.objects.create(name="{}@管理组".format(instance.name))
                user_group = Group.objects.create(name="{}@用户组".format(instance.name))
                system_manage_group, _ = Group.objects.get_or_create(name="系统用户管理组")
                GroupDetail.objects.create(group=admin_group, application=instance, app_admin=app_admin,
                                           group_type="应用管理账户", description="应用管理账户")
                GroupDetail.objects.create(group=manage_group, application=instance, app_admin=app_admin,
                                           group_type="管理组", description="管理组")
                GroupDetail.objects.create(group=user_group, application=instance, app_admin=app_admin,
                                           group_type="用户组", description="用户组")
                GroupDetail.objects.get_or_create(group=system_manage_group, group_type="全局管理组",
                                                  description="全局管理组")

                # 2.2.赋予群组相应权限
                # 2.2.1.赋予群组查看应用的权限
                assign_perm("application.view_application", system_manage_group, obj=instance)
                assign_perm("application.view_application", manage_group, obj=instance)
                assign_perm("application.view_application", user_group, obj=instance)

                # 2.2.2.赋予群组查看群组的权限
                assign_perm("view_group", admin_group, obj=manage_group)
                assign_perm("view_group", admin_group, obj=user_group)
                assign_perm("view_group", system_manage_group, obj=user_group)
                assign_perm("view_group", manage_group, obj=user_group)

                # 2.2.3.蒋管理员账户加入群组
                super_admin.groups.add(admin_group.id)
                app_admin.groups.add(admin_group.id)

        else:
            with atomic():
                group_detail_objs = GroupDetail.objects.filter(application=instance)
                for group_detail_obj in group_detail_objs:
                    group_obj = group_detail_obj.group
                    group_obj.name = "{}@{}".format(instance.name, group_detail_obj.group_type)
                    group_obj.save()
                app_admin = group_detail_obj.app_admin
                app_admin.username = "{}@admin".format(instance.name)
                app_admin.first_name = "{}@admin".format(instance.name)
                app_admin.save()


def delete_app_manager(instance, **kwargs):
    """
    删除应用时删除应用管理器
    :param instance: application实例
    :param kwargs:
    :return:
    """
    with atomic():
        group_detail_objs = GroupDetail.objects.filter(application=instance)
        for group_detail_obj in group_detail_objs:
            group_detail_obj.group.delete()
        group_detail_obj.app_admin.delete()
