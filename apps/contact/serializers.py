import redis
from django.contrib.auth.models import User
from django.db import IntegrityError
from django.db.transaction import atomic
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.generics import get_object_or_404

from server.settings import REDIS
from utils.send_websocket import send_notice
from .models import FriendList, Notice
from ..application.models import Application
from ..desktop.serializers import WriteOnceMixin


class NoticeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Notice
        fields = ("id", "text", "notice_type", "is_choice", "is_read", "is_accept",
                  "application", "sender", "receiver", "created_time")
        read_only_fields = ("id", "created_time")
        # write_once_fields = ("is_read", "is_accept", "is_choice", "notice_type",
        #                      "text", "application", "sender", "receiver")

    def to_representation(self, instance):
        attribute = super().to_representation(instance)
        attribute["application"] = "云桌面" if attribute["notice_type"] == "register" \
            else instance.application.name
        return attribute


class FriendListSerializer(serializers.ModelSerializer):
    username = serializers.ReadOnlyField(help_text="好友用户名", source="friend.username")
    first_name = serializers.ReadOnlyField(help_text="好友姓名", source="friend.first_name")

    class Meta:
        model = FriendList
        fields = ("id", "friend", "first_name", "username", "is_active")
        read_only_fields = ("is_active",)

    def create(self, validated_data):
        """保存好友信息"""
        request = self.context.get("request")

        instance, _ = FriendList.objects.get_or_create(**validated_data)
        if instance.is_active:
            raise ValidationError(detail={"detail": "该用户已经是您的好友"})

        friend = validated_data.get("friend")
        if Notice.objects.filter(sender=request.user,
                                 receiver=friend,
                                 notice_type="add_friend"):
            raise ValidationError(detail={"detail": "请求已发送，请勿重复操作"})

        # 保存通知消息 FIXME　获取应用
        serializer_data = {
            "text": "\"{}\"申请将您加入他的通讯录".format(request.user.first_name),
            "notice_type": "add_friend",
            "is_choice": True,
            "sender": request.user.id,
            "receiver": friend.id,
            "application": Application.objects.get(name="控制中心").id
        }
        notice_data = save_data(NoticeSerializer, serializer_data)

        # 通知好友
        send_notice(user_id=friend.id, data=notice_data)

        return instance


def save_data(serializer_class, serializer_data):
    """
    保存序列化数据到数据库
    :param serializer_class: 序列化类
    :param serializer_data: 序列化数据
    :return:
    """
    serializer = serializer_class(data=serializer_data)
    serializer.is_valid(raise_exception=True)
    serializer.save()
    return serializer.data
