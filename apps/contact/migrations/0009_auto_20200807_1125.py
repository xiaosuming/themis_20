# Generated by Django 2.2 on 2020-08-07 11:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contact', '0008_auto_20200807_1037'),
    ]

    operations = [
        migrations.AlterField(
            model_name='notice',
            name='notice_type',
            field=models.CharField(max_length=32, verbose_name='消息类型'),
        ),
    ]
