from django.contrib.auth.models import User
from django.db import models

from apps.application.models import Application

TYPE_CHOICES = (
    ("add_friend", "添加好友"),
    ("register", "注册用户")
)


class Notice(models.Model):
    text = models.CharField(max_length=512, verbose_name="消息内容")
    meta = models.CharField(max_length=256, default="{}", verbose_name="请求参数")
    notice_type = models.CharField(max_length=32, verbose_name="消息类型", null=True)
    application = models.ForeignKey(Application, on_delete=models.CASCADE, verbose_name="所属应用", null=True)
    sender = models.ForeignKey(User, on_delete=models.CASCADE, related_name="sender", verbose_name="发送者")
    receiver = models.ForeignKey(User, on_delete=models.CASCADE, related_name="receiver", verbose_name="接收者")
    is_read = models.BooleanField(verbose_name="是否已读", default=False)
    is_accept = models.BooleanField(verbose_name="是否同意", null=True)
    is_choice = models.BooleanField(verbose_name="是否需要选择", null=True)
    created_time = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')


class FriendList(models.Model):
    user = models.ForeignKey(User, verbose_name="用户", related_name="self_user", on_delete=models.CASCADE)
    friend = models.ForeignKey(User, verbose_name="好友", related_name="friend", on_delete=models.CASCADE)
    is_active = models.BooleanField(default=False, verbose_name="是否有效")
    update_date = models.DateTimeField(auto_now=True, verbose_name='更新时间')

    class Meta:
        verbose_name = "好友列表"
        verbose_name_plural = verbose_name
        unique_together = (("user", "friend"),)
