import redis
from django.contrib.auth.models import User
from django.db.transaction import atomic
from django.db.models import Q
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import mixins, status
from django_filters import rest_framework as filters
from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError, PermissionDenied
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet, GenericViewSet
from django.utils.decorators import method_decorator

from apps.console.views import get_results_in_names
from apps.contact.models import FriendList, Notice
from apps.contact.serializers import FriendListSerializer, NoticeSerializer
from server.settings import REDIS
from utils.send_websocket import send_notice

REDIS_CONN = redis.Redis(
    host=REDIS["HOST"],
    port=REDIS["PORT"],
    db=REDIS["DB"],
    password=REDIS.get("PASSWORD"),
    decode_responses=True
)


class FriendFilter(filters.FilterSet):
    """好友过滤器"""
    names = filters.CharFilter(method="get_friends_in_names")

    class Meta:
        model = FriendList
        fields = ("names",)

    def get_friends_in_names(self, queryset, name, value):
        return get_results_in_names(queryset, "friend__first_name", value)


class NoticeFilter(filters.FilterSet):
    """通知过滤器"""
    apps = filters.CharFilter(method="get_apps_in_names")

    class Meta:
        model = Notice
        fields = ("apps",)

    def get_apps_in_names(self, queryset, name, value):
        # Notice.objects.filter(application__name__icontains=)
        return get_results_in_names(queryset, "application__name", value)


# @class FriendViewSet 用户视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	桌面视图
#   method:GET, POST, DELETE
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2020/08/06 15:00:22|杨伟光|创建类|完成|
# -----------------------------------------------------------------------------------------------------
@method_decorator(name='list', decorator=swagger_auto_schema(
    tags=['控制台-通讯列表'], operation_summary='返回好友列表',
    manual_parameters=[
        openapi.Parameter('limit', openapi.IN_QUERY, description='每页返回的数量',
                          type=openapi.TYPE_INTEGER),
        openapi.Parameter('offset', openapi.IN_QUERY, description='从第几条返回',
                          type=openapi.TYPE_INTEGER),
        openapi.Parameter('names', openapi.IN_QUERY, description='用户姓名列表',
                          type=openapi.TYPE_STRING),
    ]
))
@method_decorator(name='create', decorator=swagger_auto_schema(
    tags=['控制台-通讯列表'], operation_summary='添加好友'
))
@method_decorator(name='destroy', decorator=swagger_auto_schema(
    tags=['控制台-通讯列表'], operation_summary='删除单个好友'
))
class FriendViewSet(mixins.CreateModelMixin,
                    mixins.ListModelMixin,
                    mixins.DestroyModelMixin,
                    GenericViewSet):
    serializer_class = FriendListSerializer
    filterset_class = FriendFilter
    search_fields = ("names",)

    def get_queryset(self):
        queryset = FriendList.objects.filter(user=self.request.user)
        return queryset

    def create(self, request, *args, **kwargs):
        # 1.校验参数
        if not isinstance(request.data, list):
            raise ValidationError(detail={"details": "请求列表数据不合法"})

        # 2.保存好友
        for friend_data in request.data:
            serializer = self.get_serializer(data=friend_data)
            serializer.is_valid(raise_exception=True)
            self.perform_create(serializer)
        return Response(status=status.HTTP_201_CREATED)

    def perform_create(self, serializer):
        login_user = self.request.user
        serializer.save(user=login_user)


# @class NoticeViewSet 通知视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	桌面视图
#   method:GET, POST, PUT, PATCH, DELETE
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2020/08/07 09:20:22|杨伟光|创建类|完成|
# -----------------------------------------------------------------------------------------------------
@method_decorator(name='list', decorator=swagger_auto_schema(
    tags=['控制台-消息中心'], operation_summary='返回通知列表',
    manual_parameters=[
        openapi.Parameter('apps', openapi.IN_QUERY, description='应用列表',
                          type=openapi.TYPE_STRING),
        openapi.Parameter('created_time', openapi.IN_QUERY, description='按时间排序',
                          type=openapi.TYPE_STRING),
        openapi.Parameter('application__name', openapi.IN_QUERY, description='按应用名应用',
                          type=openapi.TYPE_STRING),
    ]
))
@method_decorator(name='create', decorator=swagger_auto_schema(
    tags=['控制台-消息中心'], operation_summary='添加通知'
))
@method_decorator(name='update', decorator=swagger_auto_schema(
    tags=['控制台-消息中心'], operation_summary='更新通知'
))
@method_decorator(name='partial_update', decorator=swagger_auto_schema(
    tags=['控制台-消息中心'], operation_summary='更新通知部分属性'
))
@method_decorator(name='destroy', decorator=swagger_auto_schema(
    tags=['控制台-消息中心'], operation_summary='删除单个通知'
))
class NoticeViewSet(mixins.CreateModelMixin,
                    mixins.UpdateModelMixin,
                    mixins.DestroyModelMixin,
                    mixins.ListModelMixin,
                    GenericViewSet):
    serializer_class = NoticeSerializer
    pagination_class = None
    filterset_class = NoticeFilter
    search_fields = ("apps",)
    ordering_fields = ("created_time", "application__name")

    def get_queryset(self):
        user_notices = Notice.objects.filter(receiver=self.request.user)

        # names = eval(self.request.query_params.get("apps", '[]'))
        # if names:
        #     queryset = user_notices.exclude(application__name="控制中心", notice_type="register")
        #     for name in names:
        #         if any(item in "云桌面" for item in name):
        #             queryset = queryset | user_notices.filter(application__name="控制中心", notice_type="register")
        #             return queryset.distinct()

        return user_notices.distinct()

    def partial_update(self, request, *args, **kwargs):
        """更新通知参数"""
        is_accept = request.data.get("is_accept", None)
        if is_accept is not None:
            instance = self.get_object()
            self.handle_choice(instance, is_accept)
            return Response(status=status.HTTP_204_NO_CONTENT)
        else:
            kwargs['partial'] = True
            return self.update(request, *args, **kwargs)

    @swagger_auto_schema(method='delete', operation_summary='批量删除通知', tags=['控制台-消息中心'])
    @action(methods=['delete'], detail=False)
    def deletions(self, request, *args, **kwargs):
        """删除全部通知"""
        notice_objs = Notice.objects.filter(receiver=request.user)
        notice_objs.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    @staticmethod
    @atomic
    def handle_choice(instance, is_accept):
        """
        处理通知返回结果
        :param instance: 通知实例
        :param is_accept: 是否为选择
        :return:
        """
        # 1.分类操作
        # 1.1.添加好友
        if instance.notice_type == "add_friend":
            if is_accept:
                # 添加用户到好友
                # 添加好友
                receiver, _ = FriendList.objects.get_or_create(
                    user=instance.receiver,
                    friend=instance.sender
                )
                receiver.is_active = True
                receiver.save()
                # 蒋好友状态激活
                sender, _ = FriendList.objects.get_or_create(
                    user=instance.sender,
                    friend=instance.receiver
                )
                sender.is_active = True
                sender.save()

        # 1.2.注册审批
        elif instance.notice_type == "register":
            user = instance.sender
            user_ids = REDIS_CONN.smembers("themis_reg_user")
            if str(user.id) not in user_ids:
                raise PermissionDenied(detail={"detail": "该通知已失效"})

            # 清除注册缓存
            notice_objs = Notice.objects.filter(notice_type="register", sender=user.id)
            notice_objs.update(is_accept=is_accept)
            for notice_obj in notice_objs:
                serializer = NoticeSerializer(notice_obj)
                send_notice(notice_obj.receiver.id, serializer.data)
            REDIS_CONN.srem("themis_reg_user", str(instance.sender.id))
            notice_objs.delete()

            if is_accept:
                user.is_active = True
                user.save()
            else:
                user.delete()

        # 2.删除通知
        instance.delete()
