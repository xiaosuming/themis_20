import json
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.db.models import Max, Q
from django.http import JsonResponse
from django.views import View
from apps.chat.models import GroupChat, User, Message, Myfriend, MessagePart, GroupChatPart
from server.log import s_logger
# 建立私聊和群聊 和展示群聊列表

class GroupChats(View):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    """ 
        @apiName create-groupchats
        @api {POST} /api/v1/groupchats
        @apiGroup chat-groupchats
        @apiVersion 0.0.1
        @apiDescription [聊天]建立私聊和群聊
        @apiParam {list} userlist 用户列表,例如：私聊参数[1],群聊参数[2,3,4,5]
        @apiParam {String} is_groupchat  是否是群聊，0代表私聊，1代表群聊
        @apiParam {String} groupchat_name 要输入的群名     
        @apiSuccess {String} msg 信息
        @apiSuccess {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 成功返回样例:
        {
            {
            "code": 0,
            "msg": "SUCCESS",
            "privatechat_id":2,
            "privatechat_name": 张林,
            "groupchat_creater_id": 1,
            "is_groupchat": false
            },
            {
            "code": 0,
            "msg": "SUCCESS",
            "groupchat_id":1,
            "groupchat_name": 技术交流群
            "groupchat_creater_id": 3,
            "is_groupchat": true
            }
        }
    """

    # 建立 私聊或 群聊
    def post(self, request):

        # 获取参数
        global privatechat, user_id
        log_user = request.user
        put_data = json.loads(request.body)
        userlist = put_data.get('userlist')
        is_groupchat = put_data.get('is_groupchat')
        groupchat_name = put_data.get('groupchat_name')

        # 校验参数
        # if not userlist or not is_groupchat:
        #     res_data = {
        #         "code": 406,
        #         "msg": "参数缺失",
        #     }
        #     return JsonResponse(res_data, safe=False)


        # 执行逻辑
        if is_groupchat == 0:  # 那就可以认定为私聊
            myfriend = Myfriend.objects.filter(myfriendslist__id=userlist[0]).first()
            user = User.objects.filter(id=userlist[0]).first()  # 筛选出这个用户
            if not myfriend:
                res_data = {
                    "msg": "你不是我的好友，请先添加为好友",
                }
                return JsonResponse(res_data, safe=False, status=400)
            else:
                for i in userlist:
                    privatechat = GroupChat.objects.filter(
                        Q(groupchat_members__id=i) & Q(
                            is_groupchat=False)).filter(
                        groupchat_members__id__icontains=log_user.id).first()
                    if privatechat:
                        res_data = {
                            "msg": "已经建立群聊，请不要重复建立",
                        }
                        return JsonResponse(res_data, safe=False, status=400)
                    else:
                        # if myfriend.remark:
                        #     privatechat = GroupChat.objects.create(groupchat_name=myfriend.remark,
                        #                                            is_groupchat=False)  # 那么建立私聊

                        privatechat = GroupChat.objects.create(groupchat_name=user.first_name,
                                                               is_groupchat=False)  # 那么建立私聊
                        for user_id in userlist:
                            privatechat.groupchat_members.add(user_id)  # 把用户加入群聊的人员中。
                            privatechat.groupchat_members.add(log_user)  # 把管理员加入群聊的人员中。
                            privatechat.save()

                            # 建立websocket链接
                            channel_layer = get_channel_layer()
                            async_to_sync(channel_layer.group_send)(
                                "user_%s" % user_id,
                                {
                                    'type': 'chat_message',
                                    'data':
                                        {
                                            "type": "notice",
                                            "notice":
                                                {
                                                    "privatechat_id": privatechat.id,
                                                    'is_groupchat': privatechat.is_groupchat
                                                },
                                        },
                                })

                # 返回结果
                res_data = {
                    "code": 0,
                    "msg": "SUCCESS",
                    "privatechat_id": privatechat.id,
                    'is_groupchat': privatechat.is_groupchat
                }
                return JsonResponse(res_data, safe=False)

        # 建立群聊
        if is_groupchat == 1:  # 那就可以认定为群聊。

            # 校验参数
            if not userlist or not is_groupchat or not groupchat_name:
                res_data = {
                    "msg": "参数缺失",
                }
                return JsonResponse(res_data, safe=False, status=400)

            # 执行逻辑
            groupchat = GroupChat.objects.create(groupchat_name=groupchat_name, groupchat_creater_id=log_user.id,
                                                 is_groupchat=True)  # 建立群聊
            for user_id in userlist:  # 进行用户列表的遍历
                groupchat.groupchat_members.add(user_id)  # 把用户一个个加入群聊的人员中。
            groupchat.groupchat_members.add(log_user)  # 把管理员加入群聊的人员中。
            groupchat.save()

            # 建立websocket链接
            channel_layer = get_channel_layer()
            async_to_sync(channel_layer.group_send)(
                "user_%s" % user_id,
                {
                    'type': 'chat_message',
                    'data':
                        {
                            "type": "notice",
                            "notice":
                                {
                                    "groupchat_id": groupchat.id,
                                    "groupchat_name": groupchat.groupchat_name,
                                    'groupchat_creater_id': groupchat.groupchat_creater_id,
                                    'is_groupchat': groupchat.is_groupchat
                                },
                        },
                })

            res_data = {
                "code": 0,
                "msg": "SUCCESS",
                "groupchat_id": groupchat.id,
                "groupchat_name": groupchat.groupchat_name,
                'groupchat_creater_id': groupchat.groupchat_creater_id,
                'is_groupchat': groupchat.is_groupchat
            }
            return JsonResponse(res_data, safe=False)

    """
        @apiName show-allgroupchats
        @api {GET} /api/v1/groupchats
        @apiGroup chat-groupchats
        @apiVersion 0.0.1
        @apiDescription [聊天]展示左边，私聊和群聊的列表，以及未读消息的统计
        @apiParam {int} is_groupchat 是否是群聊：0或1 
        @apiSuccess {String} msg 信息
        @apiSuccess {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data": [
                {
                    "groupchat_id": 5,
                    "groupchat_name": "张苗",
                    "groupchat_creater_id": 1,
                    "groupchat_member": {
                        "user_id": 6,
                        "user_name": "张苗",
                        "login_name": "zhangmiao",
                        "remark": "zhang",
                        "colour": "#228B22",
                        "ismyfriend": 1
                    },
                    "groupchat_message": {
                        "text_content": "收到消息啦吗？能再发下文件吗？",
                        "send_time": "2020-09-07 20:00:40"
                    },
                    "num": 2,
                    "is_groupchat": false,
                    "send_time": "2020-09-07 20:00:40"
                },
                {
                    "groupchat_id": 4,
                    "groupchat_name": "数据中心群",
                    "groupchat_creater_id": 3,
                    "groupchat_message": {
                        "text_content": "结果大家及时保存，以免过期！",
                        "send_time": "2020-09-05 13:41:46"
                    },
                    "num": 2,
                    "is_groupchat": true,
                    "send_time": "2020-09-05 13:41:46"
                },
            ]
        }
    """

    # 展示左边，私聊和群聊的列表，以及未读消息的统计
    def get(self, request):

        # 获取参数
        global allgroupchats
        log_user = request.user
        is_groupchat = request.GET.get('is_groupchat')  # 获取是否是群组的参数

        # 校验参数
        if not log_user:
            res_data = {
                "msg": "参数缺失",
            }
            return JsonResponse(res_data, safe=False, status=406)

        # 执行逻辑
        if is_groupchat:  # 如果参数存在
            groupchats = GroupChat.objects.filter(
                Q(is_groupchat=is_groupchat) & Q(groupchat_members__id__contains=log_user.id)).order_by(
                "-created_time").distinct() # 找到是群聊的所有群组
            data = []
            try:
                s_logger.glob.info("群组-{}".format(log_user.groupmembers.all()))
            except Exception as e:
                s_logger.glob.info("错误-{}".format(e))
            for groupchat in groupchats:  # 遍历所有的群组，并展示出来

                m1 = MessagePart.objects.filter(groupchat_id=groupchat.id, user_id=log_user.id).first()#查找清空数据表
                if m1:  # 如果有聊天并且有执行清空的用户
                    lastmessage_id = MessagePart.objects.filter(groupchat_id=groupchat.id,
                                                                user_id=log_user.id).last().lastmessage_id#清空数据表中储存的最新消息记录

                    messages = Message.objects.filter(groupchat_id=groupchat.id, id__gt=lastmessage_id).order_by(
                        '-send_time')#筛选出清空表中所记录的消息以后的消息

                    num = messages.filter(
                        isreaded=0).exclude(user_id=log_user.id).count()  # 筛选出【对应应用】的【未读的消息】并【计数】
                    data.append({
                        "groupchat_id": groupchat.id,
                        'groupchat_name': groupchat.groupchat_name,
                        'groupchat_creater_id': groupchat.groupchat_creater_id,
                        'num': num
                    })

                else:
                    num = Message.objects.filter(
                        Q(groupchat_id=groupchat.id) &
                        Q(isreaded=0)).exclude(user_id=log_user.id).count()  # 筛选出【对应应用】的【未读的消息】并【计数】
                    data.append({
                        "groupchat_id": groupchat.id,
                        'groupchat_name': groupchat.groupchat_name,
                        'groupchat_creater_id': groupchat.groupchat_creater_id,
                        'num': num
                    })

            # 返回结果
            res_data = {
                "code": 0,
                "msg": "SUCCESS",
                "data": data
            }
            return JsonResponse(res_data, safe=False)

        else:  # 如果参数不存在
            groupchats = GroupChat.objects.annotate(latest_msg_time=Max('messagegroupchat__send_time'))  # 获取群组中最早的消息
            groupchats = groupchats.order_by('-latest_msg_time')  # 根据每个群中最早的消息进行排序
            groupchats = groupchats.filter(groupchat_members__id__contains=log_user.id)
            groupchatparts=log_user.groupchatpartuser.all()
            if groupchatparts:
                a=[]
                for gc in groupchatparts:
                    a.append(gc.groupchat_id)
                allgroupchats=groupchats.exclude(id__in=a)

                data = []
                for g in allgroupchats:  # 逐渐遍历每一个聊天
                    log_user = request.user  # 用户登陆成功
                    m1 = MessagePart.objects.filter(groupchat_id=g.id, user_id=log_user.id).first()
                    if m1:  # 如果有聊天并且有执行清空的用户
                        lastmessage_id = MessagePart.objects.filter(groupchat_id=g.id,
                                                                    user_id=log_user.id).last().lastmessage_id  # 找出清空聊天记录时最后一条消息的id
                        message = g.messagegroupchat.filter(
                            id__gt=lastmessage_id).last()

                        num = Message.objects.filter(Q(groupchat_id=g.id) & Q(id__gt=lastmessage_id)).filter(
                            isreaded=0).exclude(user_id=log_user.id).count()  # 筛选出【每一个聊天】的【未读的消息】并【计数】
                        if g.is_groupchat == 0 and message:  # 如果不是群聊，同时最新消息存在

                            members = g.groupchat_members.exclude(id=log_user.id)
                            if members:

                                member = g.groupchat_members.exclude(id=log_user.id).first()
                                remark = Myfriend.objects.filter(
                                    myfriendslist__id=member.id, owner_id=log_user.id).first().remark
                                print(remark)

                                data.append({
                                    "groupchat_id": g.id,
                                    "groupchat_name": g.groupchat_name,
                                    "groupchat_member": {
                                        "user_id": member.id,
                                        "user_name": member.first_name,
                                        "login_name": member.username,
                                        "remark": remark,
                                        "colour": member.user_icon.pure_color,
                                        "ismyfriend": 1
                                    },
                                    'groupchat_message': {
                                        "text_content": message.text_content,
                                        "send_time": message.send_time.strftime(
                                            '%Y-%m-%d %H:%M:%S')},  # 同时展示出群消息的第一条内容
                                    "num": num,
                                    "is_groupchat": g.is_groupchat,
                                    "send_time": g.messagegroupchat.all().last().send_time.strftime(
                                        '%Y-%m-%d %H:%M:%S')
                                })  # 展示详细的结果
                            else:
                                pass

                        if g.is_groupchat == 1 and message:
                            data.append({
                                "groupchat_id": g.id,
                                "groupchat_name": g.groupchat_name,
                                'groupchat_creater_id': g.groupchat_creater_id,
                                'groupchat_message': {
                                    "text_content": message.text_content,
                                    "send_time": message.send_time.strftime(
                                        '%Y-%m-%d %H:%M:%S'
                                    )},
                                # 同时展示出群消息的第一条内容
                                "num": num,
                                "is_groupchat": g.is_groupchat,
                                "send_time": g.messagegroupchat.all().last().send_time.strftime(
                                    '%Y-%m-%d %H:%M:%S')
                            })

                        if g.is_groupchat == 0 and not message:

                            members = g.groupchat_members.exclude(id=log_user.id)
                            if members:
                                member = g.groupchat_members.exclude(id=log_user.id).first()
                                remark = Myfriend.objects.filter(
                                    myfriendslist__id=member.id, owner_id=log_user.id).first().remark

                                data.append({
                                    "groupchat_id": g.id,
                                    "groupchat_name": g.groupchat_name,
                                    'groupchat_creater_id': g.groupchat_creater_id,
                                    "groupchat_member": {
                                        "user_id": member.id,
                                        "user_name": member.first_name,
                                        "login_name": member.username,
                                        "remark": remark,
                                        "colour": member.user_icon.pure_color,
                                        "ismyfriend": 1
                                    },
                                    "is_groupchat": g.is_groupchat,
                                })
                            else:
                                pass

                        if g.is_groupchat == 1 and not message:
                            data.append({
                                "groupchat_id": g.id,
                                "groupchat_name": g.groupchat_name,
                                'groupchat_creater_id': g.groupchat_creater_id,
                                "is_groupchat": g.is_groupchat,
                            })

                    else:  # 如果用户没有清空消息记录
                        num = Message.objects.filter(groupchat_id=g.id).filter(
                            isreaded=0).exclude(user_id=log_user.id).count()  # 筛选出【对应应用】的【未读的消息】并【计数】
                        if g.is_groupchat == 0 and g.messagegroupchat.all().last() is not None:  # 如果不是群聊，同时最新消息存在

                            members = g.groupchat_members.exclude(id=log_user.id)
                            if members:
                                member = g.groupchat_members.exclude(id=log_user.id).first()
                                remark = Myfriend.objects.filter(
                                    myfriendslist__id=member.id, owner_id=log_user.id).first().remark

                                data.append({
                                    "groupchat_id": g.id,
                                    "groupchat_name": g.groupchat_name,
                                    'groupchat_creater_id': g.groupchat_creater_id,
                                    "groupchat_member": {
                                        "user_id": member.id,
                                        "user_name": member.first_name,
                                        "login_name": member.username,
                                        "remark": remark,
                                        "colour": member.user_icon.pure_color,
                                        "ismyfriend": 1
                                    },
                                    'groupchat_message': {
                                        "text_content": g.messagegroupchat.all().last().text_content,
                                        "send_time": g.messagegroupchat.all().last().send_time.strftime(
                                            '%Y-%m-%d %H:%M:%S')},  # 同时展示出群消息的第一条内容
                                    "num": num,
                                    "is_groupchat": g.is_groupchat,
                                    "send_time": g.messagegroupchat.all().last().send_time.strftime(
                                        '%Y-%m-%d %H:%M:%S')
                                })  # 展示详细的结果
                            else:
                                pass

                        if g.is_groupchat == 1 and g.messagegroupchat.all().last() is not None:
                            data.append({
                                "groupchat_id": g.id,
                                "groupchat_name": g.groupchat_name,
                                'groupchat_creater_id': g.groupchat_creater_id,
                                'groupchat_message': {
                                    "text_content": g.messagegroupchat.all().last().text_content,
                                    "send_time": g.messagegroupchat.all().last().send_time.strftime(
                                        '%Y-%m-%d %H:%M:%S'
                                    )},
                                # 同时展示出群消息的第一条内容
                                "num": num,
                                "is_groupchat": g.is_groupchat,
                                "send_time": g.messagegroupchat.all().last().send_time.strftime(
                                    '%Y-%m-%d %H:%M:%S')
                            })

                        if g.is_groupchat == 0 and g.messagegroupchat.all().last() is None:

                            members = g.groupchat_members.exclude(id=log_user.id)
                            if members:
                                member = g.groupchat_members.exclude(id=log_user.id).first()
                                remark = Myfriend.objects.filter(
                                    myfriendslist__id=member.id, owner_id=log_user.id).first().remark

                                data.append({
                                    "groupchat_id": g.id,
                                    "groupchat_name": g.groupchat_name,
                                    'groupchat_creater_id': g.groupchat_creater_id,
                                    "groupchat_member": {
                                        "user_id": member.id,
                                        "user_name": member.first_name,
                                        "login_name": member.username,
                                        "remark": remark,
                                        "colour": member.user_icon.pure_color,
                                        "ismyfriend": 1
                                    },
                                    "is_groupchat": g.is_groupchat,
                                })

                            else:
                                pass

                        if g.is_groupchat == 1 and g.messagegroupchat.all().last() is None:
                            data.append({
                                "groupchat_id": g.id,
                                "groupchat_name": g.groupchat_name,
                                'groupchat_creater_id': g.groupchat_creater_id,
                                "is_groupchat": g.is_groupchat,
                            })

                # 返回结果
                res_data = {
                    "code": 0,
                    "msg": "SUCCESS",
                    "data": data
                }
                return JsonResponse(res_data, safe=False)


            else:
                data = []
                for g in groupchats:  # 逐渐遍历每一个聊天
                    log_user = request.user  # 用户登陆成功

                    m1 = MessagePart.objects.filter(groupchat_id=g.id, user_id=log_user.id).first()
                    if m1:  # 如果有聊天并且有执行清空的用户
                        lastmessage_id = MessagePart.objects.filter(groupchat_id=g.id,
                                                                    user_id=log_user.id).last().lastmessage_id# 找出清空聊天记录时最后一条消息的id
                        message = g.messagegroupchat.filter(
                            id__gt=lastmessage_id).last()

                        num = Message.objects.filter(Q(groupchat_id=g.id) & Q(id__gt=lastmessage_id)).filter(
                            isreaded=0).exclude(user_id=log_user.id).count()  # 筛选出【每一个聊天】的【未读的消息】并【计数】
                        if g.is_groupchat == 0 and message:  # 如果不是群聊，同时最新消息存在

                            members = g.groupchat_members.exclude(id=log_user.id)
                            if members:

                                member = g.groupchat_members.exclude(id=log_user.id).first()
                                remark = Myfriend.objects.filter(
                                    myfriendslist__id=member.id,owner_id=log_user.id).first().remark

                                data.append({
                                    "groupchat_id": g.id,
                                    "groupchat_name": g.groupchat_name,
                                    "groupchat_member": {
                                        "user_id": member.id,
                                        "user_name": member.first_name,
                                        "login_name": member.username,
                                        "remark": remark,
                                        "colour": member.user_icon.pure_color,
                                        "ismyfriend": 1
                                    },
                                    'groupchat_message': {
                                        "text_content": message.text_content,
                                        "send_time": message.send_time.strftime(
                                            '%Y-%m-%d %H:%M:%S')},  # 同时展示出群消息的第一条内容
                                    "num": num,
                                    "is_groupchat": g.is_groupchat,
                                    "send_time": g.messagegroupchat.all().last().send_time.strftime(
                                        '%Y-%m-%d %H:%M:%S')
                                })  # 展示详细的结果
                            else:
                                pass

                        if g.is_groupchat == 1 and message:
                            data.append({
                                "groupchat_id": g.id,
                                "groupchat_name": g.groupchat_name,
                                'groupchat_creater_id': g.groupchat_creater_id,
                                'groupchat_message': {
                                    "text_content": message.text_content,
                                    "send_time": message.send_time.strftime(
                                        '%Y-%m-%d %H:%M:%S'
                                    )},
                                # 同时展示出群消息的第一条内容
                                "num": num,
                                "is_groupchat": g.is_groupchat,
                                "send_time": g.messagegroupchat.all().last().send_time.strftime(
                                    '%Y-%m-%d %H:%M:%S')
                            })

                        if g.is_groupchat == 0 and not message:

                            members = g.groupchat_members.exclude(id=log_user.id)
                            if members:
                                member = g.groupchat_members.exclude(id=log_user.id).first()
                                remark = Myfriend.objects.filter(
                                    myfriendslist__id=member.id, owner_id=log_user.id).first().remark

                                data.append({
                                    "groupchat_id": g.id,
                                    "groupchat_name": g.groupchat_name,
                                    'groupchat_creater_id': g.groupchat_creater_id,
                                    "groupchat_member": {
                                        "user_id": member.id,
                                        "user_name": member.first_name,
                                        "login_name": member.username,
                                        "remark": remark,
                                        "colour": member.user_icon.pure_color,
                                        "ismyfriend": 1
                                    },
                                    "is_groupchat": g.is_groupchat,
                                })
                            else:
                                pass

                        if g.is_groupchat == 1 and not message:
                            data.append({
                                "groupchat_id": g.id,
                                "groupchat_name": g.groupchat_name,
                                'groupchat_creater_id': g.groupchat_creater_id,
                                "is_groupchat": g.is_groupchat,
                            })

                    else:  # 如果用户没有清空消息记录
                        num = Message.objects.filter(groupchat_id=g.id).filter(
                            isreaded=0).exclude(user_id=log_user.id).count()  # 筛选出【对应应用】的【未读的消息】并【计数】
                        if g.is_groupchat == 0 and g.messagegroupchat.all().last() is not None:  # 如果不是群聊，同时最新消息存在

                            members = g.groupchat_members.exclude(id=log_user.id)
                            if members:
                                member = g.groupchat_members.exclude(id=log_user.id).first()
                                remark = Myfriend.objects.filter(
                                    myfriendslist__id=member.id, owner_id=log_user.id).first().remark

                                data.append({
                                    "groupchat_id": g.id,
                                    "groupchat_name": g.groupchat_name,
                                    'groupchat_creater_id': g.groupchat_creater_id,
                                    "groupchat_member": {
                                        "user_id": member.id,
                                        "user_name": member.first_name,
                                        "login_name": member.username,
                                        "remark": remark,
                                        "colour": member.user_icon.pure_color,
                                        "ismyfriend": 1
                                    },
                                    'groupchat_message': {
                                        "text_content": g.messagegroupchat.all().last().text_content,
                                        "send_time": g.messagegroupchat.all().last().send_time.strftime(
                                            '%Y-%m-%d %H:%M:%S')},  # 同时展示出群消息的第一条内容
                                    "num": num,
                                    "is_groupchat": g.is_groupchat,
                                    "send_time": g.messagegroupchat.all().last().send_time.strftime(
                                        '%Y-%m-%d %H:%M:%S')
                                })  # 展示详细的结果
                            else:
                                pass

                        if g.is_groupchat == 1 and g.messagegroupchat.all().last() is not None:
                            data.append({
                                "groupchat_id": g.id,
                                "groupchat_name": g.groupchat_name,
                                'groupchat_creater_id': g.groupchat_creater_id,
                                'groupchat_message': {
                                    "text_content": g.messagegroupchat.all().last().text_content,
                                    "send_time": g.messagegroupchat.all().last().send_time.strftime(
                                        '%Y-%m-%d %H:%M:%S'
                                    )},
                                # 同时展示出群消息的第一条内容
                                "num": num,
                                "is_groupchat": g.is_groupchat,
                                "send_time": g.messagegroupchat.all().last().send_time.strftime(
                                    '%Y-%m-%d %H:%M:%S')
                            })

                        if g.is_groupchat == 0 and g.messagegroupchat.all().last() is None:

                            members = g.groupchat_members.exclude(id=log_user.id)
                            if members:
                                member = g.groupchat_members.exclude(id=log_user.id).first()
                                remark = Myfriend.objects.filter(
                                    myfriendslist__id=member.id, owner_id=log_user.id).first().remark

                                data.append({
                                    "groupchat_id": g.id,
                                    "groupchat_name": g.groupchat_name,
                                    'groupchat_creater_id': g.groupchat_creater_id,
                                    "groupchat_member": {
                                        "user_id": member.id,
                                        "user_name": member.first_name,
                                        "login_name": member.username,
                                        "remark": remark,
                                        "colour": member.user_icon.pure_color,
                                        "ismyfriend": 1
                                    },
                                    "is_groupchat": g.is_groupchat,
                                })

                            else:
                                pass

                        if g.is_groupchat == 1 and g.messagegroupchat.all().last() is None:
                            data.append({
                                "groupchat_id": g.id,
                                "groupchat_name": g.groupchat_name,
                                'groupchat_creater_id': g.groupchat_creater_id,
                                "is_groupchat": g.is_groupchat,
                            })

                # 返回结果
                res_data = {
                    "code": 0,
                    "msg": "SUCCESS",
                    "data": data
                }
                return JsonResponse(res_data, safe=False)


# 对某一个群，进行增删改查
class SingleGroupchat(View):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    """ 
            @apiName delete-groupchats
            @api {DELETE} /api/v1/groupchats/{groupchat_id}
            @apiGroup chat-groupchats
            @apiVersion 0.0.1
            @apiDescription [聊天]解散群组
            @apiParam {int} groupchat_id 群组id
            @apiSuccess {String} msg 信息
            @apiSuccess {String} code 0代表无错误 1代表有错误
            @apiSuccessExample {json} 成功返回样例:
            {
                "code": 0,
                "msg": "SUCCESS",
            }
    """

    # 解散群组
    def delete(self, request, groupchat_id):

        # 校验参数
        if not groupchat_id:
            res_data = {
                "msg": "找不到网页",
            }
            return JsonResponse(res_data, safe=False, status=404)

        # 如果获取成功，就筛选出具体的群组！
        groupchat = GroupChat.objects.filter(id=groupchat_id).first()
        if groupchat is None:  # 如果群组不存在 ，则提示：
            res_data = {
                "msg": "参数缺失",
            }
            return JsonResponse(res_data, safe=False, status=406)

        # 如果群组存在，则删除解散群组！
        else:
            groupchat.delete()

            members=groupchat.groupchat_members.all()
            for me in members:
                user_id=me.id
                # 建立websocket链接
                channel_layer = get_channel_layer()
                async_to_sync(channel_layer.group_send)(
                    "user_%s" % user_id,
                    {
                        'type': 'chat_message',
                        'data':
                            {
                                "type": "notice",
                                "notice":
                                    {
                                        "groupchat_id": groupchat_id,
                                        "user_id": user_id,
                                        'msg': "解散群聊"
                                    },
                            },
                    })


            # 返回结果
            res_data = {
                "code": 0,
                "msg": "SUCCESS",
            }
            return JsonResponse(res_data, safe=False)

    """ 
        @apiName change-groupleaders
        @api {PATCH} /api/v1/groupchats/{groupchat_id}
        @apiGroup chat-groupchats
        @apiVersion 0.0.1
        @apiDescription [聊天]转让管理员 和 修改群名 和 删除聊天列表中的某个聊天
        @apiParam {int} groupchat_id 群组id
        @apiParam {int} is_deleteed  0为否/1为是
        @apiParam {String} user_id 群成员id
        @apiParam {String} groupchat_name 群组名字
        @apiSuccess {String} msg 信息
        @apiSuccess {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 成功返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
        }
    """

    # 转让群主 和 修改群名 和 删聊天列表中某一个聊天
    def patch(self, request, groupchat_id):


        # 获取群组id，同时输入想要转给群成员的名字
        log_user = request.user
        put_data = json.loads(request.body)
        user_id = put_data.get('user_id')
        groupchat_name = put_data.get('groupchat_name')
        is_deleteed = put_data.get('is_deleteed')


        # # 校验参数
        # if not user_id or not groupchat_name:
        #     res_data = {
        #         "code": 406,
        #         "msg": "参数缺失",
        #     }
        #     return JsonResponse(res_data, safe=False)


        # 执行逻辑
        if groupchat_id and user_id:  # 如果存在，则转让群主！
            groupchat = GroupChat.objects.filter(
                Q(id=groupchat_id) & Q(groupchat_members__id__contains=log_user.id)).first()  # 确定是哪个群组
            member = groupchat.groupchat_members.filter(id=user_id).first()  # 输入的成员名字是否在群中
            if member is None:  # 如果不在则提示如下：
                res_data = {
                    "msg": "参数缺失",
                }
                return JsonResponse(res_data, safe=False, status=406)

            else:  # 如果在则进行群组装让
                groupchat.groupchat_creater_id = member.id  # 进行群主转让
                groupchat.save()
            # 返回结果，转让成功
            res_data = {
                "code": 0,
                "msg": "SUCCESS",
            }
            return JsonResponse(res_data, safe=False)

        elif groupchat_id and groupchat_name:
            groupchat = GroupChat.objects.filter(
                Q(id=groupchat_id) & Q(groupchat_members__id__contains=log_user.id)).first()  # 确定是哪个群组
            groupchat.groupchat_name = groupchat_name  # 变更群名
            groupchat.save()  # 保存新的群名

            # 返回结果，修改群名成功
            data = ({
                "groupchat_id": groupchat.id,
                "groupchat_name": groupchat.groupchat_name,
            })
            res_data = {
                "code": 0,
                "msg": "SUCCESS",
                'data': data
            }
            return JsonResponse(res_data, safe=False)

        # 删除列表中的某一个群聊
        elif groupchat_id and is_deleteed:
            # GroupChat.objects.filter(id=groupchat_id,groupchat_members__id__contains=log_user.id).update(is_deleteed=is_deleteed)  # 确定是哪个群组
            GroupChatPart.objects.create(groupchat_id=groupchat_id, user_id=log_user.id, is_deleteed=is_deleteed)
            # 返回结果，删除成功
            res_data = {
                "code": 0,
                "msg": "SUCCESS",
            }
            return JsonResponse(res_data, safe=False)

    """ 
        @apiName change-groupleaders
        @api {GET} /api/v1/groupchats/{groupchat_id}
        @apiGroup chat-groupchats
        @apiVersion 0.0.1
        @apiDescription [聊天]展示群的详细信息，并进行群成员筛选
        @apiParam {int} groupchat_id 群组id
        @apiParam {String} user_name 群成员名字
        @apiSuccess {String} msg 信息
        @apiSuccess {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 成功返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data": {
                "groupchat_id": 4,
                "groupchat_name": "数据中心群",
                "groupchat_creater_id": 3,
                "groupchat_members": [
                    {
                        "user_id": 1,
                        "user_name": "admin",
                        "login_name": "admin",
                        "colour": "#4682B4",
                        "ismyfriend": 0
                    },
                    {
                        "user_id": 3,
                        "user_name": "张伟",
                        "login_name": "zhangwei",
                        "remark": "总经理",
                        "colour": "#70DBDB",
                        "ismyfriend": 1
                    },
                    {
                        "user_id": 4,
                        "user_name": "李璐",
                        "login_name": "lilu",
                        "remark": null,
                        "colour": "#5F9EA0",
                        "ismyfriend": 1
                    }
                ],
                "num": 3,
                "is_groupchat": true
            }
        }
"""

    # 展示某个群的详细信息
    def get(self, request, groupchat_id):

        # 获取参数
        global members, names

        log_user = request.user
        user_name = request.GET.get('user_name')  # 输入要搜索的人名

        # 校验参数
        gc = GroupChat.objects.filter(id=groupchat_id)
        if not gc:
            res_data = {
                "msg": "你还不是他（她）的好友，请先加对方为好友",
            }
            return JsonResponse(res_data, safe=False, status=400)

        # 执行逻辑
        if groupchat_id and user_name:
            # 输入群成员名字，进行筛选
            groupchat = GroupChat.objects.filter(
                Q(id=groupchat_id) & Q(groupchat_members__id__contains=log_user.id)).first()
            members = groupchat.groupchat_members.filter(
                Q(first_name__icontains=user_name) | Q(username__icontains=user_name))  # 显示群成员的名字
            allmembers = []
            for m in members:
                friend = Myfriend.objects.filter(myfriendslist__id=m.id,owner_id=log_user.id).first()
                if friend:
                    remark = Myfriend.objects.filter(myfriendslist__id=m.id,owner_id=log_user.id).first().remark
                    allmembers.append({
                        "user_id": m.id,
                        "user_name": m.first_name,
                        "login_name": m.username,
                        "remark": remark,
                        "colour": m.user_icon.pure_color,
                        "ismyfriend": 1,
                    })
                else:
                    allmembers.append({
                        "user_id": m.id,
                        "user_name": m.first_name,
                        "login_name": m.username,
                        "colour": m.user_icon.pure_color,
                        "ismyfriend": 0
                    })

            data = ({
                "groupchat_id": groupchat.id,
                'groupchat_name': groupchat.groupchat_name,
                'groupchat_creater_id': groupchat.groupchat_creater_id,
                "groupchat_members": allmembers,
                'is_groupchat': groupchat.is_groupchat
            })  # 返回展示用户详细信息

            # 返回结果
            res_data = {
                "code": 0,
                "msg": "SUCCESS",
                "data": data
            }
            return JsonResponse(res_data, safe=False)

        if groupchat_id:
            groupchat = GroupChat.objects.filter(
                Q(id=groupchat_id) & Q(groupchat_members__id__contains=log_user.id)).first()
            if groupchat.is_groupchat == 1:
                num = groupchat.groupchat_members.all().count()  # 统计群组中群成员的数量
                members = groupchat.groupchat_members.all()  # 查看群组中群成员的名字
                allmembers = []
                for m in members:
                    friend = Myfriend.objects.filter(myfriendslist__id=m.id, owner_id=log_user.id).first()
                    if friend:
                        remark = Myfriend.objects.filter(myfriendslist__id=m.id, owner_id=log_user.id).first().remark
                        allmembers.append({
                            "user_id": m.id,
                            "user_name": m.first_name,
                            "login_name": m.username,
                            "remark": remark,
                            "colour": m.user_icon.pure_color,
                            "ismyfriend": 1
                        })
                    else:
                        allmembers.append({
                            "user_id": m.id,
                            "user_name": m.first_name,
                            "login_name": m.username,
                            "colour": m.user_icon.pure_color,
                            "ismyfriend": 0
                        })

                data = ({
                    "groupchat_id": groupchat.id,
                    'groupchat_name': groupchat.groupchat_name,
                    'groupchat_creater_id': groupchat.groupchat_creater_id,
                    "groupchat_members": allmembers,
                    'num': num,
                    'is_groupchat': groupchat.is_groupchat
                })  # 返回展示用户详细信息

                # 返回结果
                res_data = {
                    "code": 0,
                    "msg": "SUCCESS",
                    "data": data
                }
                return JsonResponse(res_data, safe=False)
            else:
                num = groupchat.groupchat_members.all().count()  # 统计群组中群成员的数量
                members = groupchat.groupchat_members.all()  # 查看群组中群成员的名字
                m = groupchat.groupchat_members.exclude(id=log_user.id).first()
                remark = Myfriend.objects.filter(myfriendslist__id=m.id, owner_id=log_user.id).first().remark
                allmembers = []
                allmembers.append({
                    "user_id": m.id,
                    "user_name": m.first_name,
                    "login_name": m.username,
                    "remark": remark,
                    "colour": m.user_icon.pure_color,
                    "ismyfriend": 1
                })

                allmembers.append({
                    "user_id": log_user.id,
                    "user_name": log_user.first_name,
                    "login_name": log_user.username,
                    "colour": log_user.user_icon.pure_color,
                    "ismyfriend": 1
                })

                # for m in members:
                #     friend = Myfriend.objects.filter(myfriendslist_id=m.id).first()
                #     if friend:
                #         remark = friend.remark
                #         allmembers.append({
                #             "user_id": m.id,
                #             "user_name": m.first_name,
                #             "login_name": m.username,
                #             "remark": remark,
                #             "colour": m.user_icon.pure_color,
                #             "ismyfriend": 1
                #         })
                #     else:
                #         allmembers.append({
                #             "user_id": m.id,
                #             "user_name": m.first_name,
                #             "login_name": m.username,
                #             "colour": m.user_icon.pure_color,
                #             "ismyfriend": 0
                #         })

                data = ({
                    "groupchat_id": groupchat.id,
                    'groupchat_name': m.first_name,
                    'groupchat_creater_id': groupchat.groupchat_creater_id,
                    "groupchat_members": allmembers,
                    'num': num,
                    'is_groupchat': groupchat.is_groupchat
                })  # 返回展示用户详细信息

                # 返回结果
                res_data = {
                    "code": 0,
                    "msg": "SUCCESS",
                    "data": data
                }
                return JsonResponse(res_data, safe=False)














































