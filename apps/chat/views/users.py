from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from django.http import JsonResponse
from django.views import View
from rest_framework.exceptions import NotAcceptable
from apps.chat.models import Myfriend, GroupChat

# 通过模糊搜索，展示普通用户列表
from apps.console.models import UserIcon


class Users(View):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    """
        @apiName show-users
        @api {GET} /api/v1/users
        @apiGroup chat-users
        @apiVersion 0.0.1
        @apiDescription [聊天]通过模糊搜索，展示普通用户列表
        @apiParam {String} user_name 用户名字
        @apiSuccess {String} msg 信息
        @apiSuccess {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data": [
                {
                    "user_id": 3,
                    "user_name": "张伟",
                    "login_name": "zhangwei",
                    "remark": "总经理",
                    "colour": "#70DBDB",
                    "ismyfriend": 1
                },
                {
                    "user_id": 6,
                    "user_name": "张苗",
                    "login_name": "zhangmiao",
                    "remark": "zhang",
                    "colour": "#228B22",
                    "ismyfriend": 1
                },
                {
                    "user_id": 8,
                    "user_name": "张扬",
                    "login_name": "zhangyang",
                    "remark": "财务",
                    "colour": "#4682B4",
                    "ismyfriend": 1
                }
            ]
        }
    """

    # [聊天]通过用户名搜索，展示普通用户列表
    def get(self, request):
        log_user = request.user
        user_name = request.GET.get('user_name')  #

        # FIXME 参数校验
        if user_name:  # 如果关键字存在
            users = User.objects.filter(
                Q(first_name__icontains=user_name) | Q(username__icontains=user_name))  # 则以关键字模糊筛选我的好友
            # 返回结果，展示出用户列表
            data = []
            for u in users:
                friend = Myfriend.objects.filter(Q(myfriendslist__id=u.id) & Q(owner_id=log_user.id)).first()
                if friend:
                    data.append({
                        'user_id': u.id,
                        'user_name': u.first_name,
                        'login_name': u.username,
                        'remark': friend.remark,
                        'colour': u.user_icon.pure_color,
                        'ismyfriend': 1

                    })
                else:
                    data.append({
                        "user_id": u.id,
                        'user_name': u.first_name,
                        "login_name": u.username,
                        'colour': u.user_icon.pure_color,
                        'ismyfriend': 0
                    })

            # 返回结果
            res_data = {
                "code": 0,
                "msg": "SUCCESS",
                "data": data
            }
            return JsonResponse(res_data, safe=False)


# 展示单个用户的详细信息
class SingleUsers(View):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        """ 
            @apiName show-userdetail
            @api {GET} /api/v1/users/{user_id}
            @apiGroup chat-users
            @apiVersion 0.0.1
            @apiDescription [应用通知]展示个人详情
            @apiParam {int} user_id 用户id
            @apiSuccess {String} msg 信息
            @apiSuccess {String} code 0代表无错误 1代表有错误
            @apiSuccessExample {json} 返回样例:
            {
                "code": 0,
                "msg": "SUCCESS",
                "data": {
                    "id": 3,
                    "user_name": "张伟",
                    "groupchat_id": 155,
                    "login_name": "zhangwei",
                    "colour": "#228B22",
                    "ismyfriend": 0
                }
            }
        """

    def get(self, request, user_id):
        global data
        log_user = request.user
        userdetail = Myfriend.objects.filter(
            Q(myfriendslist__id=user_id) & Q(owner_id=log_user.id)).first()  # 通过用户的id，筛选出用户的详细信息
        u = User.objects.filter(id=user_id).first()
        g = GroupChat.objects.filter(Q(groupchat_members__id=user_id) & Q(is_groupchat=0)).first()
        if userdetail and g:
            data = ({
                "user_id": userdetail.myfriendslist.id,
                "groupchat_id": g.id,
                'user_name': userdetail.myfriendslist.first_name,
                'login_name': userdetail.myfriendslist.username,
                'remarks': userdetail.remark,
                'colour': userdetail.myfriendslist.user_icon.pure_color,
                "ismyfriend": 1
            })  # 返回展示用户详细信息
            res_data = {
                "code": 0,
                "msg": "SUCCESS",
                "data": data
            }
            return JsonResponse(res_data, safe=False)
        elif userdetail and g is None:

            data = ({
                "user_id": userdetail.myfriendslist.id,
                "groupchat_id": None,
                'user_name': userdetail.myfriendslist.first_name,
                'login_name': userdetail.myfriendslist.username,
                'remarks': userdetail.remark,
                'colour': userdetail.myfriendslist.user_icon.pure_color,
                "ismyfriend": 1
            })  # 返回展示用户详细信息

            res_data = {
                "code": 0,
                "msg": "SUCCESS",
                "data": data
            }
            return JsonResponse(res_data, safe=False)

        elif userdetail is None and g:

            data = ({
                "id": u.id,
                "user_name": u.first_name,
                "groupchat_id": g.id,
                "login_name": u.username,
                "colour": u.user_icon.pure_color,
                "ismyfriend": 0
            })  # 返回展示用户详细信息

            res_data = {
                "code": 0,
                "msg": "SUCCESS",
                "data": data
            }
            return JsonResponse(res_data, safe=False)

        elif userdetail is None and g is None:
            res_data = {
                "id": u.id,
                "user_name": u.first_name,
                "groupchat_id": None,
                "login_name": u.username,
                "colour": u.user_icon.pure_color,
                "ismyfriend": 0
            }  # 返回用户详细信息
            return JsonResponse(res_data, safe=False)
