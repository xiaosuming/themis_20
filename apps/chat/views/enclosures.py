import os
import time
from django.db.models import Q
from django.http import JsonResponse
from django.views import View
from apps.chat.models import GroupChat, Enclosure, Myfriend
from server import settings


# 附件资源的上传和展示
class Enclosures(View):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    """ 
        @apiName post-enclosures
        @api {POST} /api/v1/groupchats/{groupchat_id}/enclosures
        @apiGroup chat-enclosures
        @apiVersion 0.0.1
        @apiDescription [聊天]上传附件
        @apiParam {File} file 要上传的文件     
        @apiParam {int} groupchat_id 群聊或者私聊的id
        @apiSuccess {String} msg 信息
        @apiSuccess {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 成功返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",

        }
    """

    def post(self, request, groupchat_id):

        gc = GroupChat.objects.filter(id=groupchat_id)
        if not gc:
            res_data = {
                "msg": "你还不是他（她）的好友，请先加对方为好友"
            }
            return JsonResponse(res_data, safe=False, status=400)

        log_user = request.user
        file_obj = request.FILES['file']  # 获取前端传输的文件对象
        file_type = file_obj.name.split('.')[1]  # 获取文件类型
        file_type = file_type.split('"')[0]  # 去掉文件类型中的引号，取引号以前的数据
        file_type = file_type.lower()  # 将文件类型中的数据大写全部转换成小写
        timestr = str(time.time()).replace('.', '')  # 获取当前时间的时间戳
        path = os.path.join(settings.BASE_DIR, 'media/enclosure/')  # 文件上传地址
        if not os.path.exists(path):  # 如果文件上传地址不存在，则新建上传地址
            os.makedirs(path)
        file_name = file_obj.name.split('"')[0]  # 获取文件的名字，也去掉名字中的引号
        filepath = os.path.join("media/enclosure/{0}{1}".format(timestr, file_name))  # 文件路径
        f = open(filepath, 'wb+')  # 根据路径打开指定的文件(以二进制读写方式打开)
        for chunk in file_obj.chunks():  # chunks将对应的文件数据转换成若干片段, 分段写入, 可以有效提高文件读写速度
            f.write(chunk)
        f.close()
        size = os.path.getsize(filepath)  # 获取文件的大小
        enclosure = Enclosure.objects.create(name=file_name, path=filepath, file_type=file_type, size=size,
                                             enclosure_user_id=log_user.id)  # 把文件保存到数据库
        enclosure.groupchats.add(groupchat_id)  # 保存多对多的外键--所属群聊
        enclosure.save()

        # 4 返回应答
        res = {
            "code": 0,
            "msg": "SUCCESS",
        }
        return JsonResponse(res)

    """ 
        @apiName show-enclosures
        @api {GET} /api/v1/groupchats/{groupchat_id}/enclosures
        @apiGroup chat-enclosures
        @apiVersion 0.0.1
        @apiDescription [聊天]展示附件详细信息
        @apiParam {int} groupchat_id 群聊或者私聊的id     
        @apiSuccess {String} msg 信息
        @apiSuccess {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 成功返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data": [
                {
                    "enclosure_id": 58,
                    "enclosure_name": "themis_20-master.zip",
                    "enclosure_size": 133250,
                    "enclosure_url": "http://192.168.50.42:8000/api/media/enclosure/15995646534031315themis_20-master.zip",
                    "enclosure_type": "zip",
                    "enclosur_user": {
                        "user_id": 4,
                        "user_name": "李璐",
                        "login_name": "lilu",
                        "colour": "#5F9EA0",
                        "remark": "露露",
                        "ismyfriend": 1
                    },
                    "enclosure_colour": "#FF0000",
                    "is_deleteed": 0,
                    "created_time": "2020-09-08 11:30:53"
                },
                {
                    "enclosure_id": 59,
                    "enclosure_name": "工作报告.pdf",
                    "enclosure_size": 0,
                    "enclosure_url": "http://192.168.50.42:8000/api/media/enclosure/15995646627960577工作报告.pdf",
                    "enclosure_type": "pdf",
                    "enclosur_user": {
                        "user_id": 4,
                        "user_name": "李璐",
                        "login_name": "lilu",
                        "colour": "#5F9EA0",
                        "remark": "露露",
                        "ismyfriend": 1
                    },
                    "enclosure_colour": "#FF0000",
                    "is_deleteed": 1,
                    "created_time": "2020-09-08 11:31:02"
                },
            ]
        }
    """

    def get(self, request, groupchat_id):

        gc = GroupChat.objects.filter(id=groupchat_id)
        if not gc:
            res_data = {
                    "msg": "你还不是他（她）的好友，请先加对方为好友"
            }
            return JsonResponse(res_data, safe=False, status=400)

        groupchat = GroupChat.objects.filter(id=groupchat_id).first()  # 群组存在则筛选得到群组！
        enclosures = groupchat.enclosuregroupchats.all()  # 统计组中的附件
        data = []  # 遍历附件，并展示出来
        for e in enclosures:
            friend = Myfriend.objects.filter(myfriendslist__id=e.enclosure_user.id).first()
            if friend:
                data.append({
                    'enclosure_id': e.id,
                    'enclosure_name': e.name,
                    'enclosure_size': e.size,
                    'enclosure_url': e.get_file_url(),
                    'enclosure_type': e.file_type,
                    'enclosur_user': {
                        'user_id': e.enclosure_user.id,
                        'user_name': e.enclosure_user.first_name,
                        'login_name': e.enclosure_user.username,
                        'colour': e.enclosure_user.user_icon.pure_color,
                        'remark': friend.remark,
                        'ismyfriend': 1
                    },
                    'enclosure_colour': e.colour,
                    'is_deleteed': e.is_deleteed,
                    'created_time': e.created_time.strftime('%Y-%m-%d %H:%M:%S')
                })
            else:
                data.append({
                    'enclosure_id': e.id,
                    'enclosure_name': e.name,
                    'enclosure_size': e.size,
                    'enclosure_url': e.get_file_url(),
                    'enclosure_type': e.file_type,
                    'enclosur_user': {
                        'user_id': e.enclosure_user.id,
                        'user_name': e.enclosure_user.first_name,
                        'login_name': e.enclosure_user.username,
                        'colour': e.enclosure_user.user_icon.pure_color,
                        'ismyfriend': 0
                    },
                    'enclosure_colour': e.colour,
                    'is_deleteed': e.is_deleteed,
                    'created_time': e.created_time.strftime('%Y-%m-%d %H:%M:%S')
                })
        # 返回结果
        res_data = {
            "code": 0,
            "msg": "SUCCESS",
            "data": data
        }
        return JsonResponse(res_data, safe=False)

    """ 
        @apiName delete-enclosures
        @api {DELETE} /api/v1/groupchats/{groupchat_id}/enclosures/{enclosure_id}
        @apiGroup chat-enclosures
        @apiVersion 0.0.1
        @apiDescription [聊天]删除某一个附件
        @apiParam {int} groupchat_id 群聊或者私聊的id 
        @apiParam {int} enclosure_id 附件的id    
        @apiSuccess {String} msg 信息
        @apiSuccess {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 成功返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
        }
    """

    # 删除某一个附件

    def delete(self, request, groupchat_id, enclosure_id):

        gc = GroupChat.objects.filter(id=groupchat_id)
        if not gc:
            res_data = {
                        "msg": "你还不是他（她）的好友，请先加对方为好友"
            }
            return JsonResponse(res_data, safe=False, status=400)

        if groupchat_id and enclosure_id:  # 如果id存在
            Enclosure.objects.filter(Q(id=enclosure_id) & Q(groupchats__id=groupchat_id)).update(is_deleteed=1)
        else:  # 如果数据不存在，忽略不计
            pass
        res_data = {
            "code": 0,
            "msg": "SUCCESS",
        }
        return JsonResponse(res_data, safe=False)
