import json
import math
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.db.models import Q
from django.http import JsonResponse
from django.views import View
from rest_framework.exceptions import NotAcceptable, PermissionDenied
from rest_framework.views import APIView

from apps.chat.models import Message, GroupChat, MessagePart, Myfriend, GroupChatPart


# 对消息的增删改查
class Messages(View):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        """ 
            @apiName save-messages
            @api {POST} /api/v1/groupchats/messages
            @apiGroup chat-messages
            @apiVersion 0.0.1
            @apiDescription [聊天]保存聊天消息
            @apiParam {list} groupchatlist 所属群聊或者私聊列表 
            @apiParam {string} text_content 消息内容
            @apiParam {string} message_type 消息类型:{("message_normal","正常消息"),("message_groupcard","群名片"),("message_usercard","用户名片"),("message_task","任务")}
            @apiParam {int} usercard_id 分享名片上的人的id  
            @apiParam {int} groupcard_id 分享群名片上的群的id  
            @apiParam {int} task_id 消息中任务的id  
            @apiSuccess {String} msg 信息
            @apiSuccess {String} code 0代表无错误 1代表有错误
            @apiSuccessExample {json} 成功返回样例:
            {
                "code": 0,
                "msg": "SUCCESS",
            }
        """

    def post(self, request):

        # 输入消息内容
        global  data1, data
        log_user = request.user
        put_data = json.loads(request.body)
        groupchatlist = put_data.get('groupchatlist')
        text_content = put_data.get('text_content')
        message_type = put_data.get('message_type')
        usercard_id = put_data.get('usercard_id')
        groupcard_id = put_data.get('groupcard_id')
        task_id = put_data.get('task_id')

        # 校验用户和参数
        if not text_content:  # 如果没有输入消息
            res_data = {
                "msg": "参数缺失",
            }
            return JsonResponse(res_data, safe=False, status=406)


        # 执行逻辑
        if text_content:  # 如果消息已经输入,进行消息的保存。
            for groupchat_id in groupchatlist:
                gc=GroupChat.objects.filter(id=groupchat_id)
                if not gc :
                    res_data = {
                        "errer":{"msg":"你还不是他（她）的好友，请先加对方为好友"},
                    }
                    return JsonResponse(res_data, safe=False,status=400)
                groupchatpart=GroupChatPart.objects.filter(groupchat_id=groupchat_id)
                if groupchatpart:
                    groupchatpart.delete()#如果消息发送的群，在清空记录里面，那么清空记录里所有关于这个群的记录都删除

                # 保存消息
                m = Message()
                m.text_content = text_content  # 消息内容
                m.user_id = log_user.id  # 消息发送人
                m.groupchat_id = groupchat_id  # 所属聊天
                m.message_type = message_type  # 消息类型
                m.usercard_id = usercard_id  # 分享的个人名片
                m.groupcard_id = groupcard_id  # 分享的群名片
                m.task_id = task_id  # 分享的任务
                m.save()  # 保存新的消息
                data1 = {"message_id": m.id, }

                # 建立websoket链接
                data={}
                friend = Myfriend.objects.filter(myfriendslist__id=m.user.id).first()
                friend1 = Myfriend.objects.filter(myfriendslist__id=m.usercard_id).first()
                if friend and friend1:
                    remark = Myfriend.objects.filter(myfriendslist__id=m.user.id).first().remark
                    remark1 = Myfriend.objects.filter(myfriendslist__id=m.usercard_id).first().remark
                    if m.message_type == "message_normal":  # 如果是普通消息，则展示如下内容：
                        data={
                            "message_id": m.id,
                            'groupchat_id': m.groupchat.id,
                            'groupchat_name': m.groupchat.groupchat_name,
                            "text_content": {"content": m.text_content, "isreaded": m.isreaded, "istop": m.istop,
                                             "message_type": m.message_type, "send_time": m.send_time.strftime('%Y-%m-%d %H:%M:%S'), },
                            "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                       "login_name": m.user.username, "remark": remark,
                                       "colour": m.user.user_icon.pure_color, "ismyfriend": 1},
                            'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                        }
                    if m.message_type == "message_usercard":  # 如果是分享个人名片，则展示如下内容：
                        data={
                            "message_id": m.id,
                            'groupchat_id': m.groupchat.id,
                            'groupchat_name': m.groupchat.groupchat_name,
                            "text_content": {
                                "user_card": {"usercard_id": m.usercard_id, "user_name": m.usercard.first_name,
                                              "login_name": m.usercard.username, "remark1": remark1,
                                              "ismyfriend": 1,
                                              "colour": m.usercard.user_icon.pure_color},
                                "isreaded": m.isreaded, "istop": m.istop, "message_type": m.message_type,
                                "send_time": m.send_time.strftime('%Y-%m-%d %H:%M:%S')},
                            "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                       "login_name": m.user.username, "remark": remark,
                                       "colour": m.user.user_icon.pure_color, "ismyfriend": 1},
                            'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                        }

                    if m.message_type == "message_groupcard":  # 如果是分享群名片，则展示如下内容：
                        data={
                            "message_id": m.id,
                            'groupchat_id': m.groupchat.id,
                            'groupchat_name': m.groupchat.groupchat_name,
                            "text_content": {"group_card": {"groupcard_id": m.groupcard_id,
                                                            "groupchat_name": m.groupcard.groupchat_name,
                                                            },
                                             "isreaded": m.isreaded, "istop": m.istop,
                                             "message_type": m.message_type,
                                             "send_time": m.send_time.strftime('%Y-%m-%d %H:%M:%S')},
                            "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                       "login_name": m.user.username, "remark": remark,
                                       "colour": m.user.user_icon.pure_color, "ismyfriend": 1},
                            'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                        }

                    if m.message_type == "message_task":  # 如果是分配的任务，则展示如下内容：
                        data={
                            "message_id": m.id,
                            'groupchat_id': m.groupchat.id,
                            'groupchat_name': m.groupchat.groupchat_name,
                            "text_content": {"task_content": {"task_id": m.task.id, "task_name": m.task.task_name,
                                                              "task_img": m.task.get_img_url(),
                                                              "task_url": m.task.task_url,
                                                              "application_name": m.task.application_task.name,
                                                              "application_icon": m.task.application_task.get_logo()},
                                             "isreaded": m.isreaded, "istop": m.istop,
                                             "message_type": m.message_type,
                                             "send_time": m.send_time.strftime('%Y-%m-%d %H:%M:%S')},
                            "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                       "login_name": m.user.username, "remark": remark,
                                       "colour": m.user.user_icon.pure_color, "ismyfriend": 1},
                            'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                        }

                elif friend and friend1 is None:
                    remark = Myfriend.objects.filter(myfriendslist__id=m.user.id).first().remark
                    if m.message_type == "message_normal":  # 如果是普通消息，则展示如下内容：
                        data = {
                            "message_id": m.id,
                            'groupchat_id': m.groupchat.id,
                            'groupchat_name': m.groupchat.groupchat_name,
                            "text_content": {"content": m.text_content, "isreaded": m.isreaded, "istop": m.istop,
                                             "message_type": m.message_type, "send_time": m.send_time.strftime('%Y-%m-%d %H:%M:%S')},
                            "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                       "login_name": m.user.username, "remark": remark,
                                       "colour": m.user.user_icon.pure_color, "ismyfriend": 1},
                            'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                        }
                    if m.message_type == "message_usercard":  # 如果是分享个人名片，则展示如下内容：
                        data={
                            "message_id": m.id,
                            'groupchat_id': m.groupchat.id,
                            'groupchat_name': m.groupchat.groupchat_name,
                            "text_content": {
                                "user_card": {"usercard_id": m.usercard_id, "user_name": m.usercard.first_name,
                                              "login_name": m.usercard.username,
                                              "ismyfriend": 0,
                                              "colour": m.usercard.user_icon.pure_color},
                                "isreaded": m.isreaded, "istop": m.istop, "message_type": m.message_type,
                                "send_time": m.send_time.strftime('%Y-%m-%d %H:%M:%S')},
                            "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                       "login_name": m.user.username, "remark": remark,
                                       "colour": m.user.user_icon.pure_color, "ismyfriend": 1},
                            'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                        }

                    if m.message_type == "message_groupcard":  # 如果是分享群名片，则展示如下内容：
                        data={
                            "message_id": m.id,
                            'groupchat_id': m.groupchat.id,
                            'groupchat_name': m.groupchat.groupchat_name,
                            "text_content": {"group_card": {"groupcard_id": m.groupcard_id,
                                                            "groupchat_name": m.groupcard.groupchat_name,
                                                            },
                                             "isreaded": m.isreaded, "istop": m.istop,
                                             "message_type": m.message_type,
                                             "send_time": m.send_time.strftime('%Y-%m-%d %H:%M:%S')},
                            "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                       "login_name": m.user.username, "remark": remark,
                                       "colour": m.user.user_icon.pure_color, "ismyfriend": 1},
                            'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                        }

                    if m.message_type == "message_task":  # 如果是分配的任务，则展示如下内容：
                        data = {
                            "message_id": m.id,
                            'groupchat_id': m.groupchat.id,
                            'groupchat_name': m.groupchat.groupchat_name,
                            "text_content": {"task_content": {"task_id": m.task.id, "task_name": m.task.task_name,
                                                              "task_img": m.task.get_img_url(),
                                                              "task_url": m.task.task_url,
                                                              "application_name": m.task.application_task.name,
                                                              "application_icon": m.task.application_task.get_logo()},
                                             "isreaded": m.isreaded, "istop": m.istop,
                                             "message_type": m.message_type,
                                             "send_time": m.send_time.strftime('%Y-%m-%d %H:%M:%S')},
                            "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                       "login_name": m.user.username, "remark": remark,
                                       "colour": m.user.user_icon.pure_color, "ismyfriend": 1},
                            'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                        }
                elif friend is None and friend1:
                    remark1 = Myfriend.objects.filter(myfriendslist__id=m.usercard_id).first().remark
                    if m.message_type == "message_normal":  # 如果是普通消息，则展示如下内容：
                        data={
                            "message_id": m.id,
                            'groupchat_id': m.groupchat.id,
                            'groupchat_name': m.groupchat.groupchat_name,
                            "text_content": {"content": m.text_content, "isreaded": m.isreaded, "istop": m.istop,
                                             "message_type": m.message_type, "send_time": m.send_time.strftime('%Y-%m-%d %H:%M:%S')},
                            "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                       "login_name": m.user.username,
                                       "colour": m.user.user_icon.pure_color, "ismyfriend": 0},
                            'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                        }
                    if m.message_type == "message_usercard":  # 如果是分享个人名片，则展示如下内容：
                        data={
                            "message_id": m.id,
                            'groupchat_id': m.groupchat.id,
                            'groupchat_name': m.groupchat.groupchat_name,
                            "text_content": {
                                "user_card": {"usercard_id": m.usercard_id, "user_name": m.usercard.first_name,
                                              "login_name": m.usercard.username, "remark1": remark1,
                                              "ismyfriend": 1,
                                              "colour": m.usercard.user_icon.pure_color},
                                "isreaded": m.isreaded, "istop": m.istop, "message_type": m.message_type,
                                "send_time": m.send_time.strftime('%Y-%m-%d %H:%M:%S')},
                            "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                       "login_name": m.user.username,
                                       "colour": m.user.user_icon.pure_color, "ismyfriend": 0},
                            'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                        }

                    if m.message_type == "message_groupcard":  # 如果是分享群名片，则展示如下内容：
                        data={
                            "message_id": m.id,
                            'groupchat_id': m.groupchat.id,
                            'groupchat_name': m.groupchat.groupchat_name,
                            "text_content": {"group_card": {"groupcard_id": m.groupcard_id,
                                                            "groupchat_name": m.groupcard.groupchat_name,
                                                            },
                                             "isreaded": m.isreaded, "istop": m.istop,
                                             "message_type": m.message_type,
                                             "send_time": m.send_time.strftime('%Y-%m-%d %H:%M:%S')},
                            "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                       "login_name": m.user.username,
                                       "colour": m.user.user_icon.pure_color, "ismyfriend": 0},
                            'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                        }

                    if m.message_type == "message_task":  # 如果是分配的任务，则展示如下内容：
                        data={
                            "message_id": m.id,
                            'groupchat_id': m.groupchat.id,
                            'groupchat_name': m.groupchat.groupchat_name,
                            "text_content": {"task_content": {"task_id": m.task.id, "task_name": m.task.task_name,
                                                              "task_img": m.task.get_img_url(),
                                                              "task_url": m.task.task_url,
                                                              "application_name": m.task.application_task.name,
                                                              "application_icon": m.task.application_task.get_logo()},
                                             "isreaded": m.isreaded, "istop": m.istop,
                                             "message_type": m.message_type,
                                             "send_time": m.send_time.strftime('%Y-%m-%d %H:%M:%S')},
                            "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                       "login_name": m.user.username,
                                       "colour": m.user.user_icon.pure_color, "ismyfriend": 0},
                            'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                        }

                else:
                    if m.message_type == "message_normal":  # 如果是普通消息，则展示如下内容：
                        data={
                            "message_id": m.id,
                            'groupchat_id': m.groupchat.id,
                            'groupchat_name': m.groupchat.groupchat_name,
                            "text_content": {"content": m.text_content, "isreaded": m.isreaded, "istop": m.istop,
                                             "message_type": m.message_type, "send_time": m.send_time.strftime('%Y-%m-%d %H:%M:%S')},
                            "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                       "login_name": m.user.username,
                                       "colour": m.user.user_icon.pure_color, "ismyfriend": 0},
                            'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                        }
                    if m.message_type == "message_usercard":  # 如果是分享个人名片，则展示如下内容：
                        data={
                            "message_id": m.id,
                            'groupchat_id': m.groupchat.id,
                            'groupchat_name': m.groupchat.groupchat_name,
                            "text_content": {
                                "user_card": {"usercard_id": m.usercard_id, "user_name": m.usercard.first_name,
                                              "login_name": m.usercard.username,
                                              "ismyfriend": 0,
                                              "colour": m.usercard.user_icon.pure_color},
                                "isreaded": m.isreaded, "istop": m.istop, "message_type": m.message_type,
                                "send_time": m.send_time.strftime('%Y-%m-%d %H:%M:%S')},
                            "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                       "login_name": m.user.username,
                                       "colour": m.user.user_icon.pure_color, "ismyfriend": 0},
                            'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                        }

                    if m.message_type == "message_groupcard":  # 如果是分享群名片，则展示如下内容：
                        data={
                            "message_id": m.id,
                            'groupchat_id': m.groupchat.id,
                            'groupchat_name': m.groupchat.groupchat_name,
                            "text_content": {"group_card": {"groupcard_id": m.groupcard_id,
                                                            "groupchat_name": m.groupcard.groupchat_name,
                                                            },
                                             "isreaded": m.isreaded, "istop": m.istop,
                                             "message_type": m.message_type,
                                             "send_time": m.send_time.strftime('%Y-%m-%d %H:%M:%S')},
                            "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                       "login_name": m.user.username,
                                       "colour": m.user.user_icon.pure_color, "ismyfriend": 0},
                            'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                        }

                    if m.message_type == "message_task":  # 如果是分配的任务，则展示如下内容：
                        data={
                            "message_id": m.id,
                            'groupchat_id': m.groupchat.id,
                            'groupchat_name': m.groupchat.groupchat_name,
                            "text_content": {"task_content": {"task_id": m.task.id, "task_name": m.task.task_name,
                                                              "task_img": m.task.get_img_url(),
                                                              "task_url": m.task.task_url,
                                                              "application_name": m.task.application_task.name,
                                                              "application_icon": m.task.application_task.get_logo()},
                                             "isreaded": m.isreaded, "istop": m.istop,
                                             "message_type": m.message_type,
                                             "send_time": m.send_time.strftime('%Y-%m-%d %H:%M:%S')},
                            "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                       "login_name": m.user.username,
                                       "colour": m.user.user_icon.pure_color, "ismyfriend": 0},
                            'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                        }

                channel_layer = get_channel_layer()
                groupchat = GroupChat.objects.filter(id=groupchat_id).first()  # 群组存在则筛选得到群组！
                members = groupchat.groupchat_members.all()  # 查看群组中群中所有成员
                members_ids = []
                for me in members:
                    members_ids.append(me.id)
                for user_id in members_ids:
                        async_to_sync(channel_layer.group_send)(
                            "user_%s" % user_id,
                            {
                                'type': 'chat_message',
                                'data': {
                                    "type": "chat",
                                    "chat": data,
                                },
                            })

                # 返回结果
            res_data = {
                "code": 0,
                "msg": "SUCCESS",
                "data": data1,
            }
            return JsonResponse(res_data, safe=False)

    """ 
        @apiName clear-messages
        @api {DELETE} /api/v1/groupchats/{groupchat_id}/messages
        @apiGroup chat-messages
        @apiVersion 0.0.1
        @apiDescription [聊天]清空聊天记录
        @apiParam {int} groupchat_id 所属群聊或者私聊id 
        @apiParam {int} message_id 请空前最后一条消息的id
        @apiSuccess {String} msg 信息
        @apiSuccess {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 成功返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
        }
    """

    # 清空聊天记录
    def delete(self, request, groupchat_id):

        # 获取参数
        log_user = request.user
        put_data = json.loads(request.body)
        message_id = put_data.get('message_id')  # 获取最后一条消息的id

        # 校验用户和参数
        if not message_id:  # 如果没有输入消息
            res_data = {
                "msg": "参数缺失",
            }
            return JsonResponse(res_data, safe=False, status=406)

        # 新建删除记录
        MessagePart.objects.create(groupchat_id=groupchat_id, user_id=log_user.id, lastmessage_id=message_id)
        # 返回结果
        res_data = {
            "code": 0,
            "msg": "SUCCESS",
        }
        return JsonResponse(res_data, safe=False)

    """
        @apiName show-messages
        @api {GET} /api/v1/groupchats/{groupchat_id}/messages
        @apiGroup chat-messages
        @apiVersion 0.0.1
        @apiDescription [聊天]点击聊天的头像，展示聊天的内容，同时进行聊天记录筛选
        @apiParam {int} groupchat_id 所属群聊或者私聊id 
        @apiParam {int} offset 页码 ，分页的起始页
        @apiParam {int} limit  每页展示的消息条数
        @apiParam {string} message_content 想要搜索的内容
        @apiSuccess {String} msg 信息
        @apiSuccess {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 返回样例:             
        {
            "code": 0,
            "msg": "SUCCESS",
            "data": 
            [
                {
                    "message_id": 2,
                    "groupchat_name": "团队扩展群",
                    "text_content": {
                        "user_card": {
                            "usercard_id": 2,
                            "user_name": "林奇",
                            "login_name": "linqi",
                            "remark1": "技术负责人",
                            "colour": "#5F9EA0"
                        },
                        "isreaded": false,
                        "istop": false,
                        "message_type": "message_usercard",
                        "send_time": "2020-08-28T14:51:33Z"
                    },
                    "sender": {
                        "user_id": 7,
                        "user_name": "李宁",
                        "login_name": "lining  ",
                        "remark": null,
                        "colour": "#5F9EA0",
                        "ismyfriend": 1
                    },
                    "send_time": "2020-08-28 14:51:33"
                },
            ]
        }

    """

    # 点击聊天头像，获取消息内容,同时进行聊天记录筛选
    def get(self, request, groupchat_id):

        # 获取参数
        data = []
        log_user=request.user
        message_content = request.GET.get('message_content')  # 获取要搜索的内容
        offset = request.GET.get("offset")  # 分页的初始页0
        limit = request.GET.get("limit")  # 每页展示的消息数量10

        # 2 校验参数
        try:
            offset = int(offset)
            limit = int(limit)
        except:
            res_data = {
                "msg": "参数必须为整数",
            }
            return JsonResponse(res_data, safe=False, status=400)

        gc = GroupChat.objects.filter(id=groupchat_id)
        if not gc:
            res_data = {
                "msg": "你还不是他（她）的好友，请先加对方为好友",
            }
            return JsonResponse(res_data, safe=False, status=400)

        if groupchat_id and message_content:  # 获取群组和要搜索的内容，即使曾清空聊天记录的消息 ，也应该显示出来
            groupchat = GroupChat.objects.filter(id=groupchat_id).first()  # 找到群组
            total = groupchat.messagegroupchat.all().order_by('-send_time').count()





            m1=MessagePart.objects.filter(groupchat_id=groupchat_id,user_id=log_user.id).first()
            if m1:  # 如果有聊天并且有执行清空的用户
                lastmessage_id = MessagePart.objects.filter(groupchat_id=groupchat_id,
                                                            user_id=log_user.id).last().lastmessage_id
                messages = groupchat.messagegroupchat.filter(
                    (Q(text_content__icontains=message_content)|Q(usercard__first_name__icontains=message_content) | Q(
                        groupcard__groupchat_name__icontains=message_content) | Q(
                        task__task_name__icontains=message_content))&Q(id__gt=lastmessage_id)).order_by(
                    '-send_time')[offset: offset + limit]  # 以关键字筛选获取信息内容[offset: offset + limit]

                # 以关键字筛选获取信息内容
                for m in messages:  # 遍历消息
                    friend = Myfriend.objects.filter(myfriendslist__id=m.user.id).first()
                    friend1 = Myfriend.objects.filter(myfriendslist__id=m.usercard_id).first()
                    if friend and friend1:
                        remark = Myfriend.objects.filter(myfriendslist__id=m.user.id).first().remark
                        remark1 = Myfriend.objects.filter(myfriendslist__id=m.usercard_id).first().remark
                        if m.message_type == "message_normal":  # 如果是普通消息，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {"content": m.text_content, "isreaded": m.isreaded, "istop": m.istop,
                                                 "message_type": m.message_type, "send_time": m.send_time, },
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username, "remark": remark,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 1},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })
                        if m.message_type == "message_usercard":  # 如果是分享个人名片，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {
                                    "user_card": {"usercard_id": m.usercard_id, "user_name": m.usercard.first_name,
                                                  "login_name": m.usercard.username, "remark1": remark1,
                                                  "ismyfriend": 1,
                                                  "colour": m.usercard.user_icon.pure_color},
                                    "isreaded": m.isreaded, "istop": m.istop, "message_type": m.message_type,
                                    "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username, "remark": remark,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 1},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })

                        if m.message_type == "message_groupcard":  # 如果是分享群名片，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {"group_card": {"groupcard_id": m.groupcard_id,
                                                                "groupchat_name": m.groupcard.groupchat_name,
                                                                },
                                                 "isreaded": m.isreaded, "istop": m.istop,
                                                 "message_type": m.message_type,
                                                 "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username, "remark": remark,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 1},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })

                        if m.message_type == "message_task":  # 如果是分配的任务，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {"task_content": {"task_id": m.task.id, "task_name": m.task.task_name,
                                                                  "task_img": m.task.get_img_url(),
                                                                  "task_url": m.task.task_url,
                                                                  "application_name": m.task.application_task.application_name,
                                                                  "application_icon": m.task.application_task.get_icon_url()},
                                                 "isreaded": m.isreaded, "istop": m.istop,
                                                 "message_type": m.message_type,
                                                 "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username, "remark": remark,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 1},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })
                    elif friend and friend1 is None:
                        remark = Myfriend.objects.filter(myfriendslist__id=m.user.id).first().remark
                        # remark1 = Myfriend.objects.filter(myfriendslist__id=m.usercard_id).first().remark
                        if m.message_type == "message_normal":  # 如果是普通消息，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {"content": m.text_content, "isreaded": m.isreaded, "istop": m.istop,
                                                 "message_type": m.message_type, "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username, "remark": remark,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 1},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })
                        if m.message_type == "message_usercard":  # 如果是分享个人名片，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {
                                    "user_card": {"usercard_id": m.usercard_id, "user_name": m.usercard.first_name,
                                                  "login_name": m.usercard.username,
                                                  "ismyfriend": 0,
                                                  "colour": m.usercard.user_icon.pure_color},
                                    "isreaded": m.isreaded, "istop": m.istop, "message_type": m.message_type,
                                    "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username, "remark": remark,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 1},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })

                        if m.message_type == "message_groupcard":  # 如果是分享群名片，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {"group_card": {"groupcard_id": m.groupcard_id,
                                                                "groupchat_name": m.groupcard.groupchat_name,
                                                                },
                                                 "isreaded": m.isreaded, "istop": m.istop,
                                                 "message_type": m.message_type,
                                                 "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username, "remark": remark,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 1},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })

                        if m.message_type == "message_task":  # 如果是分配的任务，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {"task_content": {"task_id": m.task.id, "task_name": m.task.task_name,
                                                                  "task_img": m.task.get_img_url(),
                                                                  "task_url": m.task.task_url,
                                                                  "application_name": m.task.application_task.application_name,
                                                                  "application_icon": m.task.application_task.get_icon_url()},
                                                 "isreaded": m.isreaded, "istop": m.istop,
                                                 "message_type": m.message_type,
                                                 "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username, "remark": remark,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 1},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })

                    elif friend is None and friend1:
                        remark1 = Myfriend.objects.filter(myfriendslist__id=m.usercard_id).first().remark
                        if m.message_type == "message_normal":  # 如果是普通消息，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {"content": m.text_content, "isreaded": m.isreaded, "istop": m.istop,
                                                 "message_type": m.message_type, "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 0},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })
                        if m.message_type == "message_usercard":  # 如果是分享个人名片，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {
                                    "user_card": {"usercard_id": m.usercard_id, "user_name": m.usercard.first_name,
                                                  "login_name": m.usercard.username, "remark1": remark1,
                                                  "ismyfriend": 1,
                                                  "colour": m.usercard.user_icon.pure_color},
                                    "isreaded": m.isreaded, "istop": m.istop, "message_type": m.message_type,
                                    "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 0},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })

                        if m.message_type == "message_groupcard":  # 如果是分享群名片，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {"group_card": {"groupcard_id": m.groupcard_id,
                                                                "groupchat_name": m.groupcard.groupchat_name,
                                                                },
                                                 "isreaded": m.isreaded, "istop": m.istop,
                                                 "message_type": m.message_type,
                                                 "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 0},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })

                        if m.message_type == "message_task":  # 如果是分配的任务，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {"task_content": {"task_id": m.task.id, "task_name": m.task.task_name,
                                                                  "task_img": m.task.get_img_url(),
                                                                  "task_url": m.task.task_url,
                                                                  "application_name": m.task.application_task.application_name,
                                                                  "application_icon": m.task.application_task.get_icon_url()},
                                                 "isreaded": m.isreaded, "istop": m.istop,
                                                 "message_type": m.message_type,
                                                 "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 0},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })


                    else:
                        if m.message_type == "message_normal":  # 如果是普通消息，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {"content": m.text_content, "isreaded": m.isreaded, "istop": m.istop,
                                                 "message_type": m.message_type, "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 0},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })
                        if m.message_type == "message_usercard":  # 如果是分享个人名片，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {
                                    "user_card": {"usercard_id": m.usercard_id, "user_name": m.usercard.first_name,
                                                  "login_name": m.usercard.username,
                                                  "ismyfriend": 0,
                                                  "colour": m.usercard.user_icon.pure_color},
                                    "isreaded": m.isreaded, "istop": m.istop, "message_type": m.message_type,
                                    "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 0},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })

                        if m.message_type == "message_groupcard":  # 如果是分享群名片，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {"group_card": {"groupcard_id": m.groupcard_id,
                                                                "groupchat_name": m.groupcard.groupchat_name,
                                                                },
                                                 "isreaded": m.isreaded, "istop": m.istop,
                                                 "message_type": m.message_type,
                                                 "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 0},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })

                        if m.message_type == "message_task":  # 如果是分配的任务，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {"task_content": {"task_id": m.task.id, "task_name": m.task.task_name,
                                                                  "task_img": m.task.get_img_url(),
                                                                  "task_url": m.task.task_url,
                                                                  "application_name": m.task.application_task.application_name,
                                                                  "application_icon": m.task.application_task.get_icon_url()},
                                                 "isreaded": m.isreaded, "istop": m.istop,
                                                 "message_type": m.message_type,
                                                 "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 0},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })

                # 返回结果
                res_data = {
                    "code": 0,
                    "msg": "SUCCESS",
                    "message_total": total,
                    "data": data
                }
                return JsonResponse(res_data, safe=False)

            else:





                messages = groupchat.messagegroupchat.filter(
                    Q(text_content__icontains=message_content) | Q(usercard__first_name__icontains=message_content) | Q(
                        groupcard__groupchat_name__icontains=message_content) | Q(
                        task__task_name__icontains=message_content)).order_by(
                    '-send_time')[offset: offset + limit]  # 以关键字筛选获取信息内容[offset: offset + limit]

                # 以关键字筛选获取信息内容
                for m in messages:  # 遍历消息
                    friend = Myfriend.objects.filter(myfriendslist__id=m.user.id).first()
                    friend1 = Myfriend.objects.filter(myfriendslist__id=m.usercard_id).first()
                    if friend and friend1:
                        remark = Myfriend.objects.filter(myfriendslist__id=m.user.id).first().remark
                        remark1 = Myfriend.objects.filter(myfriendslist__id=m.usercard_id).first().remark
                        if m.message_type == "message_normal":  # 如果是普通消息，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {"content": m.text_content, "isreaded": m.isreaded, "istop": m.istop,
                                                 "message_type": m.message_type, "send_time": m.send_time, },
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username, "remark": remark,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 1},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })
                        if m.message_type == "message_usercard":  # 如果是分享个人名片，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {
                                    "user_card": {"usercard_id": m.usercard_id, "user_name": m.usercard.first_name,
                                                  "login_name": m.usercard.username, "remark1": remark1,
                                                  "ismyfriend": 1,
                                                  "colour": m.usercard.user_icon.pure_color},
                                    "isreaded": m.isreaded, "istop": m.istop, "message_type": m.message_type,
                                    "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username, "remark": remark,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 1},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })

                        if m.message_type == "message_groupcard":  # 如果是分享群名片，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {"group_card": {"groupcard_id": m.groupcard_id,
                                                                "groupchat_name": m.groupcard.groupchat_name,
                                                                },
                                                 "isreaded": m.isreaded, "istop": m.istop,
                                                 "message_type": m.message_type,
                                                 "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username, "remark": remark,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 1},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })

                        if m.message_type == "message_task":  # 如果是分配的任务，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {"task_content": {"task_id": m.task.id, "task_name": m.task.task_name,
                                                                  "task_img": m.task.get_img_url(),
                                                                  "task_url": m.task.task_url,
                                                                  "application_name": m.task.application_task.application_name,
                                                                  "application_icon": m.task.application_task.get_icon_url()},
                                                 "isreaded": m.isreaded, "istop": m.istop,
                                                 "message_type": m.message_type,
                                                 "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username, "remark": remark,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 1},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })
                    elif friend and friend1 is None:
                        remark = Myfriend.objects.filter(myfriendslist__id=m.user.id).first().remark
                        # remark1 = Myfriend.objects.filter(myfriendslist__id=m.usercard_id).first().remark
                        if m.message_type == "message_normal":  # 如果是普通消息，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {"content": m.text_content, "isreaded": m.isreaded, "istop": m.istop,
                                                 "message_type": m.message_type, "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username, "remark": remark,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 1},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })
                        if m.message_type == "message_usercard":  # 如果是分享个人名片，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {
                                    "user_card": {"usercard_id": m.usercard_id, "user_name": m.usercard.first_name,
                                                  "login_name": m.usercard.username,
                                                  "ismyfriend": 0,
                                                  "colour": m.usercard.user_icon.pure_color},
                                    "isreaded": m.isreaded, "istop": m.istop, "message_type": m.message_type,
                                    "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username, "remark": remark,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 1},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })

                        if m.message_type == "message_groupcard":  # 如果是分享群名片，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {"group_card": {"groupcard_id": m.groupcard_id,
                                                                "groupchat_name": m.groupcard.groupchat_name,
                                                                },
                                                 "isreaded": m.isreaded, "istop": m.istop,
                                                 "message_type": m.message_type,
                                                 "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username, "remark": remark,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 1},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })

                        if m.message_type == "message_task":  # 如果是分配的任务，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {"task_content": {"task_id": m.task.id, "task_name": m.task.task_name,
                                                                  "task_img": m.task.get_img_url(),
                                                                  "task_url": m.task.task_url,
                                                                  "application_name": m.task.application_task.application_name,
                                                                  "application_icon": m.task.application_task.get_icon_url()},
                                                 "isreaded": m.isreaded, "istop": m.istop,
                                                 "message_type": m.message_type,
                                                 "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username, "remark": remark,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 1},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })

                    elif friend is None and friend1:
                        remark1 = Myfriend.objects.filter(myfriendslist__id=m.usercard_id).first().remark
                        if m.message_type == "message_normal":  # 如果是普通消息，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {"content": m.text_content, "isreaded": m.isreaded, "istop": m.istop,
                                                 "message_type": m.message_type, "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 0},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })
                        if m.message_type == "message_usercard":  # 如果是分享个人名片，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {
                                    "user_card": {"usercard_id": m.usercard_id, "user_name": m.usercard.first_name,
                                                  "login_name": m.usercard.username, "remark1": remark1,
                                                  "ismyfriend": 1,
                                                  "colour": m.usercard.user_icon.pure_color},
                                    "isreaded": m.isreaded, "istop": m.istop, "message_type": m.message_type,
                                    "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 0},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })

                        if m.message_type == "message_groupcard":  # 如果是分享群名片，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {"group_card": {"groupcard_id": m.groupcard_id,
                                                                "groupchat_name": m.groupcard.groupchat_name,
                                                                },
                                                 "isreaded": m.isreaded, "istop": m.istop,
                                                 "message_type": m.message_type,
                                                 "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 0},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })

                        if m.message_type == "message_task":  # 如果是分配的任务，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {"task_content": {"task_id": m.task.id, "task_name": m.task.task_name,
                                                                  "task_img": m.task.get_img_url(),
                                                                  "task_url": m.task.task_url,
                                                                  "application_name": m.task.application_task.application_name,
                                                                  "application_icon": m.task.application_task.get_icon_url()},
                                                 "isreaded": m.isreaded, "istop": m.istop,
                                                 "message_type": m.message_type,
                                                 "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 0},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })


                    else:
                        if m.message_type == "message_normal":  # 如果是普通消息，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {"content": m.text_content, "isreaded": m.isreaded, "istop": m.istop,
                                                 "message_type": m.message_type, "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 0},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })
                        if m.message_type == "message_usercard":  # 如果是分享个人名片，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {
                                    "user_card": {"usercard_id": m.usercard_id, "user_name": m.usercard.first_name,
                                                  "login_name": m.usercard.username,
                                                  "ismyfriend": 0,
                                                  "colour": m.usercard.user_icon.pure_color},
                                    "isreaded": m.isreaded, "istop": m.istop, "message_type": m.message_type,
                                    "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 0},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })

                        if m.message_type == "message_groupcard":  # 如果是分享群名片，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {"group_card": {"groupcard_id": m.groupcard_id,
                                                                "groupchat_name": m.groupcard.groupchat_name,
                                                                },
                                                 "isreaded": m.isreaded, "istop": m.istop,
                                                 "message_type": m.message_type,
                                                 "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 0},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })

                        if m.message_type == "message_task":  # 如果是分配的任务，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {"task_content": {"task_id": m.task.id, "task_name": m.task.task_name,
                                                                  "task_img": m.task.get_img_url(),
                                                                  "task_url": m.task.task_url,
                                                                  "application_name": m.task.application_task.application_name,
                                                                  "application_icon": m.task.application_task.get_icon_url()},
                                                 "isreaded": m.isreaded, "istop": m.istop,
                                                 "message_type": m.message_type,
                                                 "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 0},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })

                # 返回结果
                res_data = {
                    "code": 0,
                    "msg": "SUCCESS",
                    "message_total": total,
                    "data": data
                }
                return JsonResponse(res_data, safe=False)

        if groupchat_id:  # 如果只是群组聊天

            log_user = request.user
            groupchat = GroupChat.objects.filter(id=groupchat_id).first()  # 找到群组
            total = groupchat.messagegroupchat.all().order_by('-send_time').count()

            m1=MessagePart.objects.filter(groupchat_id=groupchat_id,user_id=log_user.id).first()
            if m1:  # 如果有聊天并且有执行清空的用户
                lastmessage_id = MessagePart.objects.filter(groupchat_id=groupchat_id,user_id=log_user.id).last().lastmessage_id
                messages = Message.objects.filter(groupchat_id=groupchat_id,id__gt=lastmessage_id).order_by(
                    '-send_time')[offset: offset + limit]  # 展示全部消息[offset: offset + limit]


                # 以关键字筛选获取信息内容
                for m in messages:  # 遍历消息
                    friend = Myfriend.objects.filter(myfriendslist__id=m.user.id).first()
                    friend1 = Myfriend.objects.filter(myfriendslist__id=m.usercard_id).first()
                    if friend and friend1:
                        remark = Myfriend.objects.filter(myfriendslist__id=m.user.id).first().remark
                        remark1 = Myfriend.objects.filter(myfriendslist__id=m.usercard_id).first().remark
                        if m.message_type == "message_normal":  # 如果是普通消息，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {"content": m.text_content, "isreaded": m.isreaded, "istop": m.istop,
                                                 "message_type": m.message_type, "send_time": m.send_time, },
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username, "remark": remark,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 1},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })
                        if m.message_type == "message_usercard":  # 如果是分享个人名片，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {
                                    "user_card": {"usercard_id": m.usercard_id, "user_name": m.usercard.first_name,
                                                  "login_name": m.usercard.username, "remark1": remark1,
                                                  "ismyfriend": 1,
                                                  "colour": m.usercard.user_icon.pure_color},
                                    "isreaded": m.isreaded, "istop": m.istop, "message_type": m.message_type,
                                    "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username, "remark": remark,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 1},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })

                        if m.message_type == "message_groupcard":  # 如果是分享群名片，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {"group_card": {"groupcard_id": m.groupcard_id,
                                                                "groupchat_name": m.groupcard.groupchat_name,
                                                                },
                                                 "isreaded": m.isreaded, "istop": m.istop,
                                                 "message_type": m.message_type,
                                                 "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username, "remark": remark,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 1},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })

                        if m.message_type == "message_task":  # 如果是分配的任务，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {"task_content": {"task_id": m.task.id, "task_name": m.task.task_name,
                                                                  "task_img": m.task.get_img_url(),
                                                                  "task_url": m.task.task_url,
                                                                  "application_name": m.task.application_task.name,
                                                                  "application_icon": m.task.application_task.get_logo()},
                                                 "isreaded": m.isreaded, "istop": m.istop,
                                                 "message_type": m.message_type,
                                                 "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username, "remark": remark,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 1},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })

                    elif friend and friend1 is None:
                        remark = Myfriend.objects.filter(myfriendslist__id=m.user.id).first().remark
                        if m.message_type == "message_normal":  # 如果是普通消息，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {"content": m.text_content, "isreaded": m.isreaded, "istop": m.istop,
                                                 "message_type": m.message_type, "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username, "remark": remark,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 1},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })
                        if m.message_type == "message_usercard":  # 如果是分享个人名片，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {
                                    "user_card": {"usercard_id": m.usercard_id, "user_name": m.usercard.first_name,
                                                  "login_name": m.usercard.username,
                                                  "ismyfriend": 0,
                                                  "colour": m.usercard.user_icon.pure_color},
                                    "isreaded": m.isreaded, "istop": m.istop, "message_type": m.message_type,
                                    "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username, "remark": remark,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 1},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })

                        if m.message_type == "message_groupcard":  # 如果是分享群名片，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {"group_card": {"groupcard_id": m.groupcard_id,
                                                                "groupchat_name": m.groupcard.groupchat_name,
                                                                },
                                                 "isreaded": m.isreaded, "istop": m.istop,
                                                 "message_type": m.message_type,
                                                 "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username, "remark": remark,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 1},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })

                        if m.message_type == "message_task":  # 如果是分配的任务，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {"task_content": {"task_id": m.task.id, "task_name": m.task.task_name,
                                                                  "task_img": m.task.get_img_url(),
                                                                  "task_url": m.task.task_url,
                                                                  "application_name": m.task.application_task.name,
                                                                  "application_icon": m.task.application_task.get_logo()},
                                                 "isreaded": m.isreaded, "istop": m.istop,
                                                 "message_type": m.message_type,
                                                 "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username, "remark": remark,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 1},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })

                    elif friend is None and friend1:
                        remark1 = Myfriend.objects.filter(myfriendslist__id=m.usercard_id).first().remark
                        if m.message_type == "message_normal":  # 如果是普通消息，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {"content": m.text_content, "isreaded": m.isreaded, "istop": m.istop,
                                                 "message_type": m.message_type, "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 0},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })
                        if m.message_type == "message_usercard":  # 如果是分享个人名片，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {
                                    "user_card": {"usercard_id": m.usercard_id, "user_name": m.usercard.first_name,
                                                  "login_name": m.usercard.username, "remark1": remark1,
                                                  "ismyfriend": 1,
                                                  "colour": m.usercard.user_icon.pure_color},
                                    "isreaded": m.isreaded, "istop": m.istop, "message_type": m.message_type,
                                    "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 0},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })

                        if m.message_type == "message_groupcard":  # 如果是分享群名片，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {"group_card": {"groupcard_id": m.groupcard_id,
                                                                "groupchat_name": m.groupcard.groupchat_name,
                                                                },
                                                 "isreaded": m.isreaded, "istop": m.istop,
                                                 "message_type": m.message_type,
                                                 "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 0},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })

                        if m.message_type == "message_task":  # 如果是分配的任务，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {"task_content": {"task_id": m.task.id, "task_name": m.task.task_name,
                                                                  "task_img": m.task.get_img_url(),
                                                                  "task_url": m.task.task_url,
                                                                  "application_name": m.task.application_task.name,
                                                                  "application_icon": m.task.application_task.get_logo()},
                                                 "isreaded": m.isreaded, "istop": m.istop,
                                                 "message_type": m.message_type,
                                                 "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 0},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })


                    else:
                        if m.message_type == "message_normal":  # 如果是普通消息，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {"content": m.text_content, "isreaded": m.isreaded, "istop": m.istop,
                                                 "message_type": m.message_type, "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 0},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })
                        if m.message_type == "message_usercard":  # 如果是分享个人名片，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {
                                    "user_card": {"usercard_id": m.usercard_id, "user_name": m.usercard.first_name,
                                                  "login_name": m.usercard.username,
                                                  "ismyfriend": 0,
                                                  "colour": m.usercard.user_icon.pure_color},
                                    "isreaded": m.isreaded, "istop": m.istop, "message_type": m.message_type,
                                    "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 0},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })

                        if m.message_type == "message_groupcard":  # 如果是分享群名片，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {"group_card": {"groupcard_id": m.groupcard_id,
                                                                "groupchat_name": m.groupcard.groupchat_name,
                                                                },
                                                 "isreaded": m.isreaded, "istop": m.istop,
                                                 "message_type": m.message_type,
                                                 "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 0},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })

                        if m.message_type == "message_task":  # 如果是分配的任务，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {"task_content": {"task_id": m.task.id, "task_name": m.task.task_name,
                                                                  "task_img": m.task.get_img_url(),
                                                                  "task_url": m.task.task_url,
                                                                  "application_name": m.task.application_task.name,
                                                                  "application_icon": m.task.application_task.get_logo()},
                                                 "isreaded": m.isreaded, "istop": m.istop,
                                                 "message_type": m.message_type,
                                                 "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 0},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })

                # 返回结果
                res_data = {
                    "code": 0,
                    "msg": "SUCCESS",
                    "message_total": total,
                    "data": data
                }
                return JsonResponse(res_data, safe=False)
            else:  # 如果聊天和执行清空的用户没有，则进行以下代码：
                messages = Message.objects.filter(groupchat_id=groupchat_id).order_by(
                    '-send_time')[offset: offset + limit]  # 展示全部消息[offset: offset + limit]

                # 以关键字筛选获取信息内容
                for m in messages:  # 遍历消息
                    friend = Myfriend.objects.filter(myfriendslist__id=m.user.id).first()
                    friend1 = Myfriend.objects.filter(myfriendslist__id=m.usercard_id).first()
                    if friend and friend1:
                        remark = Myfriend.objects.filter(myfriendslist__id=m.user.id).first().remark
                        remark1 = Myfriend.objects.filter(myfriendslist__id=m.usercard_id).first().remark
                        if m.message_type == "message_normal":  # 如果是普通消息，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {"content": m.text_content, "isreaded": m.isreaded, "istop": m.istop,
                                                 "message_type": m.message_type, "send_time": m.send_time, },
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username, "remark": remark,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 1},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })
                        if m.message_type == "message_usercard":  # 如果是分享个人名片，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {
                                    "user_card": {"usercard_id": m.usercard_id, "user_name": m.usercard.first_name,
                                                  "login_name": m.usercard.username, "remark1": remark1,
                                                  "ismyfriend": 1,
                                                  "colour": m.usercard.user_icon.pure_color},
                                    "isreaded": m.isreaded, "istop": m.istop, "message_type": m.message_type,
                                    "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username, "remark": remark,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 1},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })

                        if m.message_type == "message_groupcard":  # 如果是分享群名片，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {"group_card": {"groupcard_id": m.groupcard_id,
                                                                "groupchat_name": m.groupcard.groupchat_name,
                                                                },
                                                 "isreaded": m.isreaded, "istop": m.istop,
                                                 "message_type": m.message_type,
                                                 "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username, "remark": remark,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 1},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })

                        if m.message_type == "message_task":  # 如果是分配的任务，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {"task_content": {"task_id": m.task.id, "task_name": m.task.task_name,
                                                                  "task_img": m.task.get_img_url(),
                                                                  "task_url": m.task.task_url,
                                                                  "application_name": m.task.application_task.name,
                                                                  "application_icon": m.task.application_task.get_logo()},
                                                 "isreaded": m.isreaded, "istop": m.istop,
                                                 "message_type": m.message_type,
                                                 "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username, "remark": remark,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 1},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })
                    elif friend and friend1 is None:
                        remark = Myfriend.objects.filter(myfriendslist__id=m.user.id).first().remark
                        # remark1 = Myfriend.objects.filter(myfriendslist__id=m.usercard_id).first().remark
                        if m.message_type == "message_normal":  # 如果是普通消息，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {"content": m.text_content, "isreaded": m.isreaded, "istop": m.istop,
                                                 "message_type": m.message_type, "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username, "remark": remark,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 1},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })
                        if m.message_type == "message_usercard":  # 如果是分享个人名片，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {
                                    "user_card": {"usercard_id": m.usercard_id, "user_name": m.usercard.first_name,
                                                  "login_name": m.usercard.username,
                                                  "ismyfriend": 0,
                                                  "colour": m.usercard.user_icon.pure_color},
                                    "isreaded": m.isreaded, "istop": m.istop, "message_type": m.message_type,
                                    "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username, "remark": remark,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 1},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })

                        if m.message_type == "message_groupcard":  # 如果是分享群名片，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {"group_card": {"groupcard_id": m.groupcard_id,
                                                                "groupchat_name": m.groupcard.groupchat_name,
                                                                },
                                                 "isreaded": m.isreaded, "istop": m.istop,
                                                 "message_type": m.message_type,
                                                 "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username, "remark": remark,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 1},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })

                        if m.message_type == "message_task":  # 如果是分配的任务，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {"task_content": {"task_id": m.task.id, "task_name": m.task.task_name,
                                                                  "task_img": m.task.get_img_url(),
                                                                  "task_url": m.task.task_url,
                                                                  "application_name": m.task.application_task.name,
                                                                  "application_icon": m.task.application_task.get_logo()},
                                                 "isreaded": m.isreaded, "istop": m.istop,
                                                 "message_type": m.message_type,
                                                 "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username, "remark": remark,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 1},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })



                    elif friend is None and friend1:
                        # remark = Myfriend.objects.filter(myfriendslist__id=m.user.id).first().remark
                        remark1 = Myfriend.objects.filter(myfriendslist__id=m.usercard_id).first().remark
                        if m.message_type == "message_normal":  # 如果是普通消息，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {"content": m.text_content, "isreaded": m.isreaded, "istop": m.istop,
                                                 "message_type": m.message_type, "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 0},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })
                        if m.message_type == "message_usercard":  # 如果是分享个人名片，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {
                                    "user_card": {"usercard_id": m.usercard_id, "user_name": m.usercard.first_name,
                                                  "login_name": m.usercard.username, "remark1": remark1,
                                                  "ismyfriend": 1,
                                                  "colour": m.usercard.user_icon.pure_color},
                                    "isreaded": m.isreaded, "istop": m.istop, "message_type": m.message_type,
                                    "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 0},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })

                        if m.message_type == "message_groupcard":  # 如果是分享群名片，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {"group_card": {"groupcard_id": m.groupcard_id,
                                                                "groupchat_name": m.groupcard.groupchat_name,
                                                                },
                                                 "isreaded": m.isreaded, "istop": m.istop,
                                                 "message_type": m.message_type,
                                                 "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 0},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })

                        if m.message_type == "message_task":  # 如果是分配的任务，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {"task_content": {"task_id": m.task.id, "task_name": m.task.task_name,
                                                                  "task_img": m.task.get_img_url(),
                                                                  "task_url": m.task.task_url,
                                                                  "application_name": m.task.application_task.name,
                                                                  "application_icon": m.task.application_task.get_logo()},
                                                 "isreaded": m.isreaded, "istop": m.istop,
                                                 "message_type": m.message_type,
                                                 "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 0},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })


                    else:
                        if m.message_type == "message_normal":  # 如果是普通消息，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {"content": m.text_content, "isreaded": m.isreaded, "istop": m.istop,
                                                 "message_type": m.message_type, "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 0},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })
                        if m.message_type == "message_usercard":  # 如果是分享个人名片，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {
                                    "user_card": {"usercard_id": m.usercard_id, "user_name": m.usercard.first_name,
                                                  "login_name": m.usercard.username,
                                                  "ismyfriend": 0,
                                                  "colour": m.usercard.user_icon.pure_color},
                                    "isreaded": m.isreaded, "istop": m.istop, "message_type": m.message_type,
                                    "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 0},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })

                        if m.message_type == "message_groupcard":  # 如果是分享群名片，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {"group_card": {"groupcard_id": m.groupcard_id,
                                                                "groupchat_name": m.groupcard.groupchat_name,
                                                                },
                                                 "isreaded": m.isreaded, "istop": m.istop,
                                                 "message_type": m.message_type,
                                                 "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 0},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })

                        if m.message_type == "message_task":  # 如果是分配的任务，则展示如下内容：
                            data.append({
                                "message_id": m.id,
                                'groupchat_id': m.groupchat.id,
                                'groupchat_name': m.groupchat.groupchat_name,
                                "text_content": {"task_content": {"task_id": m.task.id, "task_name": m.task.task_name,
                                                                  "task_img": m.task.get_img_url(),
                                                                  "task_url": m.task.task_url,
                                                                  "application_name": m.task.application_task.name,
                                                                  "application_icon": m.task.application_task.get_logo()},
                                                 "isreaded": m.isreaded, "istop": m.istop,
                                                 "message_type": m.message_type,
                                                 "send_time": m.send_time},
                                "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                           "login_name": m.user.username,
                                           "colour": m.user.user_icon.pure_color, "ismyfriend": 0},
                                'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            })

                # 返回结果
                res_data = {
                    "code": 0,
                    "msg": "SUCCESS",
                    "message_total": total,
                    "data": data
                }
                return JsonResponse(res_data, safe=False)

    """ 
        @apiName set-isreaded
        @api {PATCH} /api/v1/groupchats/{groupchat_id}/messages
        @apiGroup chat-messages
        @apiVersion 0.0.1
        @apiDescription [聊天]点击聊天，聊天未读消息变为已读
        @apiParam {int} groupchat_id 聊天id  
        @apiSuccess {String} msg 信息
        @apiSuccess {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 成功返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
        }
    """

    # 点击聊天，聊天未读消息变为已读
    def patch(self, request, groupchat_id):

        if groupchat_id:  # 如果id存在
            groupchat = GroupChat.objects.filter(id=groupchat_id).first()
            groupchat.messagegroupchat.filter(isreaded=0).update(isreaded=1)

        else:  # 如果数据不存在，忽略不计
            pass

        res_data = {
            "code": 0,
            "msg": "SUCCESS",
        }
        return JsonResponse(res_data, safe=False)


class SingleMessage(View):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        """
            @apiName get_messages
            @api {GET} /api/v1/groupchats/{groupchat_id}/messages/{message_id}
            @apiGroup chat-messages
            @apiVersion 0.0.1
            @apiDescription [聊天]查看消息的上下文
            @apiParam {int} groupchat_id 所属群聊或者私聊id 
            @apiParam {int} message_id 消息的id
            @apiSuccess {String} msg 信息
            @apiSuccess {String} code 0代表无错误 1代表有错误
            @apiSuccessExample {json} 返回样例:
            {
                "code": 0,
                "msg": "SUCCESS",
                "page": 2,
                "data": [
                    {
                        "message_id": 279,
                        "groupchat_name": "李宁",
                        "text_content": {
                            "content": "你好\n",
                            "isreaded": true,
                            "istop": false,
                            "message_type": "message_normal",
                            "send_time": "2020-09-16T17:33:40.060"
                        },
                        "sender": {
                            "user_id": 1,
                            "user_name": "admin",
                            "login_name": "admin",
                            "colour": "#4682B4",
                            "ismyfriend": 0
                        },
                        "send_time": "2020-09-16 17:33:40"
                    }
            
                ]
            }       

        """

    # [聊天]查看消息上下文
    def get(self, request, groupchat_id, message_id):

        global data, messagesparts, page
        groupchat = GroupChat.objects.filter(id=groupchat_id).first()
        messages = groupchat.messagegroupchat.all().order_by('-send_time')
        data1 = []
        data = []

        for m in messages:
            data1.append(m.id)
        num = data1.index(int(message_id))
        if num == 0:
            page = 1
            messagesparts = messages[(page - 1) * 10:page * 10]
            for m in messagesparts:
                if m.message_type == "message_normal":  # 如果是普通消息，则展示如下内容：
                    data.append({
                        "message_id": m.id,
                        'groupchat_name': m.groupchat.groupchat_name,
                        "text_content": {"content": m.text_content, "isreaded": m.isreaded, "istop": m.istop,
                                         "message_type": m.message_type, "send_time": m.send_time},
                        "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                   "login_name": m.user.username,
                                   "colour": m.user.user_icon.pure_color, "ismyfriend": 0},
                        'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                    })
                if m.message_type == "message_usercard":  # 如果是分享个人名片，则展示如下内容：
                    data.append({
                        "message_id": m.id,
                        'groupchat_name': m.groupchat.groupchat_name,
                        "text_content": {
                            "user_card": {"usercard_id": m.usercard_id, "user_name": m.usercard.first_name,
                                          "login_name": m.usercard.username,
                                          "ismyfriend": 0,
                                          "colour": m.usercard.user_icon.pure_color},
                            "isreaded": m.isreaded, "istop": m.istop, "message_type": m.message_type,
                            "send_time": m.send_time},
                        "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                   "login_name": m.user.username,
                                   "colour": m.user.user_icon.pure_color, "ismyfriend": 0},
                        'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                    })

                if m.message_type == "message_groupcard":  # 如果是分享群名片，则展示如下内容：
                    data.append({
                        "message_id": m.id,
                        'groupchat_name': m.groupchat.groupchat_name,
                        "text_content": {"group_card": {"groupcard_id": m.groupcard_id,
                                                        "groupchat_name": m.groupcard.groupchat_name,
                                                        },
                                         "isreaded": m.isreaded, "istop": m.istop,
                                         "message_type": m.message_type,
                                         "send_time": m.send_time},
                        "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                   "login_name": m.user.username,
                                   "colour": m.user.user_icon.pure_color, "ismyfriend": 0},
                        'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                    })

                if m.message_type == "message_task":  # 如果是分配的任务，则展示如下内容：
                    data.append({
                        "message_id": m.id,
                        'groupchat_name': m.groupchat.groupchat_name,
                        "text_content": {"task_content": {"task_id": m.task.id, "task_name": m.task.task_name,
                                                          "task_img": m.task.get_img_url(),
                                                          "task_url": m.task.task_url,
                                                          "application_name": m.task.application_task.application_name,
                                                          "application_icon": m.task.application_task.get_icon_url()},
                                         "isreaded": m.isreaded, "istop": m.istop,
                                         "message_type": m.message_type,
                                         "send_time": m.send_time},
                        "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                   "login_name": m.user.username,
                                   "colour": m.user.user_icon.pure_color, "ismyfriend": 0},
                        'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                    })

        if num != 0:
            page = math.ceil(num / 9)
            messagesparts = messages[(page - 1) * 10:page * 10]
            for m in messagesparts:
                if m.message_type == "message_normal":  # 如果是普通消息，则展示如下内容：
                    data.append({
                        "message_id": m.id,
                        'groupchat_name': m.groupchat.groupchat_name,
                        "text_content": {"content": m.text_content, "isreaded": m.isreaded, "istop": m.istop,
                                         "message_type": m.message_type, "send_time": m.send_time},
                        "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                   "login_name": m.user.username,
                                   "colour": m.user.user_icon.pure_color, "ismyfriend": 0},
                        'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                    })
                if m.message_type == "message_usercard":  # 如果是分享个人名片，则展示如下内容：
                    data.append({
                        "message_id": m.id,
                        'groupchat_name': m.groupchat.groupchat_name,
                        "text_content": {
                            "user_card": {"usercard_id": m.usercard_id, "user_name": m.usercard.first_name,
                                          "login_name": m.usercard.username,
                                          "ismyfriend": 0,
                                          "colour": m.usercard.user_icon.pure_color},
                            "isreaded": m.isreaded, "istop": m.istop, "message_type": m.message_type,
                            "send_time": m.send_time},
                        "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                   "login_name": m.user.username,
                                   "colour": m.user.user_icon.pure_color, "ismyfriend": 0},
                        'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                    })

                if m.message_type == "message_groupcard":  # 如果是分享群名片，则展示如下内容：
                    data.append({
                        "message_id": m.id,
                        'groupchat_name': m.groupchat.groupchat_name,
                        "text_content": {"group_card": {"groupcard_id": m.groupcard_id,
                                                        "groupchat_name": m.groupcard.groupchat_name,
                                                        },
                                         "isreaded": m.isreaded, "istop": m.istop,
                                         "message_type": m.message_type,
                                         "send_time": m.send_time},
                        "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                   "login_name": m.user.username,
                                   "colour": m.user.user_icon.pure_color, "ismyfriend": 0},
                        'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                    })

                if m.message_type == "message_task":  # 如果是分配的任务，则展示如下内容：
                    data.append({
                        "message_id": m.id,
                        'groupchat_name': m.groupchat.groupchat_name,
                        "text_content": {"task_content": {"task_id": m.task.id, "task_name": m.task.task_name,
                                                          "task_img": m.task.get_img_url(),
                                                          "task_url": m.task.task_url,
                                                          "application_name": m.task.application_task.application_name,
                                                          "application_icon": m.task.application_task.get_icon_url()},
                                         "isreaded": m.isreaded, "istop": m.istop,
                                         "message_type": m.message_type,
                                         "send_time": m.send_time},
                        "sender": {"user_id": m.user.id, "user_name": m.user.first_name,
                                   "login_name": m.user.username,
                                   "colour": m.user.user_icon.pure_color, "ismyfriend": 0},
                        'send_time': m.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                    })

        res_data = {
            "code": 0,
            "msg": "SUCCESS",
            "page": page,
            "data": data
        }
        return JsonResponse(res_data, safe=False)
