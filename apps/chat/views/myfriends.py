import json
from collections import OrderedDict
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.contrib.auth.models import User
from django.db.models import Q
from django.http import JsonResponse
from django.views import View
from apps.applicationnotice.models import Notice
from apps.chat.models import Myfriend, GroupChat, Message, MessagePart


# 添加好友和展示所有好友
class Friends(View):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        """ 
            @apiName request_add-friends
            @api {POST} /api/v1/myfriends
            @apiGroup chat-myfriendsxia
            @apiVersion 0.0.1
            @apiDescription [聊天]申请添加好友
            @apiParam {int} user_id 用户id
            @apiSuccess {String} msg 信息
            @apiSuccess {String} code 0代表无错误 1代表有错误
            @apiSuccessExample {json} 成功返回样例:
            {
                "code": 0,
                "msg": "SUCCESS",
            }
        """

    # 通过用户或者输入用户名，登录名，申请添加好友
    def post(self, request):

        # 获取参数
        put_data = json.loads(request.body)
        user_id = put_data.get('user_id')
        log_user = request.user

        # 校验用户和参数
        if not user_id:  # 用户如果不存在，则提示：
            res_data = {
                "msg": "参数缺失",
            }
            return JsonResponse(res_data, safe=False, status=406)

        if user_id:  # 用户如果存在，则进行添加为好友！
            oldfriend = Myfriend.objects.filter(Q(myfriendslist__id=user_id) & Q(owner_id=log_user.id))  # 验证用户是否在好友列表
            if oldfriend:  # 如果在好友列表，则提示用户已经存在！
                res_data = {
                    "msg": "好友已经存在",
                }
                return JsonResponse(res_data, safe=False, status=400)

            else:  # 如果不在好友列表。则申请添加用户为好友！
                oldnotice = Notice.objects.filter(Q(user_id=log_user.id) & Q(owner_id=user_id)).last()
                if oldnotice and oldnotice.isaccept is None:
                    res_data = {
                        "msg": "好友申请已存在",
                    }
                    return JsonResponse(res_data, safe=False, status=400)
                elif oldnotice is None or oldnotice.isaccept == 1:
                    notice = Notice.objects.create(
                        notice_content="申请加您为好友",
                        user_id=log_user.id,
                        owner_id=user_id,
                        notice_type="apply",
                        sender_type="user",
                        isaccept=None,
                        isreaded=0,
                    )

                    # 建立websocket链接
                    channel_layer = get_channel_layer()
                    async_to_sync(channel_layer.group_send)(
                        "user_%s" % user_id,
                        {
                            'type': 'chat_message',
                            'data':
                                {
                                    "type": "application",
                                    "application":
                                        {
                                            "notice_id": notice.id,
                                            'user_id': log_user.id,
                                            'user_name': log_user.first_name,
                                            'colour': log_user.user_icon.pure_color,
                                            'isaccept': None,
                                            'isreaded': 0,
                                            'application_id': "self",
                                            'application_name': "消息中心",
                                            'notice_content': "申请加您为好友",
                                            'notice_type': "apply",
                                            'sender_type': "user",
                                            'send_time': notice.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                                            'order_by': '-send_time'

                                        },
                                },
                        })

            # 返回结果
            res_data = {
                "code": 0,
                "msg": "SUCCESS",
            }
            return JsonResponse(res_data, safe=False)

    """ 
        @apiName success_add-friends
        @api {PATCH} /api/v1/myfriends
        @apiGroup chat-myfriends
        @apiVersion 0.0.1
        @apiDescription [聊天]好友点击同意，添加好友成功
        @apiParam {int} user_id 用户id
        @apiParam {int} is_accept 0不是好友/1是好友
        @apiSuccess {String} msg 信息
        @apiSuccess {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 成功返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
        }
    """

    # 好友同意，添加成功。
    def patch(self, request):

        # 获取参数
        log_user = request.user
        put_data = json.loads(request.body)
        user_id = put_data.get('user_id')
        is_accept = put_data.get('is_accept')
        user = User.objects.filter(id=user_id).first()

        # 校验用户和参数
        if not user_id or not is_accept:
            res_data = {
                "msg": "参数缺失",
            }
            return JsonResponse(res_data, safe=False, status=406)

        if is_accept == 1 and user_id:
            oldfriend = Myfriend.objects.filter(Q(myfriendslist__id=user_id) & Q(owner_id=log_user.id))  # 验证用户是否在好友列表
            if oldfriend:  # 如果在好友列表，则提示用户已经存在！
                res_data = {
                    "msg": "好友已经存在",
                }
                return JsonResponse(res_data, safe=False, status=400)
            else:
                Notice.objects.filter(Q(owner_id=log_user.id) & Q(user_id=user_id) & Q(notice_type="apply")).update(
                    isaccept=1)

                myfriend = Myfriend.objects.create(myfriendslist_id=user_id, owner_id=log_user.id)
                myfriend.save()
                myfriend1 = Myfriend.objects.create(myfriendslist_id=log_user.id, owner_id=user_id)
                myfriend1.save()

                newuser = User.objects.filter(id=user_id).first()
                privatechat = GroupChat.objects.create(groupchat_name=newuser.first_name,
                                                       is_groupchat=False)  # 那么建立私聊
                privatechat.groupchat_members.add(user_id)  # 把用户加入群聊的人员中。
                privatechat.groupchat_members.add(log_user)  # 把管理员加入群聊的人员中。
                privatechat.save()

            # 建立websocket链接
            channel_layer = get_channel_layer()
            async_to_sync(channel_layer.group_send)(
                "user_%s" % user_id,
                {
                    'type': 'chat_message',
                    'data':
                        {
                            "type": "notice",
                            "notice":
                                {
                                    "groupchat_id": privatechat.id,
                                    "user_id": user.id,
                                    "myfriend_id": myfriend.id,
                                    "remark": myfriend.remark,
                                    'user_name': user.first_name,
                                    "login_name": user.username,
                                    'colour': user.user_icon.pure_color
                                },
                        },
                })

        # 返回结果
        res_data = {
            "code": 0,
            "msg": "SUCCESS",
        }
        return JsonResponse(res_data, safe=False)

    """
        @apiName show-myfriends
        @api {GET} /api/v1/myfriends
        @apiGroup chat-myfriends
        @apiVersion 0.0.1
        @apiDescription [聊天]展示我的好友的列表，并且通过【好友名字】搜索我的好友，相关的群聊，相关的消息
        @apiParam {String} user_name 用户名字
        @apiSuccess {String} msg 信息
        @apiSuccess {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "myfriend_data": [
                {
                    "user_id": 5,
                    "groupchat_id": 161,
                    "user_name": "王强",
                    "login_name": "wangqiang",
                    "remark": "强强",
                    "colour": "#008B8B",
                    "ismyfriend": 1
                }
            ],
            "groupmembers_data": [
                {
                    "groupchat_id": 155,
                    "groupchat_name": "技术交流群",
                    "groupmembers_num": 1
                }
            ],
            "messages_data": [
                {
                    "groupchat_id": 165,
                    "groupchat_name": "王强",
                    "sender": {
                        "user_id": 5,
                        "user_name": "王强",
                        "login_name": "wangqiang",
                        "remark": "强强",
                        "colour": "#008B8B",
                        "ismyfriend": 1
                    },
                    "messages_num": 1,
                    "is_groupchat": false
                }
            ]
        }
    """

    # [聊天]展示我的好友的列表，并且通过【好友名字】搜索我的好友，相关的群聊，相关的消息
    def get(self, request):

        # 获取参数
        global messages, gs
        log_user = request.user
        user_name = request.GET.get('user_name')  # 获取发信人id参数

        if user_name:  # 如果关键字存在
            myfriends = Myfriend.objects.filter((Q(myfriendslist__first_name__icontains=user_name) | Q(
                myfriendslist__username__icontains=user_name) | Q(
                remark__contains=user_name)) & Q(owner_id=log_user.id))  # 则以关键字模糊筛选我的好友

            # 返回结果，展示出我的好友列表
            myfriend_data = []
            for m in myfriends:
                groupchat = GroupChat.objects.filter(
                    Q(groupchat_members__id__contains=m.myfriendslist.id) & Q(is_groupchat=0)).filter(
                    groupchat_members__id__contains=log_user.id).first()

                if groupchat:
                    groupchat_id = groupchat.id
                    myfriend_data.append({
                        "user_id": m.myfriendslist.id,
                        "groupchat_id": groupchat_id,
                        'user_name': m.myfriendslist.first_name,
                        "login_name": m.myfriendslist.username,
                        "remark": m.remark,
                        'colour': m.myfriendslist.user_icon.pure_color,
                        "ismyfriend": 1

                    })
                else:
                    myfriend_data.append({
                        "user_id": m.myfriendslist.id,
                        "groupchat_id": None,
                        'user_name': m.myfriendslist.first_name,
                        "login_name": m.myfriendslist.username,
                        "remark": m.remark,
                        'colour': m.myfriendslist.user_icon.pure_color,
                        "ismyfriend": 1

                    })

            # groupchats = GroupChat.objects.filter(
            #     (Q(groupchat_members__myfriendslist__remark__icontains=user_name) |
            #      Q(groupchat_members__username__icontains=user_name) |
            #      Q(groupchat_members__first_name__icontains=user_name)) & Q(
            #         groupchat_members__id__contains=log_user.id) & Q(
            #         is_groupchat=1))  # 则以关键字模糊筛选群成员


            groupchats = GroupChat.objects.filter(
                Q(groupchat_members__id__contains=log_user.id) & Q(is_groupchat=1)).filter(
                Q(groupchat_members__username__icontains=user_name) | Q(
                    groupchat_members__first_name__icontains=user_name) | Q(
                    groupchat_members__myfriendslist__remark__icontains=user_name))  # 则以关键字模糊筛选群成员


            # 返回结果，展示群成员列表
            groupmembers_data = []
            for g in groupchats:
                members = g.groupchat_members.filter(Q(first_name__icontains=user_name) | Q(
                    username__icontains=user_name) | Q(
                    myfriendslist__remark__icontains=user_name))  # 反向查询每个群中和关键字相关的群成员
                num = members.count()
                allmembers = []
                for member in members:
                    mf = Myfriend.objects.filter(
                        myfriendslist__id=member.id, owner_id=log_user.id).first()
                    if mf:
                        remark = mf.remark
                        allmembers.append({
                            "user_id": member.id,
                            "user_name": member.first_name,
                            "login_name": member.username,
                            "remark": remark,
                            "colour": member.user_icon.pure_color,
                            "ismyfriend": 1
                        })

                    else:
                        allmembers.append({
                            "user_id": member.id,
                            "user_name": member.first_name,
                            "login_name": member.username,
                            "colour": member.user_icon.pure_color,
                            "ismyfriend": 0
                        })

                groupmembers_data.append({
                    "groupchat_id": g.id,
                    'groupchat_name': g.groupchat_name,
                    "groupchat_member": allmembers,
                    'groupmembers_num': num,
                })

            messages_data = []
            allgroupchats = GroupChat.objects.filter(groupchat_members__id__icontains=log_user.id)
            # 找到和我相关的群组


            # for gs in allgroupchats:  # 遍历群组，并找到每个群组中，和关键字相关的消息
            #     messages = Message.objects.filter(
            #         (Q(text_content__icontains=user_name) | Q(
            #             user__first_name__icontains=user_name) | Q(
            #             user__username__icontains=user_name) | Q(
            #             user__myfriendslist__remark__contains=user_name)) & Q(
            #             groupchat_id=gs.id))


            for gs in allgroupchats:  # 遍历群组，并找到每个群组中，和关键字相关的消息
                messages = Message.objects.filter(
                    ( Q(user__first_name__icontains=user_name) | Q(
                        user__username__icontains=user_name) | Q(
                        user__myfriendslist__remark__icontains=user_name)) & Q(
                        groupchat_id=gs.id))


                messages_num = messages.count()  # 统计每个聊天中相关详细的数量
                if messages and gs.is_groupchat == 0:
                    messages_data.append({
                        'groupchat_id': gs.id,
                        'groupchat_name': gs.groupchat_name,
                        'messages_num': messages_num,
                        "is_groupchat": gs.is_groupchat
                    })
                elif messages and gs.is_groupchat == 1:
                    messages_data.append({
                        'groupchat_id': gs.id,
                        'groupchat_name': gs.groupchat_name,
                        'messages_num': messages_num,
                        "is_groupchat": gs.is_groupchat
                    })

            # 返回结果，展示消息记录列表
            # for ms in messages:
            #     # messages_num = messages.filter(groupchat_id=ms.groupchat.id).count()
            #     u = User.objects.filter(groupmembers__messagegroupchat__id=ms.id).first()
            #     m = Myfriend.objects.filter(myfriendslist__id=u.id).first()
            #     if ms.groupchat.is_groupchat == 0:
            #         messages_data.append({
            #             'groupchat_id': ms.groupchat.id,
            #             'groupchat_name': ms.groupchat.groupchat_name,
            #             "sender": {
            #                 "user_id": u.id,
            #                 'user_name': u.first_name,
            #                 "login_name": u.username,
            #                 "remark": m.remark,
            #                 'colour': u.user_icon.pure_color,
            #                 "ismyfriend": 1
            #             },
            #             'messages_num': messages_num,
            #             "is_groupchat": ms.groupchat.is_groupchat
            #         })
            #     else:
            #         messages_data.append({
            #             'groupchat_id': ms.groupchat.id,
            #             'groupchat_name': ms.groupchat.groupchat_name,
            #             'messages_num': messages_num,
            #             "is_groupchat": ms.groupchat.is_groupchat
            #         })

            # 为列表中的字典去重
            b = OrderedDict()
            for item in messages_data:
                b.setdefault(item['groupchat_id'], {**item, })
            messages_data = list(b.values())

            b = OrderedDict()
            for item in groupmembers_data:
                b.setdefault(item['groupchat_id'], {**item, })
            groupmembers_data = list(b.values())

            res_data = {
                "code": 0,
                "msg": "SUCCESS",
                "myfriend_data": myfriend_data,
                "groupmembers_data": groupmembers_data,
                "messages_data": messages_data,
            }
            return JsonResponse(res_data, safe=False)

        else:  # 如果关键字不存在，则显示我的好友列表！
            myfriends = Myfriend.objects.filter(owner_id=log_user.id)
            data = []
            for myfriend in myfriends:
                groupchat = GroupChat.objects.filter(
                    Q(groupchat_members__id__contains=myfriend.myfriendslist.id) & Q(is_groupchat=0)).filter(
                    groupchat_members__id__contains=log_user.id).first()

                if groupchat:
                    data.append({
                        "groupchat_id": groupchat.id,
                        "user_id": myfriend.myfriendslist.id,
                        'user_name': myfriend.myfriendslist.first_name,
                        'login_name': myfriend.myfriendslist.username,
                        "remark": myfriend.remark,
                        'colour': myfriend.myfriendslist.user_icon.pure_color,
                        "ismyfriend": 1,
                    })
                else:
                    data.append({
                        "groupchat_id": None,
                        "user_id": myfriend.myfriendslist.id,
                        'user_name': myfriend.myfriendslist.first_name,
                        'login_name': myfriend.myfriendslist.username,
                        "remark": myfriend.remark,
                        'colour': myfriend.myfriendslist.user_icon.pure_color,
                        "ismyfriend": 1,
                    })
            # 返回结果
            res_data = {
                "code": 0,
                "msg": "SUCCESS",
                "data": data
            }
            return JsonResponse(res_data, safe=False)


# 对某一个好友进行删除，更改，和查询
class SingleFriend(View):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    """ 
        @apiName delete-friends
        @api {DELETE} /api/v1/myfriends/{user_id}
        @apiGroup chat-myfriends
        @apiVersion 0.0.1
        @apiDescription [聊天]删除好友
        @apiParam {int} user_id 用户id
        @apiSuccess {String} msg 信息
        @apiSuccess {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 成功返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
        }
    """

    def delete(self, request, user_id):

        # 校验用户和参数
        log_user = request.user
        if not log_user:  # 用户如果不存在，则提示：
            res_data = {
                "msg": "参数缺失",
            }
            return JsonResponse(res_data, safe=False, status=406)

        if user_id:
            groupchat = GroupChat.objects.filter(
                Q(groupchat_members__id__icontains=user_id) & Q(is_groupchat=0)).filter(
                groupchat_members__id__icontains=log_user.id).first()  # 确定是哪群组
            groupchat.delete()

            # groupchats = GroupChat.objects.filter(
            #     Q(groupchat_members__id__icontains=user_id)&Q(is_groupchat=0)).filter(groupchat_members__id__icontains=log_user.id) # 确定是哪群组
            # if groupchats:
            #     for g in groupchats:
            #         num = g.groupchat_members.all().count()
            #         print(num)
            #         if num == 1:
            #             g.delete()
            #         if num > 1 and g.is_groupchat == 0:
            #             g.groupchat_members.remove(log_user)
            #             lastmessage = g.messagegroupchat.all().last()
            #             print(lastmessage)
            #             if lastmessage:
            #                 message_id = lastmessage.id
            #                 MessagePart.objects.create(groupchat_id=g.id, user_id=log_user.id,
            #                                            lastmessage_id=message_id)

            myfriend = Myfriend.objects.filter(Q(myfriendslist__id=user_id) & Q(owner_id=log_user.id))
            myfriend.delete()  # 删除好友
            myfriend1 = Myfriend.objects.filter(Q(myfriendslist__id=log_user.id) & Q(owner_id=user_id))
            myfriend1.delete()  # 删除好友

            # 返回结果
            res_data = {
                "code": 0,
                "msg": "SUCCESS",
            }
            return JsonResponse(res_data, safe=False)

    """ 
        @apiName change-remarks
        @api {PATCH} /api/v1/myfriends/{user_id}
        @apiGroup chat-myfriends
        @apiVersion 0.0.1
        @apiDescription [聊天]修改备注
        @apiParam {int} user_id  用户id
        @apiParam {string} remark 备注
        @apiSuccess {String} msg 信息
        @apiSuccess {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 成功返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data": {
                "user_id": 2,
                "user_name": "林奇",
                "login_name": "lining",
                "remarks": "技术负责人",
                "colour": "#5F9EA0"
                "ismyfriend": 1
            }
        }
    """

    def patch(self, request, user_id):

        # 获取用户id，同时输入想要修改的备注
        log_user = request.user
        put_data = json.loads(request.body)
        remark = put_data.get('remark')

        # 校验用户和参数
        if not remark:
            res_data = {
                "msg": "参数缺失",
            }
            return JsonResponse(res_data, safe=False, status=406)

        else:
            u = Myfriend.objects.filter(Q(myfriendslist__id=user_id) & Q(owner_id=log_user.id)).first()  # 筛选出需要修改的用户
            u.remark = remark  # 变更用户的备注
            u.save()  # 保存新的备注
            data = ({
                "user_id": u.myfriendslist.id,
                'user_name': u.myfriendslist.first_name,
                'login_name': u.myfriendslist.username,
                'remarks': u.remark,
                'colour': u.myfriendslist.user_icon.pure_color,
                "ismyfriend": 1,
            })
            # 返回结果，修改备注成功
            res_data = {
                "code": 0,
                "msg": "SUCCESS",
                'data': data
            }
            return JsonResponse(res_data, safe=False)

    """
        @apiName show-users
        @api {GET} /api/v1/myfriends/{user_id}
        @apiGroup chat-myfriends
        @apiVersion 0.0.1
        @apiDescription [应用通知]展示个人详情
        @apiParam {int} user_id   用户id
        @apiSuccess {String} msg 信息
        @apiSuccess {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data": {
                "id": 3,
                "user_name": "张伟",
                "groupchat_id": 155,
                "login_name": "zhangwei",
                "colour": "#228B22",
                "ismyfriend": 0
            }
        }
    """

    # 展示好友的详细信息
    def get(self, request, user_id):

        log_user = request.user
        userdetail = Myfriend.objects.filter(
            Q(myfriendslist__id=user_id) & Q(owner_id=log_user.id)).first()  # 通过用户的id，筛选出用户的详细信息
        u = User.objects.filter(id=user_id).first()
        g = GroupChat.objects.filter(Q(groupchat_members__id=user_id) & Q(is_groupchat=0)).first()
        if userdetail and g:
            data = {
                "user_id": userdetail.myfriendslist.id,
                "groupchat_id": g.id,
                'user_name': userdetail.myfriendslist.first_name,
                'login_name': userdetail.myfriendslist.username,
                'remarks': userdetail.remark,
                'colour': userdetail.myfriendslist.user_icon.pure_color,
                "ismyfriend": 1
            }  # 返回展示用户详细信息
            res_data = {
                "code": 0,
                "msg": "SUCCESS",
                "data": data
            }
            return JsonResponse(res_data, safe=False)
        elif userdetail and g is None:

            data = {
                "user_id": userdetail.myfriendslist.id,
                "groupchat_id": None,
                'user_name': userdetail.myfriendslist.first_name,
                'login_name': userdetail.myfriendslist.username,
                'remarks': userdetail.remark,
                'colour': userdetail.myfriendslist.user_icon.pure_color,
                "ismyfriend": 1
            }  # 返回展示用户详细信息

            res_data = {
                "code": 0,
                "msg": "SUCCESS",
                "data": data
            }
            return JsonResponse(res_data, safe=False)

        elif userdetail is None and g:

            data = {
                "id": u.id,
                "user_name": u.first_name,
                "groupchat_id": g.id,
                "login_name": u.username,
                "colour": u.user_icon.pure_color,
                "ismyfriend": 0
            }  # 返回展示用户详细信息

            res_data = {
                "code": 0,
                "msg": "SUCCESS",
                "data": data
            }
            return JsonResponse(res_data, safe=False)

        elif userdetail is None and g is None:
            res_data = {
                "id": u.id,
                "user_name": u.first_name,
                "groupchat_id": None,
                "login_name": u.username,
                "colour": u.user_icon.pure_color,
                "ismyfriend": 0
            }  # 返回用户详细信息
            return JsonResponse(res_data, safe=False)
