from django.db.models import Q
from django.http import JsonResponse
from django.views import View
from apps.applicationnotice.models import Notice
from apps.chat.models import Message


# 计算所有的未读消息和通知
class MeaasgeCounts(View):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    """ 
        @apiName message-counts
        @api {GET} /api/v1/messages/messagecounts
        @apiGroup chat-messages
        @apiVersion 0.0.1
        @apiDescription [聊天]应用通知和聊天两大板块，所有未读消息的统计
        @apiSuccess {String} msg 信息
        @apiSuccess {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 成功返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            'num':12
        }
    """

    def get(self, request):
        log_user=request.user
        num_1 = Notice.objects.filter(Q(isreaded=0)&Q(owner_id=log_user.id)).count()  # 统计未读通知的数量
        num_2 = Message.objects.filter(Q(isreaded=0)&Q(owner_id=log_user.id)).count()  # 统计未读消息的数量
        num = num_1 + num_2
        # 返回结果
        res_data = {
            "code": 0,
            "msg": "SUCCESS",
            'num': num
        }
        return JsonResponse(res_data, safe=False)
