import json

from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.db.models import Q
from django.http import JsonResponse
from django.views import View
from rest_framework.exceptions import NotAcceptable, PermissionDenied
from apps.chat.models import User, GroupChat


# 群管理员，添加群成员
class GroupMembers(View):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        """ 
            @apiName add-groupmembers
            @api {POST} /api/v1/groupchats/{groupchat_id}/groupmembers
            @apiGroup chat-groupchats
            @apiVersion 0.0.1
            @apiDescription [聊天]添加群成员
            @apiParam {int} groupchat_id 群组id 
            @apiParam {list} user_id 用户id列表    
            @apiSuccess {String} msg 信息
            @apiSuccess {String} code 0代表无错误 1代表有错误
            @apiSuccessExample {json} 成功返回样例:
            {
                "code": 0,
                "msg": "SUCCESS",
            }
        """

    def post(self, request, groupchat_id):

        # 点击群组，获得群组id，同时获取用户id
        put_data = json.loads(request.body)
        user_id = put_data.get('user_id')

        # 校验用户和参数
        if not user_id:  # 群组或用户如果不存在，则提示：
            res_data = {
                "msg": "尚未勾选群成员",
            }
            return JsonResponse(res_data, safe=False, status=400)

        # 执行逻辑
        for user_id_1 in user_id:
            groupchat_member = GroupChat.objects.filter(Q(groupchat_members__id=user_id_1) & Q(id=groupchat_id))
            if groupchat_member:  # 如果已存在群列表中，则提示已经在群组中。
                res_data = {
                    "msg": "已经是群成员，不要重复添加",
                }
                return JsonResponse(res_data, safe=False, status=400)

            else:  # 如果不在好友列表。则添加用户为好友！
                groupchat = GroupChat.objects.filter(id=groupchat_id).first()
                member = User.objects.filter(id=user_id_1).first()
                groupchat.groupchat_members.add(member)  # 向群组中添加好友

                # 建立websocket链接
                channel_layer = get_channel_layer()
                async_to_sync(channel_layer.group_send)(
                    "user_%s" % user_id_1,
                    {
                        'type': 'chat_message',
                        'data':
                            {
                                "type": "notice",
                                "notice":
                                    {
                                        "groupchat_id": groupchat_id,
                                        "user_id": user_id,
                                        'msg': "添加群成员成功"
                                    },
                            },
                    })


            # 返回结果
        res_data = {
            "code": 0,
            "msg": "SUCCESS",
        }
        return JsonResponse(res_data, safe=False)

    """ 
        @apiName delete-groupmemebers
        @api {DELETE} /api/v1/groupchats/{groupchat_id}/groupmembers/{user_id}
        @apiGroup chat-groupchats
        @apiVersion 0.0.1
        @apiDescription [聊天]删除群成员
        @apiParam {int} groupchat_id 群组id 
        @apiParam {int} user_id 用户id  
        @apiSuccess {String} msg 信息
        @apiSuccess {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 成功返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
        }
    """

    def delete(self, request, groupchat_id, user_id):

        if user_id and groupchat_id:  # 用户如果存在，则删除好友！
            groupchat = GroupChat.objects.filter(id=groupchat_id).first()  # 确定是哪个群组
            member = User.objects.filter(id=user_id).first()  # 选中要删除的人员
            if member.id == groupchat.groupchat_creater.id:  # 如果选中人员为群管理员
                res_data = {
                    "msg": "不能选择群管理员",
                }
                return JsonResponse(res_data, safe=False, status=400)

            else:  # 如果选中人员不是群管理员
                groupchat.groupchat_members.remove(member)  # 删除选中的人员

                # 建立websocket链接
                channel_layer = get_channel_layer()
                async_to_sync(channel_layer.group_send)(
                    "user_%s" % user_id,
                    {
                        'type': 'chat_message',
                        'data':
                            {
                                "type": "notice",
                                "notice":
                                    {
                                        "groupchat_id": groupchat_id,
                                        "user_id": user_id,
                                        'msg': "删除群成员/退出群聊成功"
                                    },
                            },
                    })


            # 返回结果
            res_data = {
                "code": 0,
                "msg": "SUCCESS",
            }
            return JsonResponse(res_data, safe=False)
