from django.apps import AppConfig


class ChatConfig(AppConfig):
    name = 'apps.chat'

    def ready(self):
        from django.db.models.signals import post_save
        from .signal import generate_enclosure_color, generate_groupchat_color

        post_save.connect(generate_enclosure_color, sender='chat.Enclosure',
                          dispatch_uid='generate_enclosure_color')

        post_save.connect(generate_groupchat_color, sender='chat.GroupChat',
                          dispatch_uid='generate_groupchat_color')
