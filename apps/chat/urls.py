from django.conf.urls import url
from django.urls import path

from apps.chat.views.enclosures import Enclosures
from apps.chat.views.groupchats import GroupChats, SingleGroupchat
from apps.chat.views.groupmembers import GroupMembers
from apps.chat.views.messagecounts import MeaasgeCounts
from apps.chat.views.messages import Messages, SingleMessage
from apps.chat.views.myfriends import Friends, SingleFriend
from apps.chat.views.users import Users, SingleUsers

urlpatterns = [

    # 群聊的url
    url(r'groupchats$',GroupChats.as_view()),  # 1：[POST]创建私聊或者群聊 2：[GET]展示群聊列表
    url(r'groupchats/(?P<groupchat_id>\d+)$', SingleGroupchat.as_view()),#1：[DELETE]删除某一个群聊和私聊  2：[PATCH]更改某一个群聊的群主和修改群名称  3：[GET]展示一个群的信息信息


    # 我的好友的url
    url(r'myfriends$', Friends.as_view()),  # 1:[POST]加好友   2:[PATCH]添加好友成功   2:[GET]显示好友列表
    url(r'myfriends/(?P<user_id>\d+)$', SingleFriend.as_view()),  # 1:[DELETE]删除某一个好友  2：[PATCH]修改某一个好友的备注 3：[GET] 展示某一个好友的信息


    # 群组成员的url2
    url(r'groupchats/(?P<groupchat_id>\d+)/groupmembers$', GroupMembers.as_view()),  # 1[POST]添加某一个群群成员
    url(r'groupchats/(?P<groupchat_id>\d+)/groupmembers/(?P<user_id>\d+)$',GroupMembers.as_view()),  # 2[DELETE]删除某一个群的某一个群成员


    # 消息的url
    url(r'groupchats/messages$', Messages.as_view()),# 1：[POST]保存聊天的消息
    url(r'groupchats/(?P<groupchat_id>\d+)/messages$',Messages.as_view()), # 1：[DELETE]清空某一个聊天的消息 2：[GET]点击头像展示某一个聊天的消息 3:[PATCH]点击某一群组，消息从未读变成已读
    url(r'groupchats/(?P<groupchat_id>\d+)/messages/(?P<message_id>\d+)$', SingleMessage.as_view()),
    url(r'messages/messagecounts$', MeaasgeCounts.as_view()),   # 1：[GET]所有消息和应用通知的未读统计（应用通知和消息两大板块总和）


    # 附件的url
    url(r'groupchats/(?P<groupchat_id>\d+)/enclosures$',Enclosures.as_view()),# 1:[POST]上传某一个聊天的附件 2:[GET]展示某一个聊天的附件
    url(r'groupchats/(?P<groupchat_id>\d+)/enclosures/(?P<enclosure_id>\d+)$', Enclosures.as_view()),  # 1：[DELETE]展示某一个聊天的某一个附件

    # 用户的url
    url(r'users$', Users.as_view()),  # [GET]展示普通用户的列表
    url(r'users/(?P<user_id>\d+)$', SingleUsers.as_view()),  # [GET]展示单个普通用户的详细信息

]
