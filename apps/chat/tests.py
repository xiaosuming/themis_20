# # # import mimetypes
# # #
# # # import filetype
# # #
# # #
# # # def judge_type(folder_path, file_name):
# # #     """检查文件类型"""
# # #     try:
# # #         return filetype.guess(obj=folder_path).mime
# # #     except:
# # #         file_type = mimetypes.guess_type(file_name)[0]
# # #         if file_type is None:
# # #             return 'application/octet-stream'
# # #         return file_type
# # #
# # # folder_path = r'D:\xiaosuming\projects\server\media\enclosure\1599558487213417桌面图.jpg'
# # # file_name = '15995501816396458apiDoc.txt'
# # # file_type = judge_type(folder_path, file_name)
# # # print(file_type)
# #
# #
# # # chat/consumers.py
# # from channels.generic.websocket import AsyncWebsocketConsumer
# # import json
# #
# #
# # class ChatConsumer(AsyncWebsocketConsumer):
# #     async def connect(self):
# #         self.room_name = self.scope['url_route']['kwargs']['room_name']
# #         self.room_group_name = 'chat_%s' % self.room_name
# #
# #         # Join room group
# #         await self.channel_layer.group_add(
# #             self.room_group_name,
# #             self.channel_name
# #         )
# #         await self.accept()
# #
# #     async def disconnect(self, close_code):
# #         # Leave room group
# #         await self.channel_layer.group_discard(
# #             self.room_group_name,
# #             self.channel_name
# #         )
# #
# #     # Receive message from WebSocket
# #     async def receive(self, text_data):
# #         text_data_json = json.loads(text_data)
# #         message = text_data_json['message']
# #
# #         # Send message to room group
# #         await self.channel_layer.group_send(
# #             self.room_group_name,
# #             {
# #                 'type': 'chat_message',
# #                 'message': message
# #             }
# #         )
# #
# #     # Receive message from room group
# #     async def chat_message(self, event):
# #         message = event['message']
# #
# #         # Send message to WebSocket
# #         await self.send(text_data=json.dumps({
# #             'message': message
# #         }))
# #
#
#
# # 展示左边，私聊和群聊的列表，以及未读消息的统计
# def get(self, request):
#     is_groupchat = request.GET.get('is_groupchat')  # 获取是否是群组的参数
#     if is_groupchat:  # 如果参数存在
#         groupchats = GroupChat.objects.filter(is_groupchat=is_groupchat).order_by("-created_time")  # 找到是群聊的所有群组
#         data = []
#         for groupchat in groupchats:  # 遍历所有的群组，并展示出来
#             num = Message.objects.filter(groupchat_id=groupchat.id).filter(
#                 isreaded=0).count()  # 筛选出【对应应用】的【未读的消息】并【计数】
#             data.append({
#                 "groupchat_id": groupchat.id,
#                 'groupchat_name': groupchat.groupchat_name,
#                 'groupchat_creater_id': groupchat.groupchat_creater_id,
#                 'num': num
#             })
#         # 返回结果
#         res_data = {
#             "code": 0,
#             "msg": "SUCCESS",
#             "data": data
#         }
#         return JsonResponse(res_data, safe=False)
#     else:  # 如果参数不存在
#         groupchats = GroupChat.objects.annotate(latest_msg_time=Max('messagegroupchat__send_time'))  # 获取群组中最早的消息
#         groupchats = groupchats.order_by('-latest_msg_time')  # 根据每个群中最早的消息进行排序
#         groupchats = groupchats.filter(is_deleteed=0)
#
#         data = []
#         for g in groupchats:  # 逐渐遍历每一个聊天
#             log_user = request.user  # 用户登陆成功
#             m1 = MessagePart.objects.filter(groupchat_id=g.id).first()  # 清空列表中是否存这个聊天
#             m2 = MessagePart.objects.filter(user_id=log_user.id).first()  # 清空列表中是清空的这个用户
#             if m1 and m2 :
#                 lastmessage_id = MessagePart.objects.filter(
#                     groupchat_id=g.id).last().lastmessage_id  # 找出清空聊天记录时最后一条消息的id
#                 # groupchat = GroupChat.objects.filter(id=groupchat_id).first()  # 找到聊天群组
#                 message = g.messagegroupchat.filter(
#                     id__gt=lastmessage_id).last()
#                 if message:
#                     num = Message.objects.filter(groupchat_id=g.id).filter(
#                         isreaded=0).count()  # 筛选出【每一个聊天】的【未读的消息】并【计数】
#                     if not g.is_groupchat:  # 如果不是群聊，同时最新消息存在
#                         remark = Myfriend.objects.filter(
#                             myfriendslist__id=g.groupchat_members.all().first().id).first().remark
#                         data.append({
#                             "groupchat_id": g.id,
#                             "groupchat_name": g.groupchat_name,
#                             'groupchat_creater_id': g.groupchat_creater_id,
#                             "groupchat_member": {
#                                 "user_id": g.groupchat_members.all().first().id,
#                                 "user_name": g.groupchat_members.all().first().first_name,
#                                 "login_name": g.groupchat_members.all().first().username,
#                                 "remark": remark,
#                                 "colour": g.groupchat_members.all().first().user_icon.pure_color,
#                                 "ismyfriend": 1
#                             },
#                             'groupchat_message': {
#                                 "text_content": message.text_content,
#                                 "send_time": message.send_time.strftime(
#                                     '%Y-%m-%d %H:%M:%S')},  # 同时展示出群消息的第一条内容
#                             "num": num,
#                             "is_groupchat": g.is_groupchat,
#                             "send_time": g.messagegroupchat.all().last().send_time.strftime(
#                                 '%Y-%m-%d %H:%M:%S')
#                         })  # 展示详细的结果
#
#                     if g.is_groupchat :
#                         data.append({
#                             "groupchat_id": g.id,
#                             "groupchat_name": g.groupchat_name,
#                             'groupchat_creater_id': g.groupchat_creater_id,
#                             'groupchat_message': {
#                                 "text_content": message.text_content,
#                                 "send_time": m.send_time.strftime(
#                                     '%Y-%m-%d %H:%M:%S'
#                                 )},
#                             # 同时展示出群消息的第一条内容
#                             "num": num,
#                             "is_groupchat": g.is_groupchat,
#                             "send_time": g.messagegroupchat.all().last().send_time.strftime(
#                                 '%Y-%m-%d %H:%M:%S')
#                         })
#
#                 else:
#
#                     if not g.is_groupchat:
#                         remark = Myfriend.objects.filter(
#                             myfriendslist__id=g.groupchat_members.all().first().id).first().remark
#                         data.append({
#                             "groupchat_id": g.id,
#                             "groupchat_name": g.groupchat_name,
#                             'groupchat_creater_id': g.groupchat_creater_id,
#                             "groupchat_member": {
#                                 "user_id": g.groupchat_members.all().first().id,
#                                 "user_name": g.groupchat_members.all().first().first_name,
#                                 "login_name": g.groupchat_members.all().first().username,
#                                 "remark": remark,
#                                 "colour": g.groupchat_members.all().first().user_icon.pure_color,
#                                 "ismyfriend": 1
#                             },
#                             "is_groupchat": g.is_groupchat,
#                         })
#                     if g.is_groupchat:
#                         data.append({
#                             "groupchat_id": g.id,
#                             "groupchat_name": g.groupchat_name,
#                             'groupchat_creater_id': g.groupchat_creater_id,
#                             "is_groupchat": g.is_groupchat,
#                         })
#
#
#             if not m1 and not m2:
#                 num = Message.objects.filter(groupchat_id=g.id).filter(
#                     isreaded=0).count()  # 筛选出【每一个聊天】的【未读的消息】并【计数】
#                 if not g.is_groupchat and g.messagegroupchat.all().first() is not None:  # 如果不是群聊，同时最新消息存在
#                     remark = Myfriend.objects.filter(
#                         myfriendslist__id=g.groupchat_members.all().first().id).first().remark
#                     data.append({
#                         "groupchat_id": g.id,
#                         "groupchat_name": g.groupchat_name,
#                         'groupchat_creater_id': g.groupchat_creater_id,
#                         "groupchat_member": {
#                             "user_id": g.groupchat_members.all().first().id,
#                             "user_name": g.groupchat_members.all().first().first_name,
#                             "login_name": g.groupchat_members.all().first().username,
#                             "remark": remark,
#                             "colour": g.groupchat_members.all().first().user_icon.pure_color,
#                             "ismyfriend": 1
#                         },
#                         'groupchat_message': {
#                             "text_content": g.messagegroupchat.all().last().text_content,
#                             "send_time": g.messagegroupchat.all().last().send_time.strftime(
#                                 '%Y-%m-%d %H:%M:%S')},  # 同时展示出群消息的第一条内容
#                         "num": num,
#                         "is_groupchat": g.is_groupchat,
#                         "send_time": g.messagegroupchat.all().last().send_time.strftime(
#                             '%Y-%m-%d %H:%M:%S')
#                     })  # 展示详细的结果
#
#                 if g.is_groupchat and g.messagegroupchat.all().first() is not None:
#                     data.append({
#                         "groupchat_id": g.id,
#                         "groupchat_name": g.groupchat_name,
#                         'groupchat_creater_id': g.groupchat_creater_id,
#                         'groupchat_message': {
#                             "text_content": g.messagegroupchat.all().last().text_content,
#                             "send_time": g.messagegroupchat.all().last().send_time.strftime(
#                                 '%Y-%m-%d %H:%M:%S'
#                             )},
#                         # 同时展示出群消息的第一条内容
#                         "num": num,
#                         "is_groupchat": g.is_groupchat,
#                         "send_time": g.messagegroupchat.all().last().send_time.strftime(
#                             '%Y-%m-%d %H:%M:%S')
#                     })
#
#                 if not g.is_groupchat and g.messagegroupchat.all().first() is None:
#                     remark = Myfriend.objects.filter(
#                         myfriendslist__id=g.groupchat_members.all().first().id).first().remark
#                     data.append({
#                         "groupchat_id": g.id,
#                         "groupchat_name": g.groupchat_name,
#                         'groupchat_creater_id': g.groupchat_creater_id,
#                         "groupchat_member": {
#                             "user_id": g.groupchat_members.all().first().id,
#                             "user_name": g.groupchat_members.all().first().first_name,
#                             "login_name": g.groupchat_members.all().first().username,
#                             "remark": remark,
#                             "colour": g.groupchat_members.all().first().user_icon.pure_color,
#                             "ismyfriend": 1
#                         },
#                         "is_groupchat": g.is_groupchat,
#                     })
#                 if g.is_groupchat and g.messagegroupchat.all().first() is None:
#                     data.append({
#                         "groupchat_id": g.id,
#                         "groupchat_name": g.groupchat_name,
#                         'groupchat_creater_id': g.groupchat_creater_id,
#                         "is_groupchat": g.is_groupchat,
#                     })
#
#
#
#
#
#
#
#
#
#
