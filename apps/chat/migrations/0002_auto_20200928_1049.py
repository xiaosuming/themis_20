# Generated by Django 2.2 on 2020-09-28 10:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('chat', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='enclosure',
            name='colour',
            field=models.CharField(default='#FF0000', max_length=7, verbose_name='头像颜色'),
        ),
    ]
