import random

from django.db.transaction import atomic
from guardian.shortcuts import assign_perm

from apps.console.models import GroupDetail
from apps.desktop.models import InstalledAPP
from apps.desktop.serializers import get_available_page_seq
from django.contrib.auth.models import User, Group
from django.db.models import Avg

from server.log import s_logger

enclosure_colour_choices = ["#4682B4", "#5F9EA0", "#008B8B", "#228B22", "#FF0000"]
groupchat_colour_choices = ["#4682B4", "#5F9EA0", "#008B8B", "#228B22", "#70DBDB"]


def generate_enclosure_color(instance, created, **kwargs):
    """
    新建附件时，生成随机颜色
    :param instance:
    :param kwargs:
    :return:
    """
    if created:
        instance.colour = random.choice(enclosure_colour_choices)
        instance.save()


def generate_groupchat_color(instance, created, **kwargs):
    """
    新建群聊对象时，生成随机颜色
    :param instance:
    :param kwargs:
    :return:
    """
    if created:
        instance.colour = random.choice(enclosure_colour_choices)
        instance.save()
