from django.conf.urls import url
from apps.applicationnotice.views import ApplicationsNotices, Notices, SingleNotice, SingleTask,  Applications

urlpatterns = [

    # 应用的url
    url(r'applications$', Applications.as_view()),  # 1：[GET]展示应用列表和未读消息统计 2：[POST]添加新应用

    # 消息的url
    url(r'applications/notices$', ApplicationsNotices.as_view()), #1：[GET]所有应用的消息的筛选和展示,
    url(r'applications/(?P<application_id>(\d+|self))/notices$', Notices.as_view()),#1：[POST]某一个应用消息的接收和存储  2：[GET]某一个应用消息的筛选和展示,  3：[PATCH]某一个应用消息全部变为已读
    url(r'notices/(?P<notice_id>\d+)$', SingleNotice.as_view()),#1：[PATCH]把某一条消息变为为已读和把一条消息置顶

    # 任务的url
    url(r'tasks/(?P<task_id>\d+)$', SingleTask.as_view()),#1：[GET]跳转和展示某一个任务
]
