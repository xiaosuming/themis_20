from server.settings import HOSTS
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.db.models import Q, Max
from django.http import JsonResponse
from django.views import View
from apps.application.models import Application
from apps.applicationnotice.models import Notice
import json
from apps.desktop.models import InstalledAPP


# 展示全部的应用列表
# 实现的逻辑：消息进来以后，获取消息所属的【应用名字】，如模型训练，如果消息所属的【应用名字】在【应用列表】里面，则忽略，如果不在，则添加在【应用列表】里。
class Applications(View):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        """
            @apiName show-application
            @api {POST} /api/v1/applications
            @apiGroup applications-apps
            @apiVersion 0.0.1
            @apiDescription [应用通知]向应用列表中添加应用
            @apiParam {int} application_id 应用id
            @apiSuccess {String} msg 信息
            @apiSuccess {String} code 0代表无错误 1代表有错误
            @apiSuccessExample {json} 成功返回样例:
            {
                "code": 0,
                "application_id": 3,
                "application_name": "模型训练",
            }
            @apiErrorExample {json} 失败返回样例 :
            {
                "code": 1,
                "msg": "应用已经存在",
            }
        """

    def post(self, request):

        # 获取参数
        log_user = request.user
        application_id = request.POST.get('application_id')  # 获取消息对用的【应用id】

        # 校验参数
        if not application_id or not log_user:
            res_data = {
                "msg": "参数缺失",
            }
            return JsonResponse(res_data, safe=False, status=406)

        # 执行逻辑
        installedapplication = InstalledAPP.objects.filter(
            Q(application_id=application_id) & Q(user_id=log_user.id)).first()  # 找出【应用展示列表】的所有应用
        if not installedapplication:  # 看消息对应的【应用id】，是不是在【应用展示列表中】
            application = InstalledAPP.objects.create(application_id=application_id, user_id=log_user.id)
            application.save()

            # 建立websocket链接
            log_user = request.user
            user_id = log_user.id
            channel_layer = get_channel_layer()
            async_to_sync(channel_layer.group_send)(
                "user_%s" % user_id,
                {
                    'type': 'chat_message',
                    'data':
                        {
                            "type": "application",
                            "application":
                                {
                                    "application_id": application_id,
                                    "application_name": application.application.name,
                                },
                        },
                })
            res = {
                "code": 0,
                "msg": "SUCCESS",
            }
            return JsonResponse(res, safe=False)

        else:  # 如果已经在里面，提示应用已经存在
            res = {
                "code": 1,
                "msg": '应用已经存在',
            }
            return JsonResponse(res, safe=False)

    """
        @apiName show-application
        @api {GET} /api/v1/applications
        @apiGroup applications-apps
        @apiVersion 0.0.1
        @apiDescription [应用通知]应用列表展示和未读消息统计
        @apiSuccess {String} msg 信息
        @apiSuccess {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 返回样例:
        {
        "code": 0,
        "msg": "SUCCESS",
        "data":
        [
            {
                "application.id": 1,
                "application_name": "模型训练",
                "num": 0,
                "application_icon": "http://192.168.50.42:8000/api/media/icon/moxingxunlian.svg"
            },
            {
                "application.id": 2,
                "application_name": "标注中心",
                "num": 2,
                "application_icon": "http://192.168.50.42:8000/api/media/icon/biaozhuzhongxin.svg"
            },
        ]
    """

    def get(self, request):  # 展示应用列表

        # 获取参数
        log_user = request.user

        # 校验参数
        if not log_user:
            res_data = {
                "msg": "参数缺失",
            }
            return JsonResponse(res_data, safe=False, status=406)

        # 执行逻辑
        # apps = InstalledAPP.objects.annotate(latest_msg_time=Max('applications__send_time'))  # 获取群组中最早的消息
        # apps = apps.order_by('-latest_msg_time')  # 根据每个群中最早的消息进行排序
        # applications = apps.filter(user_id=log_user.id)

        applications = InstalledAPP.objects.filter(user_id=log_user.id)
        notice = Notice.objects.filter(Q(notice_type="apply") & Q(owner_id=log_user.id)).first()
        data = []
        for application in applications:
            num = Notice.objects.filter(Q(application_id=application.id) & Q(owner_id=log_user.id) & Q(
                isreaded=0)).count()  # 筛选出【对应应用】的【未读的消息】并【计数】
            data.append({
                "application_id": application.id,
                "application_name": application.application.name,
                "num": num,
                "application_icon": application.get_logo(),
            })
        if notice:
            apply_num = Notice.objects.filter(Q(notice_type="apply") & Q(owner_id=log_user.id) & Q(
                isreaded=0)).count()  # 筛选出【对应应用】的【未读的消息】并【计数】
            data.append({
                "application_id": "self",
                "application_name": "消息中心",
                "num": apply_num,
                "application_icon": "{}{}".format(HOSTS["FRONTEND"],
                                                  "/api/files/?path=/home/project/media/application/logo/51cd4bcdd7ec1567d29fa16786e21163.svg"),
            })
        else:
            data.append({
                "application_id": "self",
                "application_name": "消息中心",
                "num": 0,
                "application_icon": "{}{}".format(HOSTS["FRONTEND"],
                                                  "/api/files/?path=/home/project/media/application/logo/51cd4bcdd7ec1567d29fa16786e21163.svg"),
            })

        res_data = {
            "code": 0,
            "msg": "SUCCESS",
            "data": data,
        }
        return JsonResponse(res_data, safe=False)


class Notices(View):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    """
          @apiName save-notices
          @api {POST} /api/v1/applications/{application_id}/notices
          @apiGroup applications-notices
          @apiVersion 0.0.1
          @apiDescription [应用通知]保存消息
          @apiParam {int} isreaded 0代表未读/1代表已读
          @apiParam {int} isaccept 0代表拒绝/1代表接受
          @apiParam {int} application_id 所属应用id
          @apiParam {String} notice_content 通知内容
          @apiParam {String} notice_type 通知类型
          @apiParam {String} sender_type 发送者类型              
          @apiSuccess {String} msg 信息
          @apiSuccess {String} code 0代表无错误 1代表有错误
          @apiSuccessExample {json} 返回样例:
          {
              "code": 0,
              "msg": "SUCCESS",
          }
    """

    def post(self, request, application_id):  # 获取新的消息

        # 获取参数
        log_user = request.user  # 接收人
        notice_content = request.POST.get('notice_content')
        user_id = request.POST.get('user_id')  # 发送人
        notice_type = request.POST.get('notice_type')
        sender_type = request.POST.get('sender_type')
        isaccept = request.POST.get('isaccept')
        isreaded = request.POST.get('isreaded')

        # 校验参数
        if not notice_content or not user_id or not notice_type or not sender_type:
            res_data = {
                "msg": "参数缺失",
            }
            return JsonResponse(res_data, safe=False, status=406)

        # 执行逻辑
        notice = Notice()
        notice.notice_content = notice_content
        notice.user_id = user_id
        notice.notice_type = notice_type
        notice.sender_type = sender_type
        notice.isaccept = isaccept
        notice.isaccept = isreaded
        notice.application_id = application_id
        notice.owner_id = log_user.id
        notice.save()  # 保存新的消息

        # 建立websocket链接
        log_user = request.user
        user_id = log_user.id
        channel_layer = get_channel_layer()
        async_to_sync(channel_layer.group_send)(
            "user_%s" % user_id,
            {
                'type': 'chat_message',
                'data':
                    {
                        "type": "application",
                        "application":
                            {
                                "notice_id": notice.id,
                                'user_id': notice.user_id,
                                'user_name': notice.user.first_name,
                                'colour': notice.user.user_icon.pure_color,
                                'isaccept': notice.isaccept,
                                'isreaded': notice.isreaded,
                                'application_id': notice.application_id,
                                'application_name': notice.application.name,
                                'notice_content': notice.notice_content,
                                'notice_type': notice.notice_type,
                                'sender_type': notice.sender_type,
                            },
                    },
            })

        res = {
            "code": 0,
            "msg": "SUCCESS",
        }
        return JsonResponse(res, safe=False)

    """
        @apiName show-notices
        @api {GET} /api/v1/applications/{application_id}/notices
        @apiGroup applications-notices
        @apiVersion 0.0.1
        @apiDescription [应用通知]筛选和展示消息
        @apiParam {int} isreaded 0代表未读/1代表已读
        @apiParam {int} application_id 所属应用id
        @apiParam {String} keyword 关键字
        @apiSuccess {String} msg 信息
        @apiSuccess {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data":[{
                "notice_id": 6,
                "isaccept": true,
                "isreaded": true,
                "application_id": 6,
                "application_name": "消息中心",
                "notice_content": "申请加您为好友",
                "notice_type": "apply",
                "sender_type": "system",
                "send_time": "2020-08-26 17:38:40",
                "order_by": "-send_time"
            },
            {
                "notice_id": 11,
                "isaccept": false,
                "isreaded": true,
                "application_id": 1,
                "application_name": "模型训练",
                "notice_content": "任务”动物“切片完成，可以进行任务划分。",
                "notice_type": "notice",
                "sender_type": "system",
                "tasks": [
                    {
                        "task_name": "动物",
                        "task_url": "http://192.168.50.42:8000/api/v1/tasks/1"
                    }
                ],
                "send_time": "2020-08-26 17:33:00",
                "order_by": "-send_time"
            }
            ]
        }
    """

    # 展示全部的消息列表和实现筛选功能（筛选是以【是否已读】和【应用名】，【关键字】进行筛选）
    def get(self, request, application_id):  # 查看所有的消息通知列表

        # 获取参数
        log_user = request.user
        isreaded = request.GET.get('isreaded')  # 获取是否已读参数
        keyword = request.GET.get('keyword')

        # 校验参数
        if not log_user:
            res_data = {
                "msg": "参数缺失",
            }
            return JsonResponse(res_data, safe=False, status=406)

        # 执行逻辑
        data = []
        if application_id == "self":
            if isreaded and application_id:  # 如果isreaded参数存在，进行是否已读筛选
                notices = Notice.objects.filter(
                    Q(isreaded=isreaded) & Q(notice_type="apply") & Q(owner_id=log_user.id)).order_by(
                    '-send_time')

            elif keyword and application_id:  # 如果application_id参数存在，进行关键字筛选
                notices = Notice.objects.filter(
                    (Q(notice_content__icontains=keyword) | Q(notice_type__icontains=keyword)) & Q(
                        notice_type="apply") & Q(owner_id=log_user.id)).order_by('-send_time')

            else:
                notices = Notice.objects.filter(Q(notice_type="apply") & Q(owner_id=log_user.id)).order_by('-send_time')

            for n in notices:  # 返回展示结果
                data.append({
                    "notice_id": n.id,
                    'user_id': n.user_id,
                    'user_name': n.user.first_name,
                    'colour': n.user.user_icon.pure_color,
                    'isaccept': n.isaccept,
                    'isreaded': n.isreaded,
                    'application_id': "self",
                    'application_name': "消息中心",
                    'notice_content': n.notice_content,
                    'notice_type': n.notice_type,
                    'sender_type': n.sender_type,
                    'send_time': n.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                    'order_by': '-send_time'
                })
            res_data = {
                "code": 0,
                "msg": "SUCCESS",
                "data": data,
            }
            return JsonResponse(res_data, safe=False)

        else:
            if isreaded and application_id:  # 如果isreaded参数存在，进行是否已读筛选
                notices = Notice.objects.filter(
                    Q(isreaded=isreaded) & Q(application_id=application_id) & Q(owner_id=log_user.id)).order_by(
                    '-send_time')

            elif keyword and application_id:  # 如果application_id参数存在，进行关键字筛选
                notices = Notice.objects.filter(
                    (Q(notice_content__icontains=keyword) | Q(notice_type__icontains=keyword)) & Q(
                        application_id=application_id) & Q(owner_id=log_user.id)).order_by('-send_time')

            else:  # 如果application_id参数存在，进行应用名筛选。
                notices = Notice.objects.filter(Q(application_id=application_id) & Q(owner_id=log_user.id)).order_by(
                    '-send_time')
            for n in notices:  # 返回展示结果
                if n.sender_type == 'user' and n.tasks.first() is not None:
                    data.append({
                        "notice_id": n.id,
                        'user_id': n.user_id,
                        'user_name': n.user.first_name,
                        'colour': n.user.user_icon.pure_color,
                        'isaccept': n.isaccept,
                        'isreaded': n.isreaded,
                        'application_id': n.application_id,
                        'application_name': n.application.name,
                        'notice_content': n.notice_content,
                        'notice_type': n.notice_type,
                        'sender_type': n.sender_type,
                        'tasks': [{'task_name': n.tasks.first().task_name, 'task_url': n.tasks.first().task_url}],
                        'send_time': n.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                        'order_by': '-send_time'
                    })

                elif n.sender_type == 'user' and n.tasks.first() is None:
                    data.append({
                        "notice_id": n.id,
                        'user_id': n.user_id,
                        'user_name': n.user.first_name,
                        'colour': n.user.user_icon.pure_color,
                        'isaccept': n.isaccept,
                        'isreaded': n.isreaded,
                        'application_id': n.application_id,
                        'application_name': n.application.name,
                        'notice_content': n.notice_content,
                        'notice_type': n.notice_type,
                        'sender_type': n.sender_type,
                        'send_time': n.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                        'order_by': '-send_time'
                    })

                elif n.sender_type == 'system' and n.tasks.first() is not None:
                    data.append({
                        "notice_id": n.id,
                        'isaccept': n.isaccept,
                        'isreaded': n.isreaded,
                        'application_id': n.application_id,
                        'application_name': n.application.name,
                        'notice_content': n.notice_content,
                        'notice_type': n.notice_type,
                        'sender_type': n.sender_type,
                        'tasks': [{'task_name': n.tasks.first().task_name, 'task_url': n.tasks.first().task_url}],
                        'send_time': n.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                        'order_by': '-send_time'
                    })

                else:
                    data.append({
                        "notice_id": n.id,
                        'isaccept': n.isaccept,
                        'isreaded': n.isreaded,
                        'application_id': n.application_id,
                        'application_name': n.application.name,
                        'notice_content': n.notice_content,
                        'notice_type': n.notice_type,
                        'sender_type': n.sender_type,
                        'send_time': n.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                        'order_by': '-send_time'
                    })

            res_data = {
                "code": 0,
                "msg": "SUCCESS",
                "data": data,
            }
            return JsonResponse(res_data, safe=False)

    """
        @apiName set-isreaded
        @api {PATCH} /api/v1/applications/{application_id}/notices
        @apiGroup applications-notices
        @apiVersion 0.0.1
        @apiDescription [应用通知]具体某一个应用消息全部标为已读
        @apiParam {int} isreaded 0代表未读/1代表已读
        @apiParam {int} application_id 所属应用id
        @apiSuccess {String} msg 信息
        @apiSuccess {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
        }
    """

    def patch(self, request, application_id):

        # 获取参数
        log_user = request.user
        put_data = json.loads(request.body)
        isreaded = put_data.get('isreaded')

        # 校验参数
        if not log_user or not isreaded:
            res_data = {
                "msg": "参数缺失",
            }
            return JsonResponse(res_data, safe=False, status=406)

        # 执行逻辑
        if application_id == "self":
            notices = Notice.objects.filter(
                Q(isreaded=0) & Q(notice_type="apply") & Q(owner_id=log_user.id))  # 查找表中是未读（isreaded=0）的数据
            if notices:  # 如果数据存在
                notices.update(isreaded=isreaded)  # 则全部转换为已读
            else:  # 如果数据不存在，忽略不计
                pass
        else:
            notices = Notice.objects.filter(
                Q(isreaded=0) & Q(application_id=application_id) & Q(owner_id=log_user.id))  # 查找表中是未读（isreaded=0）的数据
            if notices:  # 如果数据存在
                notices.update(isreaded=isreaded)  # 则全部转换为已读
            else:  # 如果数据不存在，忽略不计
                pass

        res_data = {
            "code": 0,
            "msg": "SUCCESS",
        }
        return JsonResponse(res_data, safe=False)


# 把单个消息从未读变成已读
class SingleNotice(View):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    """
        @apiName change-isreaded
        @api {PATCH} /api/v1/notices/{notice_id}
        @apiGroup applications-notices
        @apiVersion 0.0.1
        @apiDescription [应用通知]消息从未读变为已读，消的拒绝或接受
        @apiParam {int} notice_id 消息id
        @apiParam {int} isreaded  0是未读/1是已读
        @apiParam {int} isaccept  0是拒绝/1是接受
        @apiSuccess {String} msg 信息
        @apiSuccess {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
        }
    """

    def patch(self, request, notice_id):

        # 获取参数
        put_data = json.loads(request.body)
        isreaded = put_data.get('isreaded')
        isaccept = put_data.get('isaccept')

        # # 校验参数
        # if not isreaded :
        #     res_data = {
        #         "code": 406,
        #         "msg": "参数缺失",
        #     }
        #     return JsonResponse(res_data, safe=False)
        #
        # # 校验参数
        # if not isaccept :
        #     res_data = {
        #         "code": 406,
        #         "msg": "参数缺失",
        #     }
        #     return JsonResponse(res_data, safe=False)

        if isreaded:
            Notice.objects.filter(id=notice_id).update(isreaded=isreaded)

        if isaccept:
            Notice.objects.filter(id=notice_id).update(isaccept=isaccept)

        res_data = {
            "code": 0,
            "msg": "SUCCESS",
        }
        return JsonResponse(res_data, safe=False)


# 点击任务名，进行跳转.
class SingleTask(View):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        """
            @apiName link-task
            @api {GET} /api/v1/tasks/{task_id}
            @apiGroup applications-tasks
            @apiVersion 0.0.1
            @apiDescription [应用通知]点击任务名，进行链接跳转
            @apiParam {int} task_id 任务id
            @apiSuccess {String} msg 信息
            @apiSuccess {String} code 0代表无错误 1代表有错误
            @apiSuccessExample {json} 返回样例:
            {
                "code": 0,
                "msg": "SUCCESS",
            }
        """

    def get(self, request, task_id):
        if task_id:
            pass
        res_data = {
            "code": 0,
            "msg": "SUCCESS",
        }
        return JsonResponse(res_data, safe=False)


# 所有应用的消息的【关键字】筛选
class ApplicationsNotices(View):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    """
        @apiName show-applicationsnotices
        @api {GET} /api/v1/applications/notices
        @apiGroup applications-notices
        @apiVersion 0.0.1
        @apiDescription [应用通知]所有应用的消息的筛选和展示
        @apiParam {String} keyword 关键字
        @apiSuccess {String} msg 信息
        @apiSuccess {String} code 0代表无错误 1代表有错误
        @apiSuccessExample {json} 返回样例:
        {
            "code": 0,
            "msg": "SUCCESS",
            "data": [
                {
                    "notice_id": 11,
                    "isaccept": false,
                    "isreaded": true,
                    "application_id": 1,
                    "application_name": "模型训练",
                    "notice_content": "任务”动物“切片完成，可以进行任务划分。",
                    "notice_type": "notice",
                    "sender_type": "system",
                    "tasks": [
                        {
                            "task_name": "动物",
                            "task_url": "http://192.168.50.42:8000/api/v1/tasks/1"
                        }
                    ],
                    "send_time": "2020-08-26 17:33:00",
                    "order_by": "-send_time"
                }
            ]
        }
        
    """

    # 所有应用的消息的【关键字】筛选
    def get(self, request):

        # 获取参数
        data = []
        log_user = request.user
        keyword = request.GET.get('keyword')

        # 校验参数
        # if not keyword :
        #     res_data = {
        #         "code": 406,
        #         "msg": "参数缺失",
        #     }
        #     return JsonResponse(res_data, safe=False)

        # 执行逻辑
        if keyword:  # 进行关键字筛选
            notices = Notice.objects.filter(
                (Q(notice_content__icontains=keyword) | Q(notice_type__icontains=keyword)) & Q(
                    owner_id=log_user.id)).order_by('-send_time')
            for n in notices:  # 返回展示结果
                if n.notice_type == "apply":
                    data.append({
                        "notice_id": n.id,
                        'user_id': n.user_id,
                        'user_name': n.user.first_name,
                        'colour': n.user.user_icon.pure_color,
                        'isaccept': n.isaccept,
                        'isreaded': n.isreaded,
                        'application_id': "self",
                        'application_name': "消息中心",
                        'notice_content': n.notice_content,
                        'notice_type': n.notice_type,
                        'sender_type': n.sender_type,
                        'send_time': n.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                        'order_by': '-send_time'
                    })
                else:
                    if n.sender_type == 'user' and n.tasks.first() is not None:
                        data.append({
                            "notice_id": n.id,
                            'user_id': n.user_id,
                            'user_name': n.user.first_name,
                            'colour': n.user.user_icon.pure_color,
                            'isaccept': n.isaccept,
                            'isreaded': n.isreaded,
                            'application_id': n.application_id,
                            'application_name': n.application.name,
                            'notice_content': n.notice_content,
                            'notice_type': n.notice_type,
                            'sender_type': n.sender_type,
                            'tasks': [{'task_name': n.tasks.first().task_name, 'task_url': n.tasks.first().task_url}],
                            'send_time': n.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            'order_by': '-send_time'
                        })

                    if n.sender_type == 'user' and n.tasks.first() is None:
                        data.append({
                            "notice_id": n.id,
                            'user_id': n.user_id,
                            'user_name': n.user.first_name,
                            'colour': n.user.user_icon.pure_color,
                            'isaccept': n.isaccept,
                            'isreaded': n.isreaded,
                            'application_id': n.application_id,
                            'application_name': n.application.name,
                            'notice_content': n.notice_content,
                            'notice_type': n.notice_type,
                            'sender_type': n.sender_type,
                            'send_time': n.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            'order_by': '-send_time'
                        })

                    if n.sender_type == 'system' and n.tasks.first() is not None:
                        data.append({
                            "notice_id": n.id,
                            'isaccept': n.isaccept,
                            'isreaded': n.isreaded,
                            'application_id': n.application_id,
                            'application_name': n.application.name,
                            'notice_content': n.notice_content,
                            'notice_type': n.notice_type,
                            'sender_type': n.sender_type,
                            'tasks': [{'task_name': n.tasks.first().task_name, 'task_url': n.tasks.first().task_url}],
                            'send_time': n.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            'order_by': '-send_time'
                        })

                    if n.sender_type == 'system' and n.tasks.first() is None:
                        data.append({
                            "notice_id": n.id,
                            'isaccept': n.isaccept,
                            'isreaded': n.isreaded,
                            'application_id': n.application_id,
                            'application_name': n.application.name,
                            'notice_content': n.notice_content,
                            'notice_type': n.notice_type,
                            'sender_type': n.sender_type,
                            'send_time': n.send_time.strftime('%Y-%m-%d %H:%M:%S'),
                            'order_by': '-send_time'
                        })
        else:
            pass

        res_data = {
            "code": 0,
            "msg": "SUCCESS",
            "data": data,
        }
        return JsonResponse(res_data, safe=False)
