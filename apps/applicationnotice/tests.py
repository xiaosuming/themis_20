# from asgiref.sync import async_to_sync
# from channels.layers import get_channel_layer
# from django.contrib.auth.models import User
# from django.db.models import Q
# from django.http import JsonResponse
# from django.views import View
# from applicationnotice.models import Application, Notice, Application
# from chat.models import Myfriend
# import json
#
#
# # 展示全部的应用列表
# # 实现的逻辑：消息进来以后，获取消息所属的【应用名字】，如模型训练，如果消息所属的【应用名字】在【应用列表】里面，则忽略，如果不在，则添加在【应用列表】里。
# class Applications(View):
#     def __init__(self, **kwargs):
#         super().__init__(**kwargs)
#
#
#         """
#             @apiName show-application
#             @api {POST} /applications
#             @apiGroup applications-application
#             @apiVersion 0.0.1
#             @apiDescription [应用通知]向应用列表中添加应用
#             @apiParam {int} application_id 应用id
#             @apiSuccess {String} msg 信息
#             @apiSuccess {String} code 0代表无错误 1代表有错误
#             @apiSuccessExample {json} 成功返回样例:
#             {
#                 "code": 0,
#                 "application_id": 3,
#                 "application_name": "模型训练",
#             }
#             @apiErrorExample {json} 失败返回样例 :
#             {
#                 "code": 1,
#                 "msg": "应用已经存在",
#             }
#         """
#
#
#     def post(self, request):
#         application_id = request.POST.get('application_id')  # 获取消息对用的【应用id】
#         app_name = Application.objects.filter(id=application_id).first().application_name  # 通过【应用id】找出对应的【应用名字】
#         applications = Application.objects.all().values()  # 找出【应用展示列表】的所有应用
#         try:
#             if app_name not in applications:  # 看消息对应的【应用名字】，是不是在【应用展示列表中】
#                 application = Application()  # 如果不在里面，把他加到里面，并展示出来
#                 application.name = app_name
#                 application.save()
#
#
#                 # 建立websocket链接
#                 log_user=request.user
#                 user_id=log_user.id
#                 channel_layer = get_channel_layer()
#                 async_to_sync(channel_layer.group_send)(
#                     "user_%s" % user_id,
#                     {
#                         'type': 'chat_message',
#                         'data':
#                             {
#                                 "type": "application",
#                                 "application":
#                                     {
#                                         "application_id": application_id,
#                                         "application_name": app_name,
#                                     },
#                             },
#                     })
#
#
#
#
#                 res = {
#                     "code": 0,
#                     "msg": "SUCCESS",
#                     "application_name": app_name,
#                 }
#                 return JsonResponse(res, safe=False)
#         except:  # 如果已经在里面，提示应用已经存在
#             res = {
#                 "code": 1,
#                 "msg": '应用已经存在',
#             }
#             return JsonResponse(res, safe=False)
#
#
#     """
#         @apiName show-application
#         @api {GET} /applications
#         @apiGroup applications-application
#         @apiVersion 0.0.1
#         @apiDescription [应用通知]应用列表展示和未读消息展示
#         @apiSuccess {String} msg 信息
#         @apiSuccess {String} code 0代表无错误 1代表有错误
#         @apiSuccessExample {json} 返回样例:
#         {
#         "code": 0,
#         "msg": "SUCCESS",
#         "data":
#         [
#             {
#                 "application.id": 1,
#                 "application_name": "模型训练",
#                 "num": 0,
#                 "application_icon": "http://192.168.50.43:8000//api/media/icon/moxingxunlian.svg"
#             },
#             {
#                 "application.id": 2,
#                 "application_name": "标注中心",
#                 "num": 2,
#                 "application_icon": "http://192.168.50.43:8000//api/media/icon/biaozhuzhongxin.svg"
#             },
#         ]
#     """
#
#     def get(self, request):  # 展示应用列表
#         # global num, application
#         applications = Application.objects.all()
#         res_data = []
#         for application in applications:
#             num = Notice.objects.filter(application_id=application.id).filter(
#                 isreaded=0).count()  # 筛选出【对应应用】的【未读的消息】并【计数】
#             res_data.append({
#                 "application.id": application.id,
#                 "application_name": application.name,
#                 "num": num,
#                 "application_icon": application.get_icon_url(),
#             })
#         return JsonResponse(res_data, safe=False)
#
#
# # 展示全部的消息列表和实现筛选功能（筛选是以【是否已读】和【应用名】，【关键字】进行筛选）
# class Notices(View):
#     def __init__(self, **kwargs):
#         super().__init__(**kwargs)
#
#
#
#     # def post(self, request):  # 获取新的消息
#     #     notice_content = request.POST.get('notice_content')
#     #     notice_type = request.POST.get('notice_type')
#     #     isaccept = request.POST.get('isaccept')
#     #     isreaded = request.POST.get('isreaded')
#     #     application_id = request.POST.get('application_id')
#     #     notice = Notice()
#     #     notice.notice_content = notice_content
#     #     notice.notice_type = notice_type
#     #     notice.isaccept = isaccept
#     #     notice.isaccept = isreaded
#     #     notice.application_id = application_id
#     #     notice.save()  # 保存新的消息
#     #     res = {
#     #         "code": 0,
#     #         "msg": "SUCCESS",
#     #     }
#     #     return JsonResponse(res, safe=False)
#
#
#
#     """
#         @apiName show-notices
#         @api {GET} /notices
#         @apiGroup applications-notices
#         @apiVersion 0.0.1
#         @apiDescription [应用通知]筛选和展示消息
#         @apiParam {int} isreaded 0代表未读/1代表已读
#         @apiParam {int} application_id 所属应用id
#         @apiParam {String} keyword 关键字
#         @apiSuccess {String} msg 信息
#         @apiSuccess {String} code 0代表无错误 1代表有错误
#         @apiSuccessExample {json} 返回样例:
#         [
#
#             {
#                 "notice_id": 6,
#                 "isaccept": true,
#                 "isreaded": true,
#                 "application_id": 6,
#                 "application_name": "消息中心",
#                 "notice_content": "申请加您为好友",
#                 "notice_type": "apply",
#                 "sender_type": "system",
#                 "send_time": "2020-08-26 17:38:40",
#                 "order_by": "-send_time"
#             },
#             {
#                 "notice_id": 5,
#                 "user_id": 3,
#                 "user_name": "张伟",
#                 "colour": "#70DBDB",
#                 "isaccept": false,
#                 "isreaded": true,
#                 "application_id": 3,
#                 "application_name": "模型发布",
#                 "notice_content": "评论了你的实验“汽车识别”：好厉害，我能加你为好友吗？",
#                 "notice_type": "comment",
#                 "sender_type": "user",
#                 "tasks": [
#                     {
#                         "task_name": "汽车识别 ",
#                         "task_url": "http://192.168.50.42:8000/applicationnotice/link-applications"
#                     }
#                 ],
#                 "send_time": "2020-08-26 17:37:44",
#                 "order_by": "-send_time"
#             },
#         ]
#     """
#
#     def get(self, request):  # 查看所有的消息通知列表
#         isreaded = request.GET.get('isreaded')  # 获取是否已读参数
#         application_id = request.GET.get('application_id')  # 获取应用id参数
#         keyword = request.GET.get('keyword')
#         if isreaded and application_id:  # 如果isreaded参数存在，进行是否已读筛选
#             notices = Notice.objects.filter(Q(isreaded=isreaded) & Q(application_id=application_id)).order_by(
#                 '-send_time')
#         elif application_id:  # 如果application_id参数存在，进行应用名筛选。
#             notices = Notice.objects.filter(application_id=application_id).order_by('-send_time')
#         elif keyword:  # 如果application_id参数存在，进行关键字筛选
#             notices = Notice.objects.filter(
#                 Q(notice_content__icontains=keyword) | Q(notice_type__icontains=keyword)).order_by('-send_time')
#         else:  # 否则，不筛选，显示全部消息
#             notices = Notice.objects.all().order_by('-send_time')
#             print(notices)
#         res_data = []
#         for n in notices:  # 返回展示结果
#             if n.sender_type == 'user' and n.tasks.first() is not None:
#                 res_data.append({
#                     "notice_id": n.id,
#                     'user_id': n.user_id,
#                     'user_name': n.user.first_name,
#                     'colour': n.user.user_icon.pure_color,
#                     'isaccept': n.isaccept,
#                     'isreaded': n.isreaded,
#                     'application_id': n.application_id,
#                     'application_name': n.application.name,
#                     'notice_content': n.notice_content,
#                     'notice_type': n.notice_type,
#                     'sender_type': n.sender_type,
#                     'tasks': [{'task_name': n.tasks.first().task_name, 'task_url': n.tasks.first().task_url}],
#                     'send_time': n.send_time.strftime('%Y-%m-%d %H:%M:%S'),
#                     'order_by': '-send_time'
#                 })
#
#             if n.sender_type == 'user' and n.tasks.first() is None:
#                 res_data.append({
#                     "notice_id": n.id,
#                     'user_id': n.user_id,
#                     'user_name': n.user.first_name,
#                     'colour': n.user.user_icon.pure_color,
#                     'isaccept': n.isaccept,
#                     'isreaded': n.isreaded,
#                     'application_id': n.application_id,
#                     'application_name': n.application.name,
#                     'notice_content': n.notice_content,
#                     'notice_type': n.notice_type,
#                     'sender_type': n.sender_type,
#                     'send_time': n.send_time.strftime('%Y-%m-%d %H:%M:%S'),
#                     'order_by': '-send_time'
#                 })
#
#             if n.sender_type == 'system' and n.tasks.first() is not None:
#                 res_data.append({
#                     "notice_id": n.id,
#                     'isaccept': n.isaccept,
#                     'isreaded': n.isreaded,
#                     'application_id': n.application_id,
#                     'application_name': n.application.name,
#                     'notice_content': n.notice_content,
#                     'notice_type': n.notice_type,
#                     'sender_type': n.sender_type,
#                     'tasks': [{'task_name': n.tasks.first().task_name, 'task_url': n.tasks.first().task_url}],
#                     'send_time': n.send_time.strftime('%Y-%m-%d %H:%M:%S'),
#                     'order_by': '-send_time'
#                 })
#
#             if n.sender_type == 'system' and n.tasks.first() is None:
#                 res_data.append({
#                     "notice_id": n.id,
#                     'isaccept': n.isaccept,
#                     'isreaded': n.isreaded,
#                     'application_id': n.application_id,
#                     'application_name': n.application.name,
#                     'notice_content': n.notice_content,
#                     'notice_type': n.notice_type,
#                     'sender_type': n.sender_type,
#                     'send_time': n.send_time.strftime('%Y-%m-%d %H:%M:%S'),
#                     'order_by': '-send_time'
#                 })
#
#         return JsonResponse(res_data, safe=False)
#
#
#     """
#         @apiName set-isreaded
#         @api {PATCH} /notices
#         @apiGroup applications-notices
#         @apiVersion 0.0.1
#         @apiDescription [应用通知]消息全部标为已读
#         @apiParam {int} isreaded 0代表未读/1代表已读
#         @apiSuccess {String} msg 信息
#         @apiSuccess {String} code 0代表无错误 1代表有错误
#         @apiSuccessExample {json} 返回样例:
#         {
#             "code": 0,
#             "msg": "SUCCESS",
#         }
#     """
#
#
#     def patch(self, request):
#
#         put_data = json.loads(request.body)
#         isreaded = put_data.get('isreaded')
#         notices = Notice.objects.filter(isreaded=isreaded)  # 查找表中是未读（isreaded=0）的数据
#         if notices:  # 如果数据存在
#             notices.update(isreaded=1)  # 则全部转换为已读
#         else:  # 如果数据不存在，忽略不计
#             pass
#         res_data = {
#             "code": 0,
#             "msg": "SUCCESS",
#         }
#         return JsonResponse(res_data, safe=False)
#
#
#
# # 把单个消息从未读变成已读
# class SingleNotice(View):
#     def __init__(self, **kwargs):
#         super().__init__(**kwargs)
#
#     """
#         @apiName change-isreaded
#         @api {PATCH} /notices/{notice_id}
#         @apiGroup applications-notices
#         @apiVersion 0.0.1
#         @apiDescription [应用通知]消息从未读变为已读
#         @apiParam {int} notice_id 消息id
#         @apiSuccess {String} msg 信息
#         @apiSuccess {String} code 0代表无错误 1代表有错误
#         @apiSuccessExample {json} 返回样例:
#         {
#             "code": 0,
#             "msg": "SUCCESS",
#         }
#     """
#
#
#     def patch(self, request,notice_id):
#         # put_data = json.loads(request.body)
#         # notice_id = put_data.get('notice_id')
#         if notice_id:  # 如果id存在
#             Notice.objects.filter(id=notice_id).update(isreaded=1)
#         else:  # 如果数据不存在，忽略不计
#             pass
#         res_data = {
#             "code": 0,
#             "msg": "SUCCESS",
#         }
#         return JsonResponse(res_data, safe=False)
#
#
#
# # 点击任务名，进行跳转.
# class SingleTask(View):
#     def __init__(self, **kwargs):
#         super().__init__(**kwargs)
#
#         """
#             @apiName link-task
#             @api {GET} /tasks/{task_id}
#             @apiGroup applications-tasks
#             @apiVersion 0.0.1
#             @apiDescription [应用通知]点击任务名，进行链接跳转
#             @apiParam {int} task_id 任务id
#             @apiSuccess {String} msg 信息
#             @apiSuccess {String} code 0代表无错误 1代表有错误
#             @apiSuccessExample {json} 返回样例:
#             {
#                 "code": 0,
#                 "msg": "SUCCESS",
#             }
#         """
#
#     def get(self, request,task_id):
#         if task_id:
#             pass
#         res_data = {
#             "code": 0,
#             "msg": "SUCCESS",
#         }
#         return JsonResponse(res_data, safe=False)
