from apps.application.models import Application
from apps.desktop.models import InstalledAPP
from apps.desktop.serializers import get_available_page_seq
from apps.console.models import UserIcon, get_color


def install_default_app(instance, **kwargs):
    """
    新建用户时，安装默认应用
    :param instance: User对象
    :param kwargs:
    :return:
    """
    default_apps = Application.objects.filter(is_default=True)
    for default_app in default_apps:
        page, seq = get_available_page_seq(instance)
        installed_app, created = InstalledAPP.objects.get_or_create(user=instance, application=default_app)
        if created:
            installed_app.page = page
            installed_app.seq = seq
            installed_app.save()


def create_portrait(instance, **kwargs):
    """
    新建用户时生成纯色头像
    :param instance:
    :param kwargs:
    :return:
    """
    db_icon, created = UserIcon.objects.get_or_create(user=instance)
    if created:
        db_icon.pure_color = get_color()
        db_icon.save()
