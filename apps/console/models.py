import os
from django.db import models
from django.contrib.auth.models import User, Group
from random import choice
from apps.application.models import hash_file, Application
from server import settings

PORTRAIT_CHOICES = (
    "#FE375F", "#C059F2", "#2A84FF",
    "#32D74B", "#5E5CE6", "#64D2FF",
    "#FD453A", "#FD9F0A", "#FDD50A"
)
GROUP_TYPE_CHOICES = (
    ("用户组", "普通用户组"),
    ("管理组", "普通管理组"),
    ("全局管理组", "管理所有应用的用户组"),
    ("应用管理账户", "管理对应应用的管理组和用户组"),
    ("全局管理账户", "管理所有的管理组和用户组")
)


class AttributeType(models.Model):
    name = models.CharField(
        max_length=32, verbose_name="属性名称", db_index=True, unique=True)
    description = models.CharField(max_length=256, verbose_name="属性描述")

    class Meta:
        verbose_name = "自定义属性"
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name


class AttributeKey(models.Model):
    key = models.CharField(max_length=32, verbose_name="自定义字段", db_index=True)
    type = models.CharField(max_length=32, verbose_name="自定义字段的类型", default="")
    number = models.CharField(
        max_length=32, verbose_name="自定义字段的数量", default="")
    attr_type = models.ForeignKey(
        to=AttributeType, related_name="attr_key", on_delete=models.CASCADE, null=True)

    class Meta:
        verbose_name = "自定义字段"
        verbose_name_plural = verbose_name
        unique_together = ("key", "attr_type")

    def __str__(self):
        return self.key


class AttributeValue(models.Model):
    value = models.CharField(max_length=32, verbose_name="自定义字段的值")
    key = models.ForeignKey(
        to=AttributeKey, related_name="attr_value", on_delete=models.CASCADE)
    user = models.ForeignKey(to=User, on_delete=models.CASCADE, null=True)

    class Meta:
        verbose_name = "自定义字段的值"
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.value


def icon_upload(instance, filename):
    """
    用户头像上传函数
    :param instance:
    :param filename:
    :return:
    """
    instance.icon.open()
    pic_ext = filename.rsplit('.')[1]
    pic_path = os.path.join('head_icons', instance.user_id, '{}.{}'.format(hash_file(
        instance.icon), pic_ext))

    return pic_path


def get_color():
    return choice(PORTRAIT_CHOICES)


class UserIcon(models.Model):
    user = models.OneToOneField(
        to=User, on_delete=models.CASCADE, related_name="user_icon")
    icon = models.FileField(max_length=128, null=True, upload_to=icon_upload,
                            verbose_name='用户头像存储路径', help_text='用户头像存储路径')
    pure_color = models.CharField(max_length=8, default='#5E5CE6',
                                  verbose_name='头像颜色', help_text='头像颜色')

    class Meta:
        verbose_name = "用户头像"
        verbose_name_plural = verbose_name

    def return_icon(self):
        """返回头像访问地址"""
        if self.icon:
            pic_url = os.path.join(settings.HOSTS["FRONTEND"], "api/files",
                                   "?path={}".format(self.icon.path))
        else:
            pic_url = None
        return pic_url


class GroupDetail(models.Model):
    group = models.OneToOneField(Group, verbose_name="所属群组", on_delete=models.CASCADE)
    application = models.ForeignKey(Application, verbose_name="所属应用", null=True, on_delete=models.CASCADE)
    description = models.CharField(max_length=128, verbose_name="群组描述", null=True)
    group_type = models.CharField(max_length=16, verbose_name="群组类型", choices=GROUP_TYPE_CHOICES)
    app_admin = models.ForeignKey(User, verbose_name="应用管理员", null=True, on_delete=models.SET_NULL)

    class Meta:
        verbose_name = "群组详情"
        verbose_name_plural = verbose_name
