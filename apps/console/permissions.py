import re

from django.contrib.auth.models import User
from rest_framework.permissions import BasePermission

from apps.application.models import Application
from apps.console.models import GroupDetail


class IsSuperAdmin(BasePermission):
    """系统默认管理员"""

    def has_permission(self, request, view):
        return request.user.username == "admin"


class IsSuperAdminOrAppAdmin(BasePermission):
    """系统默认管理员或应用默认管理员"""

    def has_permission(self, request, view):
        return request.user.is_staff


class IsManageUser(BasePermission):
    """拥有应用管理权限"""

    def has_permission(self, request, view):
        user = request.user
        return check_admin_auth(user)


class IsUserSelf(BasePermission):
    """是否为用户本人"""

    def has_permission(self, request, view):
        return is_user_self(request)


class IsUserCreator(BasePermission):
    """是否是用户创建者"""

    def has_permission(self, request, view):
        return is_user_creator(request)


class IsUserSelfOrManager(BasePermission):
    """用户本人或者是拥有应用管理权限"""

    def has_permission(self, request, view):
        return is_user_self(request) or check_admin_auth(request.user)


class IsSuperAdminOrUserCreator(BasePermission):
    """系统默认管理员或用户创建者"""

    def has_permission(self, request, view):
        return is_user_creator(request) or request.user.username == "admin"


class IsSuperAdminOrUserCreatorOrUserSelf(BasePermission):
    """系统默认管理员或用户创建者或用户本人"""

    def has_permission(self, request, view):
        return is_user_creator(request) or request.user.username == "admin" or is_user_self(request)


def check_admin_auth(user):
    """
    根据用户判断是否拥有管理权限
    :param user: 用户实例
    :return: 是否为管理员或者超级用户
    """
    if not user.is_staff:
        all_apps = Application.objects.filter(is_default=False)
        for app_obj in all_apps:
            group_detail = GroupDetail.objects.filter(application=app_obj, group_type="用户组").first()
            app_user_group = group_detail.group
            if user.has_perm("view_group", obj=app_user_group):
                return True
        return False
    return True


def is_user_self(request):
    """
    根据请求，判断用户是否为本人
    :param request: 客户端请求
    :return:
    """
    path = request.path
    login_user = request.user
    user_id_list = re.findall(r"users/(\d+)", path)
    if user_id_list:
        user_id = user_id_list[0]
        return login_user.id == int(user_id)
    return False


def is_user_creator(request):
    """
    根据请求，判断是否为用户创建者
    :param request: 客户端请求
    :return:
    """
    path = request.path
    login_user = request.user
    user_id_list = re.findall(r"users/(\d+)", path)
    if user_id_list:
        user_id = user_id_list[0]
        user = User.objects.get(id=user_id)
        user_creator = login_user.has_perm('delete_user', user)
        return user_creator
    return False
