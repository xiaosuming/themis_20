from django.apps import AppConfig


class ConsoleConfig(AppConfig):
    name = 'apps.console'

    def ready(self):
        from django.db.models.signals import post_save
        from .signal import install_default_app, create_portrait

        post_save.connect(install_default_app, sender='auth.User',
                          dispatch_uid='new_user_install_app')
        post_save.connect(create_portrait, sender='auth.User',
                          dispatch_uid='create_portrait')
