# Generated by Django 2.2 on 2020-05-19 19:57

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('console', '0002_auto_20200427_1537'),
    ]

    operations = [
        migrations.CreateModel(
            name='AttributeKey',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('key', models.CharField(max_length=32, verbose_name='自定义属性的键')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': '自定义属性的键表',
                'verbose_name_plural': '自定义属性的键表',
            },
        ),
        migrations.CreateModel(
            name='AttributeValue',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('value', models.CharField(max_length=32, verbose_name='自定义属性的键')),
                ('key', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='console.AttributeKey')),
            ],
            options={
                'verbose_name': '自定义属性的值表',
                'verbose_name_plural': '自定义属性的值表',
            },
        ),
    ]
