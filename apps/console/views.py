import re
import uuid

import redis
from random import randint
from django.contrib.auth.models import User, Group
from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction, IntegrityError
from django.db.models import Q

from django.utils.decorators import method_decorator
from django_filters import rest_framework as filters
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from guardian.core import ObjectPermissionChecker
from guardian.shortcuts import assign_perm, remove_perm, get_users_with_perms
from rest_framework import status, mixins
from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError, ParseError, NotFound, PermissionDenied
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet, GenericViewSet
from rest_framework_csv.renderers import CSVRenderer
from apps.application.models import Application
from server.log import s_logger
from server.settings import REDIS
from .models import AttributeKey, AttributeValue, AttributeType, UserIcon
from .permissions import IsSuperAdmin, IsSuperAdminOrAppAdmin, check_admin_auth, IsManageUser, IsUserSelf, \
    IsUserCreator, \
    IsUserSelfOrManager, IsSuperAdminOrUserCreator, IsSuperAdminOrUserCreatorOrUserSelf
from .serializers import UserSerializer, CanAccessAppSerializer, GroupSerializer, AttributeTypeSerializer
from ..desktop.models import InstalledAPP

REDIS_CONN = redis.Redis(
    host=REDIS["HOST"],
    port=REDIS["PORT"],
    db=REDIS["DB"],
    password=REDIS.get("PASSWORD"),
    decode_responses=True
)


def update_or_delete_group_users(request, user_ids, group_ids):
    """
    批量加入或移出用户
    :param request: http.request对象
    :param user_ids: 用户id列表
    :param group_ids: 群组id列表
    :return:
    """
    # 1.获取参数
    method = request.method

    # 2.业务处理
    for user_id in user_ids:
        for group_id in group_ids:
            # 2.1.获取用户和群组信息
            user_obj = get_object_or_404(User, pk=user_id)
            user_groups = user_obj.groups
            group_obj = get_object_or_404(Group, pk=group_id)

            # 2.2.处理添加操作
            if method == "PUT":
                if user_groups.filter(id=group_id):
                    raise ValidationError(detail="用户\"{}\"已在群组中\"{}\"".format(
                        user_obj.username, group_obj.name))
                ok = user_groups.add(group_id)
                s_logger.glob.info("添加群组：{}".format(str(ok)))

            # 2.3.处理移出操作
            elif method == "DELETE":
                user_groups.remove(group_id)

    return Response(status=status.HTTP_200_OK)


def get_access_groups(login_user):
    """
    接受一个用户实例,返回用户可操作的群组
    :param login_user: 用户实例
    :return: 用户可操作用户组群组
    """
    user_groups = Group.objects.filter(groupdetail__group_type__in=["管理组", "用户组"])
    can_manage_group_ids = [group.id for group in user_groups
                            if login_user.has_perm("view_group", group)]
    return user_groups.filter(id__in=can_manage_group_ids)


def get_or_update_acc_app(request, user_or_group):
    """
    接受一个http.request对象和一个用户对象或群组对象,
    更新对象拥有访问或管理权限的应用, 返回用户或群组拥有访问或管理权限的应用
    :param request: http.request对象
    :param user_or_group: 用户或群组对象
    :return: 用户群组有访问权限的对象
    """
    # 1.获取参数
    method = request.method
    login_user = request.user
    op_obj = user_or_group
    perm_checker = ObjectPermissionChecker(op_obj)

    # 1.2.筛选出当前用户或群组有权操作的应用
    app_set = Application.objects.exclude(is_default=True).order_by("id")

    # FIXME 弃用
    if method == 'PATCH':
        # 获取参数
        to_add_apps = request.data.get('to_add_apps') or []
        filter(lambda x: type(x) is int, to_add_apps)
        to_rm_apps = request.data.get('to_rm_apps') or []
        filter(lambda x: type(x) is int, to_rm_apps)
        to_add_admins = request.data.get('to_add_admins') or []
        filter(lambda x: type(x) is int, to_add_admins)
        to_rm_admins = request.data.get('to_rm_admins') or []
        filter(lambda x: type(x) is int, to_rm_admins)

        # 校验权限
        if (to_add_apps or to_rm_apps) and not check_admin_auth(login_user):
            return Response(status=status.HTTP_403_FORBIDDEN)
        if (to_add_admins or to_rm_admins) and not login_user.is_staff:
            return Response(status=status.HTTP_403_FORBIDDEN)

        # 2.筛选出应用
        add_apps = Application.objects.filter(id__in=to_add_apps)
        rm_apps = Application.objects.filter(id__in=to_rm_apps)
        add_admins = Application.objects.filter(id__in=to_add_admins)
        rm_admins = Application.objects.filter(id__in=to_rm_admins)

        # 3.修改权限
        for app in add_apps:
            assign_perm("application.view_application", op_obj, obj=app)
        for app in rm_apps:
            remove_perm("application.view_application", op_obj, obj=app)
        for app in add_admins:
            assign_perm("application.view_application", op_obj, obj=app)
            assign_perm("application.change_application", op_obj, obj=app)
        for app in rm_admins:
            remove_perm("application.change_application", op_obj, obj=app)

    # 4.添加是否可以访问属性
    app_list = list()
    user_groups = op_obj.groups.all()
    for app in app_set:
        app_info = {'id': app.id, 'name': app.name, 'logo': app.get_logo()}
        groupdetails = app.groupdetail_set.filter(group_type__in=["管理组", "用户组"])
        for groupdetail in groupdetails:
            if groupdetail.group in user_groups:
                if groupdetail.group_type == "用户组":
                    app_info['can_access'] = True
                elif groupdetail.group_type == "管理组":
                    app_info['can_manage'] = True
        app_list.append(app_info)

    # 5.返回结果
    serializer = CanAccessAppSerializer(app_list, many=True)
    response = Response(data=serializer.data, status=status.HTTP_200_OK)
    return response


def generate_init_password(username=None):
    """
    根据用户ID生成初始密码
    :param username: 用户名
    :return: 生成的密码
    """
    if username:
        init_password = REDIS_CONN.get("user:{}".format(username))
    else:
        init_password = ''.join(["%s" % randint(0, 9) for num in range(0, 8)])
    return init_password


def get_results_in_names(queryset, field_name, value):
    """
    根据查询条件，过滤用户或群组
    :param queryset: 用户或群组的queryset
    :param field_name: 搜索字段名称
    :param value: 搜索字段的值
    :return: 请求的应用
    """
    # 1.校验参数
    try:
        filter_names = eval(value)
    except (NameError, SyntaxError):
        return queryset
    if len(filter_names) == 0 or not isinstance(filter_names, list):
        return queryset

    # 2.根据请求字段过滤queryset
    filter_condition = {"{}__icontains".format(field_name): filter_names[0]}
    filter_queryset = queryset.filter(**filter_condition)
    for filter_name in filter_names[1:]:
        filter_condition = {"{}__icontains".format(field_name): filter_name}
        filter_queryset = filter_queryset | queryset.filter(**filter_condition)

    # 3.返回queryset
    return filter_queryset


class UserFilter(filters.FilterSet):
    """用户过滤器"""
    names = filters.CharFilter(method="get_users_in_names")
    username = filters.CharFilter(field_name="username", lookup_expr="icontains")

    class Meta:
        model = User
        fields = ("names", "username")

    def get_users_in_names(self, queryset, name, value):
        return get_results_in_names(queryset, "first_name", value)


class GroupFilter(filters.FilterSet):
    """群组过滤器"""
    name = filters.CharFilter(field_name="name", lookup_expr="icontains")
    names = filters.CharFilter(method="get_groups_in_names")

    class Meta:
        model = Group
        fields = ("name", "names")

    def get_groups_in_names(self, queryset, name, value):
        return get_results_in_names(queryset, "name", value)


# @class UserViewSet 用户视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	桌面视图
#   method:GET, POST, PUT, PATCH, DELETE
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2020/05/25 10:17:22|杨伟光|创建类|完成|
# -----------------------------------------------------------------------------------------------------
@method_decorator(name='list', decorator=swagger_auto_schema(
    tags=['控制台-用户信息'], operation_summary='返回系统内用户',
    manual_parameters=[
        openapi.Parameter('limit', openapi.IN_QUERY, description='每页返回的数量',
                          type=openapi.TYPE_INTEGER),
        openapi.Parameter('offset', openapi.IN_QUERY, description='从第几条返回',
                          type=openapi.TYPE_INTEGER),
        openapi.Parameter('names', openapi.IN_QUERY, description='用户姓名列表',
                          type=openapi.TYPE_STRING),
        openapi.Parameter('username', openapi.IN_QUERY, description='用户名',
                          type=openapi.TYPE_STRING), ]
))
@method_decorator(name='create', decorator=swagger_auto_schema(
    tags=['控制台-用户信息'], operation_summary='注册用户'
))
@method_decorator(name='retrieve', decorator=swagger_auto_schema(
    tags=['控制台-用户信息'], operation_summary='返回单个用户信息'
))
@method_decorator(name='update', decorator=swagger_auto_schema(
    tags=['控制台-用户信息'], operation_summary='更新用户信息'
))
@method_decorator(name='partial_update', decorator=swagger_auto_schema(
    tags=['控制台-用户信息'], operation_summary='更新用户指定的属性'
))
@method_decorator(name='destroy', decorator=swagger_auto_schema(
    tags=['控制台-用户信息'], operation_summary='删除单个用户'
))
class UserViewSet(ModelViewSet):
    queryset = User.objects.exclude(username='AnonymousUser')
    serializer_class = UserSerializer
    search_fields = ("names", "username")
    filterset_class = UserFilter
    ordering_fields = ("username", "first_name")

    def get_permissions(self):
        """权限校验"""
        # 1.获取参数
        user = self.request.user
        method = self.request.method
        path = self.request.path
        permission_classes = []

        # 2.根据用户身份，请求方法，请求路径，校验权限
        # 2.1.1. 获取用户列表或创建用户
        if re.findall(r"users$", path):
            permission_classes = [IsManageUser]
        # 2.1.2. 操作单个用户
        elif re.findall(r"users/(\d+)$", path):
            if method == "GET":
                permission_classes = [IsUserSelfOrManager]
            elif method in ["PUT", "PATCH"]:
                permission_classes = [IsSuperAdminOrUserCreator]
            elif method == "DELETE":
                permission_classes = [IsUserCreator]

        # 2.2. 操作属性
        elif re.findall(r"attributes", path):
            permission_classes = [IsUserSelf]

        # 2.3. 查看应用和群组
        elif re.findall(r"access_apps", path) or re.findall(r"groups", path):
            permission_classes = [IsSuperAdminOrAppAdmin]

        # 2.4. 重置密码
        elif re.findall(r"password", path):
            if method == "PUT":
                permission_classes = [IsSuperAdminOrUserCreatorOrUserSelf]

        # 2.5. 设置状态
        elif re.findall(r"status", path):
            permission_classes = [IsSuperAdminOrUserCreator]

        return [permission() for permission in permission_classes]

    def get_queryset(self):
        login_user = self.request.user

        # 除admin账户外直接拉取用户列表时
        if re.findall(r"users$", self.request.path):
            user_key = "themis_reg_user"
            user_ids = REDIS_CONN.smembers(user_key)
            self.queryset = self.queryset.exclude(Q(is_staff=True) | Q(id__in=user_ids))
            if login_user.username != "admin" and not self.request.query_params.get("names"):
                groups = get_access_groups(login_user)
                self.queryset = self.queryset.filter(groups__in=groups).distinct()

        return self.queryset.order_by("first_name")

    @swagger_auto_schema(method='put', operation_summary='修改用户自定义属性',
                         tags=['控制台-用户信息'], request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT, description="列表为应用id",
            properties={'attributes': openapi.Schema(
                type=openapi.TYPE_OBJECT, description="键为自定义字段，值为对应的值",
                properties={'字段名': openapi.Schema(type=openapi.TYPE_STRING)})}))
    @action(methods=['PUT'], detail=True, serializer_class=UserSerializer,
            description="修改用户自定义属性")
    def attributes(self, request, pk):
        """设置用户自定义属性"""
        # 1.获取参数，并校验参数和身份
        user = self.get_object()
        attrubite = request.data
        if not attrubite:
            raise ValidationError(detail="参数缺少")
        attrubite = eval(attrubite) if isinstance(attrubite, str) else attrubite

        # 2.保存属性信息
        attr_type = AttributeType.objects.get(id=attrubite["id"])
        for element in attrubite["elements"]:
            key = get_object_or_404(AttributeKey, id=element["id"], attr_type=attr_type)
            attr_value, _ = AttributeValue.objects.get_or_create(key=key, user=user)
            attr_value.value = element["value"]
            attr_value.save()

        # 3.响应数据
        serializer = UserSerializer(user)
        return Response(serializer.data)

    @swagger_auto_schema(method='get', operation_summary='获取登录用户信息', tags=['控制台-用户信息'])
    @action(methods=['GET'], detail=False, description="获取登录用户信息")
    def self_info(self, request):
        """获取登录用户信息"""
        # 1.获取用户信息
        user = request.user

        # 2.处理响应
        serializer = UserSerializer(user)

        # 3.返回结果
        return Response(serializer.data)

    @swagger_auto_schema(method='get', operation_summary='获取用户权限', tags=['控制台-用户设置'])
    @action(methods=['GET'], detail=True, serializer_class=CanAccessAppSerializer, description="获取用户权限信息")
    def access_apps(self, request, pk):
        """获取用户应用权限列表"""
        # 1.获取用户信息
        user = self.get_object()

        # 2.处理响应
        response = get_or_update_acc_app(request, user)

        # 3.返回结果
        return response

    @swagger_auto_schema(method='put', operation_summary='重置用户密码',
                         tags=['控制台-用户设置'], request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT, description="列表为应用id",
            properties={'password': openapi.Schema(
                type=openapi.TYPE_STRING, description="密码")}), )
    @swagger_auto_schema(method='get', operation_summary='重置用户密码',
                         tags=['控制台-用户设置'], responses={
            '200': openapi.Response(description="密码", schema=openapi.Schema(
                type=openapi.TYPE_OBJECT, description="密码",
                properties={'password': openapi.Schema(type=openapi.TYPE_STRING,
                                                       description='密码'), }), ), })
    @action(methods=['PUT', 'GET'], detail=True)
    def password(self, request, pk):
        """重置密码"""
        # 1.get请求，直接返回密码
        if request.method == "GET":
            init_password = generate_init_password()
            return Response(data={"password": init_password}, status=status.HTTP_200_OK)

        # 2.put请求，修改密码
        # 2.1.获取用户信息
        user = self.get_object()
        password = request.data.get("password")
        # 2.2.修改密码
        user.set_password(password)
        user.save()

        return Response(status=status.HTTP_200_OK)

    @swagger_auto_schema(method='delete', operation_summary='暂停用户', tags=['控制台-用户设置'])
    @swagger_auto_schema(method='get', operation_summary='激活用户', tags=['控制台-用户设置'])
    @action(methods=['GET', 'DELETE'], detail=True)
    def status(self, request, pk):
        """设置用户状态"""
        # 1.获取用户
        user = self.get_object()

        # 2.修改用户状态
        if request.method == "GET":
            user.is_active = True
        elif request.method == "DELETE":
            user.is_active = False

        # 3.保存状态，返回结果
        user.save()
        return Response(status=status.HTTP_200_OK)

    @swagger_auto_schema(method='delete', operation_summary='批量删除用户',
                         tags=['控制台-用户设置'], request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT, description="用户id列表",
            properties={'user_ids': openapi.Schema(type=openapi.TYPE_ARRAY, items=openapi.Schema(
                type=openapi.TYPE_INTEGER, description="")), }))
    @action(methods=['delete'], detail=False)
    def deletions(self, request, *args, **kwargs):
        """批量删除用户"""
        # 1.获取参数,并校验参数
        login_user = request.user
        user_ids = request.data.get("user_ids")
        if not user_ids:
            raise ValidationError(detail="参数缺失")
        user_ids = eval(user_ids) if user_ids and isinstance(user_ids, str) else user_ids

        # 2.遍历用户列表,移除用户
        for user_id in user_ids:
            user = get_object_or_404(User, pk=user_id)
            if login_user.has_perm('delete_user', user):
                user.delete()
            else:
                raise PermissionDenied(detail="权限不足，无法删除用户\"{}\"".format(user.username))

        # 3.返回数据
        return Response(status=status.HTTP_204_NO_CONTENT)

    @swagger_auto_schema(method='get', operation_summary='用户的群组信息', tags=['控制台-用户信息'],
                         serializer_class=GroupSerializer, manual_parameters=[
            openapi.Parameter('limit', openapi.IN_QUERY, description='每页返回的数量',
                              type=openapi.TYPE_INTEGER),
            openapi.Parameter('offset', openapi.IN_QUERY, description='从第几条返回',
                              type=openapi.TYPE_INTEGER), ])
    @action(methods=['get'], detail=True)
    def groups(self, request, *args, **kwargs):
        """用户群组信息"""
        # 1.获取参数
        user_obj = self.get_object()
        login_user = request.user

        # 2.权限校验
        if user_obj.is_staff:
            raise PermissionDenied(detail="权限不足")

        # 3.获取群组，并分页
        groups = user_obj.groups.filter(groupdetail__group_type__in=["管理组", "用户组"])
        if login_user.username != "admin":
            groups = groups.filter(groupdetail__app_admin=login_user)
        page = self.paginate_queryset(groups)
        serializer = GroupSerializer(page, many=True)

        # 4.返回响应
        return self.get_paginated_response(serializer.data)

    @action(methods=["PUT", ], detail=True)
    def portrait(self, request, pk):
        """上传用户头像"""
        # 1.获取并检验参数
        user = self.get_object()
        filename = list(request.data.keys())[0]
        pic_ext = filename.rsplit('.')[1]
        if pic_ext.lower() not in ["jpg", "gif", "jpeg", "png"]:
            raise ValidationError(detail="文件类型不匹配")

        # 2.获取文件名，保存数据
        user_icon, _ = UserIcon.objects.get_or_create(user=user)
        user_icon.icon = request.data.get(filename)
        user_icon.save()

        # 3.返回响应
        return Response(data={"head_icon": user_icon.return_icon()},
                        status=status.HTTP_200_OK)


# @class AttributeView 用户自定义属性视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	桌面视图
#   method:GET, POST, PUT, DELETE
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2020/06/08 15:17:22|杨伟光|创建类|完成|
# -----------------------------------------------------------------------------------------------------
@method_decorator(name='list', decorator=swagger_auto_schema(
    tags=['控制台-用户属性模板'], operation_summary='获取自定义属性'
))
@method_decorator(name='create', decorator=swagger_auto_schema(
    tags=['控制台-用户属性模板'], operation_summary='增加新的自定义属性'
))
@method_decorator(name='retrieve', decorator=swagger_auto_schema(
    tags=['控制台-用户属性模板'], operation_summary='返回单个自定义属性'
))
@method_decorator(name='update', decorator=swagger_auto_schema(
    tags=['控制台-用户属性模板'], operation_summary='更新属性'
))
@method_decorator(name='partial_update', decorator=swagger_auto_schema(
    tags=['控制台-用户属性模板'], operation_summary='更新指定的属性'
))
@method_decorator(name='destroy', decorator=swagger_auto_schema(
    tags=['控制台-用户属性模板'], operation_summary='删除属性'
))
class AttributeViewSet(ModelViewSet):
    serializer_class = AttributeTypeSerializer
    queryset = AttributeType.objects.all()
    permission_classes = [IsSuperAdmin]

    @swagger_auto_schema(method='delete', operation_summary='删除自定义字段', tags=['控制台-用户属性模板'],
                         request_body=openapi.Schema(
                             type=openapi.TYPE_OBJECT, description="用户id列表",
                             properties={'elements': openapi.Schema(type=openapi.TYPE_ARRAY, items=openapi.Schema(
                                 type=openapi.TYPE_OBJECT, properties={
                                     "id": openapi.Schema(type=openapi.TYPE_INTEGER, description="字段id")
                                 }, description="字段列表，键值对")), }))
    @action(methods=['delete'], detail=True)
    def fields(self, request, pk):
        """删除自定义字段"""
        # 1.获取参数
        attr_type = self.get_object()
        attr_keys = request.data.get("elements")

        # 2.处理请求
        for element in attr_keys:
            attr_key = AttributeKey.objects.filter(id=element["id"], attr_type=attr_type)
            attr_key.delete()

        # 3.返回响应
        return Response(status=status.HTTP_204_NO_CONTENT)


# @class GroupViewSet 群组视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	桌面视图
#   method:GET
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2020/06/11 09:53:22|杨伟光|创建类|完成|
# -----------------------------------------------------------------------------------------------------
@method_decorator(name='list', decorator=swagger_auto_schema(
    tags=['控制台-群组信息'], operation_summary='获取群组',
    manual_parameters=[
        openapi.Parameter('limit', openapi.IN_QUERY, description='每页返回的数量',
                          type=openapi.TYPE_INTEGER),
        openapi.Parameter('offset', openapi.IN_QUERY, description='从第几条返回',
                          type=openapi.TYPE_INTEGER),
        openapi.Parameter('names', openapi.IN_QUERY, description='群组名列表',
                          type=openapi.TYPE_STRING),
    ],
))
@method_decorator(name='retrieve', decorator=swagger_auto_schema(
    tags=['控制台-群组信息'], operation_summary='返回单个群组信息'
))
class GroupViewSet(mixins.ListModelMixin,
                   mixins.RetrieveModelMixin,
                   GenericViewSet):
    serializer_class = GroupSerializer
    queryset = Group.objects.all()
    permission_classes = [IsManageUser]
    filterset_class = GroupFilter
    search_fields = ("names",)

    def get_queryset(self):
        login_user = self.request.user
        groups = get_access_groups(login_user)
        return groups

    @swagger_auto_schema(method='get', operation_summary='获取单个群组内用户', tags=['控制台-群组信息'],
                         manual_parameters=[
                             openapi.Parameter('limit', openapi.IN_QUERY, description='每页返回的数量',
                                               type=openapi.TYPE_INTEGER),
                             openapi.Parameter('offset', openapi.IN_QUERY, description='从第几条返回',
                                               type=openapi.TYPE_INTEGER),
                             openapi.Parameter('names', openapi.IN_QUERY, description='检索用户名列表',
                                               type=openapi.TYPE_INTEGER), ])
    @swagger_auto_schema(method='put', operation_summary='id传0,批量添加用户到群组', tags=['控制台-群组信息'],
                         request_body=openapi.Schema(
                             type=openapi.TYPE_OBJECT, description="要操作的用户和群组",
                             properties={'user_ids': openapi.Schema(type=openapi.TYPE_ARRAY, items=openapi.Schema(
                                 type=openapi.TYPE_INTEGER, description="")),
                                         'group_ids': openapi.Schema(type=openapi.TYPE_ARRAY, items=openapi.Schema(
                                             type=openapi.TYPE_INTEGER, description="")), }))
    @swagger_auto_schema(method='delete', operation_summary='id传0,批量删除群组中的用户', tags=['控制台-群组信息'],
                         request_body=openapi.Schema(
                             type=openapi.TYPE_OBJECT, description="要操作的用户和群组",
                             properties={'user_ids': openapi.Schema(type=openapi.TYPE_ARRAY, items=openapi.Schema(
                                 type=openapi.TYPE_INTEGER, description="")),
                                         'group_ids': openapi.Schema(type=openapi.TYPE_ARRAY, items=openapi.Schema(
                                             type=openapi.TYPE_INTEGER, description="")), }))
    @action(methods=['get', 'put', 'delete'], detail=True, filterset_class=UserFilter)
    def users(self, request, pk):
        """获取设置群组中的用户"""
        if request.method == "GET":
            # 1.获取群组
            group_obj = get_object_or_404(Group, pk=pk)

            # 2.获取用户，并分页
            group_users = group_obj.user_set.all()
            users = self.filter_queryset(group_users)
            page = self.paginate_queryset(users)
            serializer = UserSerializer(page, many=True)

            # 3.返回响应
            return self.get_paginated_response(serializer.data)

        else:
            # 1.获取参数
            group_ids = request.data.get("group_ids")
            user_ids = request.data.get("user_ids")

            # 2.校验参数，校验群组
            if pk != "0":
                raise ValidationError(detail="参数错误")
            for group_id in group_ids:
                get_object_or_404(self.queryset, pk=group_id)

            # 2.处理请求
            response = update_or_delete_group_users(request, user_ids, group_ids)

            # 3.返回响应
            return response


# @class UserUploadAPI 用户上传视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	桌面视图
#   method:POST
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2020/06/24 15:33:22|杨伟光|创建类|完成|
# -----------------------------------------------------------------------------------------------------
class UserUploadAPI(APIView):
    permission_classes = [IsSuperAdminOrAppAdmin, ]

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    @swagger_auto_schema(tags=["控制台-用户设置"], operation_summary='上传用户信息',
                         request_body=openapi.Schema(
                             type=openapi.TYPE_OBJECT, description="csv文件",
                             properties={'file': openapi.Schema(type=openapi.TYPE_ARRAY, items=openapi.Schema(
                                 type=openapi.TYPE_FILE, description="")), }))
    def post(self, request):
        # 1.获取参数
        user = request.user
        user_info = request.data.get("file")

        # 2.校验参数
        if not user_info:
            raise ParseError(detail='上传内容为空')
        if not user.is_staff:
            raise PermissionDenied(detail="您没有上传用户的权限")

        # 3.处理数据
        file = user_info.file.read().decode()
        try:
            self.save_user(file)
        except Exception:
            raise ValidationError(detail="上传的数据有误")

        # 4.返回响应
        return Response(status=status.HTTP_201_CREATED)

    @classmethod
    def save_user(cls, file):
        """
        根据csv文件,保存用户信息
        :param file: 需要导入用户信息的csv
        :return
        """
        # 读取文件
        data_list = file.split("\n")[:-1]
        headers = data_list[0].strip().split(",")

        # 处理数据
        for line in data_list[1:]:
            info_list = line.strip().split(",")
            info_dict = dict(zip(headers, info_list))

            # 存入数据库
            with transaction.atomic():
                try:
                    user_obj = User.objects.create_user(
                        username=info_dict["username"],
                        first_name=info_dict["name"],
                        password=info_dict["password"]
                    )
                except IntegrityError:
                    raise ValidationError(detail="登录名\"{}\"已存在".format(info_dict["username"]))

                del info_dict["username"]
                del info_dict["name"]
                del info_dict["password"]

                for key, value in info_dict.items():
                    key = AttributeKey.objects.get(key=key)
                    AttributeValue.objects.get_or_create(key=key, user=user_obj, value=value)


# @class UserDownloadAPI 用户下载视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	桌面视图
#   method:GET
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2020/06/24 15:40:22|杨伟光|创建类|完成|
# -----------------------------------------------------------------------------------------------------
class UserDownloadAPI(APIView):
    renderer_classes = [CSVRenderer, ]

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.redis_conn = redis.Redis(
            host=REDIS["HOST"],
            port=REDIS["PORT"],
            db=REDIS["DB"],
            password=REDIS["PASSWORD"],
            decode_responses=True
        )

    @swagger_auto_schema(tags=["控制台-用户设置"], operation_summary='下载用户信息',
                         manual_parameters=[
                             openapi.Parameter('user_ids', openapi.IN_QUERY, description='用户id列表',
                                               type=openapi.TYPE_ARRAY,
                                               items=openapi.Schema(type=openapi.TYPE_INTEGER)),
                             openapi.Parameter('template', openapi.IN_QUERY, description='是否为模板，1为下载模板',
                                               type=openapi.TYPE_INTEGER, enum=[0, 1]),
                             openapi.Parameter('credential', openapi.IN_QUERY, description='用户下载凭证',
                                               type=openapi.TYPE_STRING),
                         ], )
    def get(self, request):
        # 1.获取用户信息
        credential = request.query_params.get("credential")
        user_ids = request.query_params.get("user_ids")
        is_template = request.query_params.get("template")

        # 2.组织响应数据
        # 2.1.获取用户集
        user_id = self.redis_conn.get("user_download:{}".format(credential))
        login_user = get_object_or_404(User, pk=user_id)
        all_users = User.objects.exclude(Q(username='AnonymousUser') | Q(is_staff=True))
        if login_user.username == "admin":
            users = all_users
        else:
            groups = get_access_groups(login_user)
            users = all_users.filter(groups__in=groups).distinct()

        if user_ids:
            user_ids = eval(user_ids)
            users = users.filter(id__in=user_ids)
        elif is_template:
            users = User.objects.filter(username='AnonymousUser')

        # 2.2.响应成csv对应格式
        data = self.csv_paint(users)

        # 3.返回数据
        return Response(data, headers={'Content-Disposition': 'attachment; filename=user.csv'})

    @classmethod
    def csv_paint(cls, users):
        """
        解析用户QuerySet数据
        :param users: 用户信息QuerySet
        :return: 用户信息列表
        """
        serializer = UserSerializer(users, many=True)
        data = []
        for user_info in serializer.data:
            user_detail = dict()
            user_detail["username"] = user_info["username"]
            user_detail["name"] = user_info["first_name"]
            user_detail["password"] = None
            attr_keys = AttributeKey.objects.all()
            for attr_key in attr_keys:
                attr_value = AttributeValue.objects.filter(key_id=attr_key.id, user=user_info["id"]).first()
                user_detail[attr_key.key] = attr_value.value if attr_value else ""
            data.append(user_detail)

        return data


# @class CredentialAPI 用户凭证视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	桌面视图
#   method:GET
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2020/06/22 13:33:22|杨伟光|创建类|完成|
# -----------------------------------------------------------------------------------------------------
class CredentialAPI(APIView):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.redis_conn = redis.Redis(
            host=REDIS["HOST"],
            port=REDIS["PORT"],
            db=REDIS["DB"],
            password=REDIS["PASSWORD"],
            decode_responses=True
        )

    @swagger_auto_schema(tags=["控制台-用户设置"], operation_summary='下载用户凭证')
    def get(self, request):
        # 1.获取用户身份
        login_user = request.user

        # 2.生成凭证并存入缓存
        user_credential = str(uuid.uuid4())
        self.redis_conn.setex("user_download:{}".format(user_credential), 60, login_user.id)

        # 3.返回响应
        return Response(data={"credential": user_credential})
