from django.contrib.auth.models import User, Group
from django.core.exceptions import ObjectDoesNotExist
from django.db import IntegrityError
from django.db.transaction import atomic
from guardian.shortcuts import assign_perm
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.generics import get_object_or_404

from .models import AttributeKey, AttributeValue, AttributeType, UserIcon
from .permissions import check_admin_auth
from ..application.models import Application


class GroupBaseSerializer(serializers.ModelSerializer):
    """基础群组类"""

    class Meta:
        model = Group
        fields = ('id', 'name')


class UserBaseSerializer(serializers.ModelSerializer):
    """用户基础序列"""

    class Meta:
        model = User
        fields = ('id', 'username', 'first_name', 'email')
        read_only_field = ('id', 'username', 'first_name', 'email')


class AttributeValueSerializer(serializers.ModelSerializer):
    """属性值序列"""

    class Meta:
        model = AttributeValue
        fields = "__all__"


class AttributeKeySerializer(serializers.ModelSerializer):
    """用户信息序列"""
    # value = AttributeValueSerializer(source="attr_value", many=True)
    value = serializers.CharField(max_length=32, allow_null=True, read_only=True, help_text="字段值")

    class Meta:
        model = AttributeKey
        fields = ('id', 'key', 'value', 'type', 'number')


class AttributeTypeSerializer(serializers.ModelSerializer):
    """类型序列"""
    elements = AttributeKeySerializer(source="attr_key", many=True, help_text="字段列表", read_only=True)

    class Meta:
        model = AttributeType
        fields = ["id", "elements", "name", "description"]

    def create(self, validated_data):
        """创建属性并添加字段"""
        request = self.context.get('request')
        attr_keys = request.data.get("elements")
        with atomic():
            attr_type = super().create(validated_data)
            for attr_key in attr_keys:
                attr_key["attr_type"] = attr_type
                try:
                    attr_key.pop("value")
                    AttributeKey.objects.create(**attr_key)
                except IntegrityError:
                    raise ValidationError(detail="字段\"{}\"已存在".format(attr_key["key"]))
        return attr_type

    def update(self, instance, validated_data):
        """更新属性和字段"""
        request = self.context.get('request')
        attr_keys = request.data.get("elements")
        with atomic():
            attr_type = super().update(instance, validated_data)
            for element in attr_keys:
                if element.get("id"):
                    attr_key = AttributeKey.objects.get(id=element["id"], attr_type=attr_type)
                    attr_key.key = element["key"] if element.get("key") else attr_key.key
                else:
                    try:
                        attr_key = AttributeKey.objects.create(key=element["key"], attr_type=attr_type)
                    except IntegrityError:
                        raise ValidationError(detail="字段\"{}\"已存在".format(element["key"]))
                attr_key.type = element["type"] if element.get("type") else attr_key.type
                attr_key.number = element["number"] if element.get("number") else attr_key.number
                attr_key.save()
        return attr_type


class UserPortrait(serializers.ModelSerializer):
    """用户头像"""

    class Meta:
        model = UserIcon
        fields = ('icon', 'pure_color',)
        read_only_fields = ('pure_color',)


class UserSerializer(serializers.ModelSerializer):
    """用户序列"""
    password = serializers.CharField(max_length=128, write_only=True, default="", help_text='密码')
    identity = serializers.SerializerMethodField(help_text="用户身份system@admin;app@admin;manage@user;common@user")
    attributes = serializers.SerializerMethodField(help_text="自定义属性")
    portrait = UserPortrait(source='user_icon', allow_null=True, read_only=True)
    total_volume = serializers.SerializerMethodField(help_text="总存储空间")
    used_volume = serializers.SerializerMethodField(help_text="数据中心用量")

    # head_icon = serializers.SerializerMethodField(help_text="用户头像")

    def get_attributes(self, instance):
        attr_types = AttributeType.objects.all()
        serializer = AttributeTypeSerializer(attr_types, many=True)
        attributes = serializer.data
        for attribute in attributes:
            for element in attribute["elements"]:
                attr_obj = AttributeValue.objects.filter(key_id=element["id"], user=instance).first()
                element["value"] = attr_obj.value if attr_obj else None
        return attributes

    def get_identity(self, instance):
        if instance.username == "admin":
            return "system@admin"
        elif instance.is_staff:
            return "app@admin"
        elif check_admin_auth(instance):
            return "manage@user"
        else:
            return "common@user"

    def get_total_volume(self, instance):
        return "无限量"

    def get_used_volume(self, instance):
        return "0GB"

    class Meta:
        model = User
        fields = ('id', 'username', 'password', 'identity', 'is_active', 'first_name', 'date_joined',
                  'last_login', 'attributes', 'portrait', 'total_volume', 'used_volume')
        read_only_fields = ('id', 'date_joined', 'last_login', 'is_active', 'portrait')

    def create(self, validated_data):
        """自定义用户创建方法"""
        request = self.context.get('request')
        # 处理用户头像
        db_user = User.objects.create_user(**validated_data)

        # 赋予创建者权限，标记用户创建者
        assign_perm("delete_user", request.user, db_user)
        return db_user

    def update(self, instance, validated_data):
        """更新用户姓名"""
        instance.first_name = validated_data.get("first_name", instance.first_name)
        instance.save()
        return instance


class GroupSerializer(serializers.ModelSerializer):
    """群组序列"""
    name = serializers.CharField(source="groupdetail.application.name", help_text="群组名称")
    app_id = serializers.CharField(source="groupdetail.application.id", help_text="应用id")
    type = serializers.CharField(source="groupdetail.group_type", help_text="群组类型")
    group_id = serializers.IntegerField(help_text="群组id", read_only=True, source="id")
    count = serializers.SerializerMethodField(help_text="用户数量")

    def get_count(self, instance):
        users = instance.user_set.all()
        return len(users)

    class Meta:
        model = Group
        fields = ('name', 'type', 'app_id', 'group_id', 'count')


class CanAccessAppSerializer(serializers.Serializer):
    """可访问应用应用序列"""
    id = serializers.IntegerField(help_text='应用id')
    name = serializers.CharField(max_length=32, help_text='应用名称')
    logo = serializers.CharField(max_length=256, help_text='应用logo')
    can_access = serializers.BooleanField(default=False, help_text='用户是否拥有访问访问')
    can_manage = serializers.BooleanField(default=False, help_text='用户是否拥有管理权限')
