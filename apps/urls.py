from django.urls import path, include
from rest_framework import routers
from apps.application import views as app_views
from apps.contact.views import FriendViewSet, NoticeViewSet
from apps.desktop.views import DesktopAppViewSet, FolderViewSet, DesktopObjView, BackGroundViewSet, ThemeView
from apps.file.views import SendFileView
from apps.console.views import UserViewSet, AttributeViewSet, GroupViewSet, CredentialAPI, UserUploadAPI, \
    UserDownloadAPI

# 应用商店路由
app_routers = routers.DefaultRouter(trailing_slash=False)
app_routers.register('systems', app_views.SystemViewSet, )
app_routers.register('applications', app_views.ApplicationViewSet)

# 桌面应用与文件夹路由
desktop_routers = routers.DefaultRouter(trailing_slash=False)
desktop_routers.register('apps', DesktopAppViewSet, basename='desktop_app')
desktop_routers.register('folders', FolderViewSet, basename='folder')
desktop_routers.register('background', BackGroundViewSet, basename='back')
desktop_urls = [path('', DesktopObjView.as_view()),
                path('theme', ThemeView.as_view())
                ] + desktop_routers.urls

# 控制台路由
console_routers = routers.DefaultRouter(trailing_slash=False)
console_routers.register('users', UserViewSet, basename='user')
console_routers.register('groups', GroupViewSet, basename='group')
console_routers.register('attributes', AttributeViewSet, basename='attribute')
console_urls = [path('credential', CredentialAPI.as_view()),
                path('users/upload', UserUploadAPI.as_view()),
                path('users/download', UserDownloadAPI.as_view()),
                ] + console_routers.urls

contact_routers = routers.DefaultRouter(trailing_slash=False)
contact_routers.register("friends", FriendViewSet, basename="friend")
contact_routers.register("notices", NoticeViewSet, basename="notice")
contact_urls = [] + contact_routers.urls

urlpatterns = [
    path('app_store/', include((app_routers.urls, 'apps.application.apps'), namespace='application')),
    path('desktop/', include((desktop_urls, 'apps.desktop.apps'), namespace='desktop')),
    path('auth/', include('apps.oauth.urls')),
    path('files/', include('apps.file.urls')),
    path('notification/', include('apps.notification.urls')),
    path('console/', include((console_urls, 'apps.console.apps'), namespace='console')),
    path('contact/', include((contact_urls, 'apps.contact.apps'), namespace='contact')),

    path('v1/',include('apps.applicationnotice.urls')),
    path('v1/',include('apps.chat.urls')),

]
