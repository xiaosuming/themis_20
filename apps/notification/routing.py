from django.conf.urls import url
from django.urls import re_path
from . import consumers

websocket_urlpatterns = [
    re_path(r'ws/notification/(?P<uid>\d+)/$', consumers.NotificationConsumer),
    url(r'ws/chat/(?P<user_id>[^/]+)/$', consumers.ChatConsumer),
]
