from channels.generic.websocket import AsyncJsonWebsocketConsumer
from channels.generic.websocket import AsyncWebsocketConsumer
import json


class NotificationConsumer(AsyncJsonWebsocketConsumer):
    async def connect(self):
        uid = self.scope['url_route']['kwargs']['uid']
        group_name = 'themis_notification_{}'.format(uid)

        # 将当前用户加入群组
        await self.channel_layer.group_add(
            group_name,
            self.channel_name
        )

        await self.accept()

    async def disconnect(self, close_code):
        uid = self.scope['url_route']['kwargs']['uid']
        group_name = 'themis_notification_{}'.format(uid)

        await self.channel_layer.group_discard(
            group_name,
            self.channel_name
        )

    async def notification(self, event):
        """消息通知基本函数"""
        await self.send_json(content=event["data"])


class ChatConsumer(AsyncWebsocketConsumer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.user_id = None

    async def connect(self):
        self.user_id = self.scope['url_route']['kwargs']['user_id']
        self.room_group_name = 'user_%s' % self.user_id

        # Join room group
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )
        await self.accept()

    async def disconnect(self, close_code):
        # Leave room group
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    # # Receive message from WebSocket
    # async def receive(self, text_data):
    #     text_data_json = json.loads(text_data)
    #     message = text_data_json['data']
    #
    #     # Send message to room group
    #     await self.channel_layer.group_send(
    #         self.room_group_name,
    #         {
    #             'type': 'chat_message',
    #             'message': message
    #         }
    #     )

    # Receive message from room group
    async def chat_message(self, event):
        message = event['data']

        # Send message to WebSocket
        await self.send(
            text_data=json.dumps({
            'message': message
        }))


