import redis
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.contrib.auth.models import User, Group
from guardian.shortcuts import assign_perm
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from drf_yasg.utils import swagger_auto_schema

from apps.application.models import Application
from apps.console.models import GroupDetail
from apps.console.permissions import IsSuperAdmin
from apps.contact.models import Notice
from apps.contact.serializers import NoticeSerializer
from server.settings import REDIS

# @class NotificationView 通知视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	登录视图
#   method:GET
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2020/07/17 14:02:22|杨伟光|创建类|完成|
# -----------------------------------------------------------------------------------------------------
from utils.send_websocket import send_notice


class NotificationView(APIView):
    permission_classes = [IsSuperAdmin, ]

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.redis_conn = redis.Redis(
            host=REDIS["HOST"],
            port=REDIS["PORT"],
            db=REDIS["DB"],
            password=REDIS.get("PASSWORD"),
            decode_responses=True
        )

    @swagger_auto_schema(tags=["控制台-获取通知-弃用"], operation_summary='获取通知消息')
    def get(self, request):
        # FIXME　修复群组数据库
        app_set = Application.objects.filter(is_default=False)
        super_admin, _ = User.objects.get_or_create(username="admin")
        system_manage_group, _ = Group.objects.get_or_create(name="系统用户管理组")
        GroupDetail.objects.get_or_create(group=system_manage_group, group_type="全局管理组",
                                          description="全局管理组")

        for instance in app_set:
            # 1.用户管理
            app_admin = User.objects.get(username="{}@admin".format(instance.name))

            # 2.群组管理
            # 2.1.创建群组
            admin_group, _ = Group.objects.get_or_create(name="{}@应用管理账户".format(instance.name))
            manage_group, _ = Group.objects.get_or_create(name="{}@管理组".format(instance.name))
            user_group, _ = Group.objects.get_or_create(name="{}@用户组".format(instance.name))
            GroupDetail.objects.get_or_create(group=admin_group, application=instance, app_admin=app_admin,
                                              group_type="应用管理账户", description="应用管理账户")
            GroupDetail.objects.get_or_create(group=manage_group, application=instance, app_admin=app_admin,
                                              group_type="管理组", description="管理组")
            GroupDetail.objects.get_or_create(group=user_group, application=instance, app_admin=app_admin,
                                              group_type="用户组", description="用户组")

            # 2.2.赋予群组相应权限
            # 2.2.1.赋予群组查看应用的权限
            assign_perm("application.view_application", system_manage_group, obj=instance)
            assign_perm("application.view_application", manage_group, obj=instance)
            assign_perm("application.view_application", user_group, obj=instance)

            # 2.2.2.赋予群组查看群组的权限
            assign_perm("view_group", admin_group, obj=manage_group)
            assign_perm("view_group", admin_group, obj=user_group)
            assign_perm("view_group", system_manage_group, obj=user_group)
            assign_perm("view_group", manage_group, obj=user_group)

            # 2.2.3.蒋管理员账户加入群组
            super_admin.groups.add(admin_group.id)
            app_admin.groups.add(admin_group.id)

        # FIXME 修复背景缩略图
        # all_backs = Background.objects.all()
        # for back in all_backs:
        #     if back.pic:
        #         thumb_name = back.save_thumb(back.pic.path)
        #         back.thumbnail = "background/{}".format(thumb_name)
        #         back.save()
        #         print(back.id)

        # 1.获取用户
        login_user = request.user

        # 2.获取信息
        notice_objs = Notice.objects.filter(id=31)
        for notice_obj in notice_objs:
            serializer = NoticeSerializer(notice_obj)
            send_notice(user_id=login_user.id, data=serializer.data)

        # 3.返回响应
        return Response(status=status.HTTP_200_OK)
