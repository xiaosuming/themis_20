from django.contrib.auth.models import User, Group
from rest_framework import viewsets, mixins, status, views, generics
from rest_framework.response import Response
from rest_framework.exceptions import ValidationError, PermissionDenied
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi
from django.utils.decorators import method_decorator
from django.db.transaction import atomic
from django.db.models import Max, Q
from django.core.exceptions import ObjectDoesNotExist
from apps.desktop.serializers import (
    InstalledAppSerializer, FolderSerializer, DesktopSerializer, BackGroundSerializer,
    ThemeSerializer)
from apps.desktop.models import InstalledAPP, Folder, Background, Theme


# @class ApplicationViewSet 桌面视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	桌面视图
#   method:GET, UPDATE
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2020/04/12 14:17:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class DesktopObjView(views.APIView):
    @swagger_auto_schema(
        tags=['桌面-应用与文件夹'], operation_summary='返回用户指定页序号的桌面上的应用和文件夹',
        manual_parameters=[
            openapi.Parameter('page', openapi.IN_QUERY, description='页序号',
                              type=openapi.TYPE_INTEGER), ]
    )
    def get(self, request):
        # 1.获取参数
        user = request.user
        page = request.GET.get('page') or 1

        # 2.获取数据
        # 2.1.获取指定页桌面上的对象
        app_set = InstalledAPP.objects.filter(
            user=user, folder=None, page=int(page)).prefetch_related("application"). \
            order_by('seq')
        # 2.2.获取用具所在群组
        groups = user.groups.filter(name__regex=r'^\S+@users$'). \
            values_list('name', flat=True)

        # 2.3.组织用户桌面应用信息, 根据用户是否有权访问应用展示入口地址
        app_list = [{
            'id': x.id,
            'page': x.page,
            'seq': x.seq,
            'name': x.application.name,
            'english_name': x.application.english_name,
            'entry_url': x.application.entry_url if user.has_perm("application.view_application",
                                                                  x.application) or
                                                    x.application.is_default is True else None,
            'logo': x.application.get_logo(),
            'is_default': x.application.is_default,
            'use_iframe': x.application.use_iframe,
            'allow_resize': x.application.allow_resize,
        } for x in app_set]

        # 2.4.组织用户桌面文件夹信息
        folder_set = Folder.objects.prefetch_related('installedapp_set__application'). \
            filter(user=user, page=int(page)).order_by('seq')
        folder_list = [{
            'id': x.id,
            'page': x.page,
            'seq': x.seq,
            'name': x.name,
            'logo': [app.get_logo() for app in x.installedapp_set.all()],
        } for x in folder_set]

        # 2.5.用户桌面总页数
        installed_page = InstalledAPP.objects.filter(
            user=user, folder=None).values("page").aggregate(max_page=Max('page'))
        folder_page = Folder.objects.filter(
            user=user).values("page").aggregate(max_page=Max('page'))
        max_page = max(installed_page["max_page"] or 1, folder_page["max_page"] or 1)

        # 3.组织应答
        res = {
            'max_page': max_page,
            'apps': app_list,
            'folders': folder_list,
        }
        serializer = DesktopSerializer(data=res)
        if serializer.is_valid():
            return Response(data=serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(data={"detail": "桌面序列化类有误"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @swagger_auto_schema(
        tags=["桌面-应用与文件夹"], operation_summary='更新桌面上对象的位置',
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                'apps': openapi.Schema(
                    type=openapi.TYPE_ARRAY, description='请求参数, 应用位置信息列表',
                    items=openapi.Schema(
                        type=openapi.TYPE_OBJECT, description='应用位置信息',
                        required=['id', 'page', 'seq'],
                        properties={
                            'id': openapi.Schema(
                                type=openapi.TYPE_INTEGER, description='桌面对象id'),
                            'folder_id': openapi.Schema(
                                type=openapi.TYPE_INTEGER, description='目标文件夹id, 移出则为null'),
                            'page': openapi.Schema(
                                type=openapi.TYPE_INTEGER, description='目标页序号'),
                            'seq': openapi.Schema(
                                type=openapi.TYPE_INTEGER, description='目标排序序号'),
                        })),
                'folders': openapi.Schema(
                    type=openapi.TYPE_ARRAY, description='文件夹位置信息列表',
                    items=openapi.Schema(
                        type=openapi.TYPE_OBJECT, description='文件夹位置信息',
                        required=['id', 'page', 'seq'],
                        properties={
                            'id': openapi.Schema(
                                type=openapi.TYPE_INTEGER, description='桌面对象id'),
                            'page': openapi.Schema(
                                type=openapi.TYPE_INTEGER, description='目标页序号'),
                            'seq': openapi.Schema(
                                type=openapi.TYPE_INTEGER, description='目标排序序号'),
                        }))},
        )
    )
    def put(self, request):
        # 1.获取参数
        user = request.user
        put_app = request.data.get('apps', [])
        app_data = {x.pop('id'): x for x in put_app}
        put_folder = request.data.get('folders', [])
        folder_data = {x.pop('id'): x for x in put_folder}

        # 3.更新位置信息
        with atomic():
            for app_id, site_info in app_data.items():
                InstalledAPP.objects.filter(id=app_id, user=user).update(**site_info)
            for folder_id, site_info in folder_data.items():
                Folder.objects.filter(id=folder_id, user=user).update(**site_info)

        # 4.返回响应
        return Response(status=status.HTTP_200_OK)


# @class ApplicationViewSet 桌面应用视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	桌面应用视图
#   method:POST, GET, DELETE
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2020/04/12 14:37:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------
@method_decorator(name='create', decorator=swagger_auto_schema(
    tags=['桌面-应用'], operation_summary='安装应用'
))
@method_decorator(name='destroy', decorator=swagger_auto_schema(
    tags=['桌面-应用'], operation_summary='卸载应用'
))
class DesktopAppViewSet(mixins.CreateModelMixin, mixins.DestroyModelMixin,
                        mixins.ListModelMixin, viewsets.GenericViewSet):
    serializer_class = InstalledAppSerializer
    pagination_class = None

    # queryset = InstalledAPP.objects.all()

    def get_queryset(self):
        user = self.request.user
        return user.installedapp_set.all()

    def create(self, request, *args, **kwargs):
        user = request.user
        data = request.data
        serializer = InstalledAppSerializer(data=data, context={'request': request})
        if serializer.is_valid(raise_exception=True):
            serializer.save(user=user)
        return Response(serializer.data, status=status.HTTP_201_CREATED)


# @class FolderViewSet 桌面文件夹视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	桌面文件夹视图
#   method:POST, GET, UPDATE, DELETE
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2020/04/12 14:17:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------
@method_decorator(name='create', decorator=swagger_auto_schema(
    tags=['桌面-文件夹'], operation_summary='创建文件夹'
))
@method_decorator(name='update', decorator=swagger_auto_schema(
    tags=['桌面-文件夹'], operation_summary='更新文件夹名称'
))
@method_decorator(name='partial_update', decorator=swagger_auto_schema(
    tags=['桌面-文件夹'], operation_summary='更新文件夹内应用位置',
    request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        properties={
            'installed_apps': openapi.Schema(
                type=openapi.TYPE_ARRAY, description='请求参数, 应用位置信息列表',
                items=openapi.Schema(
                    type=openapi.TYPE_OBJECT, description='应用位置信息',
                    required=['id', 'page', 'seq'],
                    properties={
                        'id': openapi.Schema(
                            type=openapi.TYPE_INTEGER, description='应用id'),
                        'page': openapi.Schema(
                            type=openapi.TYPE_INTEGER, description='目标页序号'),
                        'seq': openapi.Schema(
                            type=openapi.TYPE_INTEGER, description='目标排序序号'),
                    })), },
    )
))
@method_decorator(name='destroy', decorator=swagger_auto_schema(
    tags=['桌面-文件夹'], operation_summary='删除文件夹'
))
class FolderViewSet(mixins.CreateModelMixin, mixins.RetrieveModelMixin,
                    mixins.UpdateModelMixin, mixins.DestroyModelMixin,
                    viewsets.GenericViewSet):
    serializer_class = FolderSerializer

    # queryset = Folder.objects.all()

    def get_queryset(self):
        user = self.request.user
        return user.folder_set.all()

    def create(self, request, *args, **kwargs):
        """
        新建文件夹
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        # 1.获取参数
        user = request.user
        data = request.POST
        page = data.get('page')
        seq = data.get('seq')

        # 2.校验参数
        if page and seq:
            conflict = InstalledAPP.objects.filter(
                user=user, page=page, seq=seq, folder=None).count() + Folder.objects. \
                           filter(user=user, page=page, seq=seq).count()
            if conflict:
                raise ValidationError(detail="该位置已有应用或文件夹")
        serializer = FolderSerializer(data=data, context={'request': request})

        # 3.创建文件夹
        if serializer.is_valid(raise_exception=True):
            serializer.save(user=user)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def update(self, request, *args, **kwargs):
        """
        更新文件夹
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        user = request.user
        data = request.data
        db_folder = self.get_object()
        installed_app = data.pop('installed_app')
        for app_info in installed_app:
            app_info.folder = db_folder
            app_serializer = InstalledAppSerializer(data=app_info, context={'request'})
        serializer = FolderSerializer(data=data, context={'request': request})
        if serializer.is_valid(raise_exception=True):
            serializer.save(user=user)

    def partial_update(self, request, *args, **kwargs):
        """
        更新文件夹内应用位置
        """
        login_user = request.user
        db_folder = self.get_object()
        installed_apps = request.data.get("installed_apps")
        for app_info in installed_apps:
            try:
                db_app = InstalledAPP.objects.get(id=app_info.get('id'), user=login_user, folder=db_folder)
            except ObjectDoesNotExist:
                continue
            db_app.page = app_info.get('page') or 1
            db_app.seq = app_info.get('seq') if 'seq' in app_info.keys() else db_app.seq
            db_app.save()

        serializer = FolderSerializer(db_folder, context={'request': request})
        return Response(data=serializer.data, status=status.HTTP_200_OK)


# @class BackGroundViewSet 桌面背景视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	桌面视图
#   method:GET
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2020/05/29 10:13:22|杨伟光|创建类|完成|
# -----------------------------------------------------------------------------------------------------
@method_decorator(name='list', decorator=swagger_auto_schema(
    tags=['桌面-主题设置'], operation_summary='获取全部主题和背景颜色'
))
@method_decorator(name='create', decorator=swagger_auto_schema(
    tags=['桌面-主题设置'], operation_summary='上传背景'
))
@method_decorator(name='destroy', decorator=swagger_auto_schema(
    tags=['桌面-主题设置'], operation_summary='删除背景'
))
class BackGroundViewSet(mixins.ListModelMixin,
                        mixins.CreateModelMixin,
                        mixins.DestroyModelMixin,
                        viewsets.GenericViewSet):
    serializer_class = BackGroundSerializer
    queryset = Background.objects.all()

    def get_queryset(self):
        login_user = self.request.user
        queryset = Background.objects.filter(Q(is_official=True) | Q(owner=login_user))
        return queryset.order_by("-id")

    def perform_create(self, serializer):
        login_user = self.request.user
        serializer.save(owner=login_user, is_official=False)

        # if login_user.username == "admin":
        #     serializer.save()
        # else:
        #     serializer.save(owner=login_user, is_official=False)

    def destroy(self, request, *args, **kwargs):
        """删除背景"""
        # 1.获取参数
        instance = self.get_object()

        # 2.校验权限
        if instance.is_official and request.user.username != "admin":
            raise PermissionDenied(detail="该背景为系统背景，无法删除")

        # 3.删除背景，返回响应
        instance.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    # def create(self, request, *args, **kwargs):
    #     # 1.获取并检验参数
    #     enclosures = request.data.get("enclosures")
    #     enclosures = eval(enclosures) if isinstance(enclosures, str) else enclosures
    #
    #     # 2.蒋文件路径存入数据库
    #     for enclosure in enclosures:
    #         path = enclosure.get("path")
    #         pic_path = path.split('/', 1)[1]
    #         background_name = enclosure.get("name")
    #         Background.objects.create(
    #             pic=pic_path,
    #             name=background_name
    #         )
    #
    #     # 3.返回响应
    #     return Response(status=status.HTTP_201_CREATED)


# @class ThemeView 桌面背景视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	桌面视图
#   method:GET, PATCH
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2020/05/29 11:20:22|杨伟光|创建类|完成|
# -----------------------------------------------------------------------------------------------------
class ThemeView(views.APIView):
    serializer_class = ThemeSerializer

    @swagger_auto_schema(operation_summary='获取个人主题', tags=['桌面-主题设置'],
                         responses={
                             '200': openapi.Response(description="个人主题信息", schema=openapi.Schema(
                                 type=openapi.TYPE_OBJECT, description="个人主题信息",
                                 properties={
                                     'url': openapi.Schema(type=openapi.TYPE_STRING, description='背景图路由'),
                                     'color': openapi.Schema(type=openapi.TYPE_STRING, description='背景颜色'),
                                     'logo_size': openapi.Schema(type=openapi.TYPE_STRING, description='图标大小'),
                                     'font_size': openapi.Schema(type=openapi.TYPE_STRING, description='字体大小'),
                                     'background_id': openapi.Schema(type=openapi.TYPE_STRING, description='背景id')
                                 }), ), })
    def get(self, request):
        # 1.获取用户
        user = request.user

        # 2.获取主题
        try:
            db_theme = Theme.objects.get(user=user)
        except ObjectDoesNotExist:
            # 当前用户没有设置主题，则创建默认主题
            default_background = Background.objects.get(name="default")
            db_theme = Theme.objects.create(user=user, background=default_background)

        # 3.序列化数据
        theme_serializer = ThemeSerializer(db_theme)
        res_data = theme_serializer.data

        # 4.返回响应
        return Response(data=res_data)

    @swagger_auto_schema(operation_summary='设置个人主题', tags=['桌面-主题设置'],
                         request_body=openapi.Schema(
                             type=openapi.TYPE_OBJECT, description="修改时携带对应参数",
                             properties={'logo_size': openapi.Schema(type=openapi.TYPE_STRING, description='图标大小'),
                                         'font_size': openapi.Schema(type=openapi.TYPE_STRING, description='字体大小'),
                                         'dock_position': openapi.Schema(type=openapi.TYPE_STRING,
                                                                         description='应用栏位置',
                                                                         enum=["left", "right"]),
                                         'rank_position': openapi.Schema(type=openapi.TYPE_STRING,
                                                                         description='应用位置',
                                                                         enum=["center", "aside"]),
                                         'background_id': openapi.Schema(type=openapi.TYPE_STRING,
                                                                         description='背景id')}),
                         responses={
                             '200': openapi.Response(description="个人主题信息", schema=openapi.Schema(
                                 type=openapi.TYPE_OBJECT, description="个人主题信息",
                                 properties={
                                     'url': openapi.Schema(type=openapi.TYPE_STRING, description='背景图路由'),
                                     'color': openapi.Schema(type=openapi.TYPE_STRING, description='背景颜色'),
                                     'logo_size': openapi.Schema(type=openapi.TYPE_STRING, description='图标大小'),
                                     'font_size': openapi.Schema(type=openapi.TYPE_STRING, description='字体大小'),
                                     'dock_position': openapi.Schema(type=openapi.TYPE_STRING, description='应用栏位置'),
                                     'rank_position': openapi.Schema(type=openapi.TYPE_STRING, description='应用位置'),
                                     'background_id': openapi.Schema(type=openapi.TYPE_STRING, description='背景id')
                                 }), ), })
    def patch(self, request):
        # 1.获取参数
        user = request.user
        data = request.data
        logo_size = data.get("logo_size")
        font_size = data.get("font_size")
        dock_position = data.get("dock_position")
        rank_position = data.get("rank_position")
        background_id = data.get("background_id")

        # 2.校验参数
        to_update = dict()
        if logo_size:
            if logo_size not in ['1', '2', '3']:
                raise ValidationError(detail='logo_size不合法')
            else:
                to_update["logo_size"] = logo_size

        if font_size:
            if font_size not in [str(i) for i in range(1, 8)]:
                raise ValidationError(detail='font_size大小不合法')
            else:
                to_update["font_size"] = font_size

        if dock_position:
            if dock_position not in ["left", "right"]:
                raise ValidationError(detail="dock_position不合法")
            else:
                to_update["dock_position"] = dock_position

        if rank_position:
            if rank_position not in ["center", "aside"]:
                raise ValidationError(detail="rank_position不合法")
            else:
                to_update["rank_position"] = rank_position

        if background_id:
            try:
                Background.objects.get(id=background_id)
            except ObjectDoesNotExist:
                raise ValidationError(detail='背景图片不存在')
            else:
                to_update["background_id"] = background_id

        # 3.记录用户修改
        db_theme = Theme.objects.filter(user=user)
        if to_update and db_theme:
            db_theme.update(**to_update)

        # 4.返回响应
        theme_serializer = ThemeSerializer(db_theme, many=True)
        res_data = theme_serializer.data[0] if theme_serializer else None
        return Response(data=res_data)
