from PIL import Image, UnidentifiedImageError
from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
from rest_framework.exceptions import ValidationError

from apps.application.models import Application, hash_file
import os

DOCK_POSITION_CHOICES = (
    ("left", "左侧"),
    ("right", "右侧")
)
RANK_POSITION_CHOICES = (
    ("center", "居中"),
    ("aside", "侧边")
)


class DeskBase(models.Model):
    user = models.ForeignKey(to=User, on_delete=models.CASCADE, )
    created_time = models.DateTimeField(auto_now_add=True, verbose_name='创建时间',
                                        help_text='创建时间')
    update_time = models.DateTimeField(auto_now=True, verbose_name='更新时间',
                                       help_text='更新时间')
    page = models.SmallIntegerField(null=True, verbose_name='所在页数', help_text='所在页数')
    seq = models.SmallIntegerField(null=True, verbose_name='排序序号', help_text='排序序号', )

    class Meta:
        abstract = True


class Folder(DeskBase):
    name = models.CharField(max_length=16, null=True, verbose_name='名称', help_text='名称')

    class Meta:
        verbose_name = '文件夹'
        verbose_name_plural = verbose_name

    def __str__(self):
        return '{}-{}'.format(self.user.username, self.name)


class InstalledAPP(DeskBase):
    application = models.ForeignKey(to=Application, on_delete=models.CASCADE,
                                    related_name='authorized_apps', verbose_name='应用',
                                    help_text='应用')
    folder = models.ForeignKey(to=Folder, on_delete=models.SET_NULL, null=True,
                               default=None, verbose_name='文件夹', help_text='文件夹')

    class Meta:
        verbose_name = '安装的应用'
        verbose_name_plural = verbose_name
        unique_together = ('user', 'application')

    def __str__(self):
        return '{}-{}'.format(self.user.username, self.application.name)

    def get_logo(self):
        return self.application.get_logo()

def background_upload(instance, filename):
    """
    背景图片上传函数
    :param instance:
    :param filename:
    :return:
    """
    instance.pic.open()
    pic_ext = filename.rsplit('.')[1]
    pic_path = os.path.join('background', '{}.{}'.format(hash_file(
        instance.pic), pic_ext))
    return pic_path


class Background(models.Model):
    name = models.CharField(max_length=64, verbose_name='背景名称', help_text='背景名称', null=True)
    pic = models.FileField(max_length=128, null=True, upload_to=background_upload,
                           verbose_name='背景图片存储路径', help_text='背景图片存储路径')
    thumbnail = models.FileField(max_length=128, null=True, upload_to=background_upload,
                                 verbose_name='背景图片缩略图', help_text='存储路径')
    color = models.CharField(max_length=8, null=True, verbose_name='背景颜色')
    is_official = models.BooleanField(default=True, verbose_name="是否为官方背景")
    owner = models.ForeignKey(User, null=True, verbose_name="创建者", on_delete=models.CASCADE)

    class Meta:
        verbose_name = '云桌面背景'
        verbose_name_plural = verbose_name

    def return_pic(self, pic):
        """返回文件访问地址"""
        if pic:
            pic_url = os.path.join(settings.HOSTS["FRONTEND"], "api/files",
                                   "?path={}".format(pic.path))
        else:
            pic_url = None
        return pic_url

    def save_thumb(self, pic_path):
        """保存缩略图"""
        pic_folder, pic_name = pic_path.rsplit("/", 1)
        thumb_name = "thumbnail_{}".format(pic_name)
        try:
            pic = Image.open(pic_path)
        except UnidentifiedImageError:
            raise ValidationError(detail={"detail": "不支持的媒体类型"})
        thumbnail = pic.copy()
        thumbnail.thumbnail((360, 200), Image.ANTIALIAS)
        thumbnail.save("{}/{}".format(pic_folder, thumb_name))
        return thumb_name

    def __str__(self):
        return self.name


class Theme(models.Model):
    user = models.OneToOneField(to=User, on_delete=models.CASCADE,
                                verbose_name='用户', help_text='用户', null=True)
    background = models.ForeignKey(to=Background, on_delete=models.CASCADE,
                                   null=True, verbose_name='用户选择的背景')
    logo_size = models.CharField(max_length=1, default='2', verbose_name='图标大小')
    font_size = models.CharField(max_length=1, default='4', verbose_name='文字大小')
    dock_position = models.CharField(max_length=16, choices=DOCK_POSITION_CHOICES, default="right")
    rank_position = models.CharField(max_length=16, choices=RANK_POSITION_CHOICES, default="center")

    class Meta:
        verbose_name = '用户桌面背景'
        verbose_name_plural = verbose_name
