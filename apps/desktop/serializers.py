from rest_framework.exceptions import ValidationError

from rest_framework import serializers
from apps.desktop.models import InstalledAPP, Folder, Background, Theme
from django.db.transaction import atomic

from server.log import s_logger
from utils.store_server.FileSysInteractor import FileSysInteractor


def get_available_page_seq(user):
    """
    接受用户对象, 返回该用户桌面上第一个可用位置
    :param user: User对象
    :return: 桌面页序号, 该页最小可用位置
    """
    page = 1
    while True:
        app_seqs = InstalledAPP.objects.filter(
            user=user, page=page, folder=None).values_list('seq', flat=True)
        folder_seqs = Folder.objects.filter(
            user=user, page=page).values_list('seq', flat=True)
        seqs = list(app_seqs)
        seqs.extend(list(folder_seqs))
        # 当前页有应用, 检查是否有空位
        if seqs:
            seq_set = set(seqs)
            available_seq = _get_max_seq(seq_set)
            # 有空位则退出循环
            if available_seq is not None:
                break
        # 当前页没有应用, 则返回页序号与第一个位置
        else:
            available_seq = 1
            break
    return page, available_seq


def _get_max_seq(seq_set, max_seq=23):
    """
    接受页数相同的桌面对象, 返回最大序号
    :param seq_set: 已被占用的序号
    :param max_seq: 最大允许序号
    :return: 最大可用序号
    """
    allow_seq = set([i for i in range(0, int(max_seq + 1))])
    available_seq = allow_seq.difference(seq_set)
    max_seq = 0
    if len(available_seq):
        max_seq = list(available_seq)[0]
    return max_seq


class WriteOnceMixin:
    """Adds support for write once fields to serializers.

    To use it, specify a list of fields as `write_once_fields` on the
    serializer's Meta:
    ```
    class Meta:
        model = SomeModel
        fields = '__all__'
        write_once_fields = ('collection', )
    ```

    Now the fields in `write_once_fields` can be set during POST (create),
    but cannot be changed afterwards via PUT or PATCH (update).
    Inspired by http://stackoverflow.com/a/37487134/627411.
    """

    def get_extra_kwargs(self):
        extra_kwargs = super().get_extra_kwargs()

        # We're only interested in PATCH/PUT.
        if 'update' in getattr(self.context.get('view'), 'action', ''):
            return self._set_write_once_fields(extra_kwargs)

        return extra_kwargs

    def _set_write_once_fields(self, extra_kwargs):
        """Set all fields in `Meta.write_once_fields` to read_only."""
        write_once_fields = getattr(self.Meta, 'write_once_fields', None)
        if not write_once_fields:
            return extra_kwargs

        if not isinstance(write_once_fields, (list, tuple)):
            raise TypeError(
                'The `write_once_fields` option must be a list or tuple. '
                'Got {}.'.format(type(write_once_fields).__name__)
            )

        for field_name in write_once_fields:
            kwargs = extra_kwargs.get(field_name, {})
            kwargs['read_only'] = True
            extra_kwargs[field_name] = kwargs

        return extra_kwargs


class InstalledAppSerializer(WriteOnceMixin, serializers.ModelSerializer):
    """用户安装的应用模型序列"""
    name = serializers.CharField(source='application.name', read_only=True)
    english_name = serializers.CharField(
        source='application.english_name', read_only=True)
    # entry_url = serializers.CharField(source='application.entry_url', read_only=True)
    entry_url = serializers.SerializerMethodField()
    logo = serializers.CharField(source='application.get_logo', read_only=True)
    use_iframe = serializers.BooleanField(
        source='application.use_iframe', default=True, help_text='是否使用iframe')
    is_default = serializers.BooleanField(
        source='application.is_default', read_only=True)

    class Meta:
        model = InstalledAPP
        fields = ('id', 'name', 'english_name', 'entry_url', 'logo', 'use_iframe', 'is_default',
                  'application', 'folder', 'page', 'seq')
        write_once_fields = ('application',)

    def get_entry_url(self, instance):
        request = self.context.get('request')
        user = request.user
        entry_url = instance.application.entry_url if user.has_perm(
            "application.view_application", instance.application) or instance.application.is_default \
                                                      is True else None
        return entry_url

    def create(self, validated_data):
        with atomic():
            db_installed_app = InstalledAPP.objects.create(**validated_data)
            user = db_installed_app.user
            page, seq = get_available_page_seq(user)
            db_installed_app.page = page
            db_installed_app.seq = seq
            db_installed_app.save()
        return db_installed_app


class FolderSerializer(serializers.ModelSerializer):
    """文件夹模型序列"""
    installed_app = InstalledAppSerializer(
        many=True, source='installedapp_set', allow_empty=True, read_only=True)
    page = serializers.IntegerField(help_text='桌面页序号', required=False,
                                    write_only=True)
    seq = serializers.IntegerField(help_text='排序序号', required=False,
                                   write_only=True)

    class Meta:
        model = Folder
        fields = ('id', 'name', 'installed_app', 'page', 'seq')

    def create(self, validated_data):
        """
        创建文件夹
        :param validated_data:
        :return:
        """
        with atomic():
            validated_data['name'] = validated_data.get(
                'name', None) or '新建文件夹'
            db_folder = Folder.objects.create(**validated_data)
            # for app in installed_apps:
            #     try:
            #         db_app = InstalledAPP.objects.get(id=app.get('id'), user=db_folder.user)
            #     except ObjectDoesNotExist:
            #         continue
            #     db_app.folder = db_folder
            #     db_app.seq = app.get('seq')
            #     db_app.page = app.get('page')
            #     db_app.save()
        return db_folder


class DeskObjSerializer(serializers.Serializer):
    id = serializers.IntegerField(help_text='id')
    page = serializers.IntegerField(help_text='页序号', )
    seq = serializers.IntegerField(help_text='排序序号', allow_null=True)
    name = serializers.CharField(
        max_length=64, help_text='名称', allow_null=True)


class DeskAppSerializer(DeskObjSerializer):
    english_name = serializers.CharField(
        max_length=64, help_text='英文名称', allow_null=True)
    entry_url = serializers.CharField(
        max_length=256, help_text='应用入口地址', allow_null=True)
    logo = serializers.CharField(max_length=256, help_text='应用图标')
    is_default = serializers.BooleanField(default=False, help_text="是否默认应用")
    use_iframe = serializers.BooleanField(default=True, help_text='是否使用iframe')
    allow_resize = serializers.BooleanField(
        default=True, help_text='是否允许修改iframe大小')

    class Meta:
        fields = ('id', 'page', 'seq', 'name', 'english_name', 'entry_url', 'logo',
                  'use_iframe', 'allow_resize')


class DeskFolderSerializer(DeskObjSerializer):
    logo = serializers.ListField(child=serializers.CharField(max_length=256),
                                 help_text='应用logo列表', allow_empty=True)

    class Meta:
        fields = ('id', 'page', 'seq', 'name', 'logo')


class DesktopSerializer(serializers.Serializer):
    """桌面上对象模型序列器"""
    max_page = serializers.IntegerField(help_text='最大页序号', )
    apps = DeskAppSerializer(many=True, help_text='应用列表', allow_empty=True, )
    folders = DeskFolderSerializer(
        many=True, help_text='文件夹列表', allow_empty=True)

    class Meta:
        fields = ('max_page', 'apps', 'folders')


class BackGroundSerializer(serializers.ModelSerializer):
    """背景模型序列器"""

    class Meta:
        model = Background
        fields = ('id', 'pic', 'thumbnail', 'color', 'is_official', 'owner', 'name')
        read_only_fields = ('is_official', 'owner', 'thumbnail')

    def to_representation(self, instance):
        attribute = super().to_representation(instance)
        attribute['pic'] = instance.return_pic(instance.pic)
        attribute['thumbnail'] = instance.return_pic(instance.thumbnail)
        return attribute

    def create(self, validated_data):
        request = self.context.get('request')
        file_id = request.data.get("id")

        with atomic():
            instance = super().create(validated_data)

            # 根据file_id从数据中心获取文件
            if file_id:
                file_path = FileSysInteractor(request).download_file(file_id)
                instance.pic = file_path
                instance.save()

            # 生成缩略图
            if instance.pic:
                if instance.pic.size > 500 * 1024:
                    raise ValidationError(detail={"detail": "文件超出限制"})
                thumb_name = instance.save_thumb(instance.pic.path)
                instance.thumbnail = "background/{}".format(thumb_name)
                instance.save()

            # 校验背景是否合法
            if not (instance.pic or instance.color):
                raise ValidationError(detail={"detail": "上传背景为空"})

        return instance


class ThemeSerializer(serializers.Serializer):
    """桌面主体序列器"""
    url = serializers.CharField(max_length=128, help_text='背景图路由')
    color = serializers.CharField(max_length=8, help_text='背景颜色')
    logo_size = serializers.CharField(max_length=8, help_text='应用logo大小')
    font_size = serializers.CharField(max_length=8, help_text='字体大小')
    dock_position = serializers.CharField(max_length=16, help_text='应用栏位置')
    rank_position = serializers.CharField(max_length=16, help_text='应用位置')
    is_official = serializers.BooleanField(help_text="是否为官方背景")
    background_id = serializers.CharField(max_length=8, help_text='桌面背景id')

    def to_representation(self, instance):
        instance.url = instance.background.return_pic(instance.background.pic)
        instance.color = instance.background.color
        instance.background_id = instance.background.id
        instance.is_official = instance.background.is_official
        attribute = super().to_representation(instance)
        return attribute
