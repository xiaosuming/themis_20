# Generated by Django 2.2 on 2020-04-27 14:25

import apps.desktop.models
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('desktop', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Background',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(help_text='背景名称', max_length=64, verbose_name='背景名称')),
                ('pic', models.FileField(help_text='背景图片存储路径', max_length=128, null=True, upload_to=apps.desktop.models.background_upload, verbose_name='背景图片存储路径')),
                ('color', models.CharField(max_length=8, null=True, verbose_name='背景颜色')),
            ],
            options={
                'verbose_name': '云桌面背景',
                'verbose_name_plural': '云桌面背景',
            },
        ),
        migrations.CreateModel(
            name='Theme',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('logo_size', models.CharField(max_length=1, verbose_name='图标大小')),
                ('font_size', models.CharField(max_length=1, verbose_name='文字大小')),
                ('background', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='desktop.Background', verbose_name='用户选择的背景')),
                ('user', models.ForeignKey(help_text='用户', on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='用户')),
            ],
            options={
                'verbose_name': '用户桌面背景',
                'verbose_name_plural': '用户桌面背景',
            },
        ),
    ]
