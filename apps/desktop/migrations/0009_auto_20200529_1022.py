# Generated by Django 2.2 on 2020-05-29 10:22

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('desktop', '0008_auto_20200529_1016'),
    ]

    operations = [
        migrations.AddField(
            model_name='theme',
            name='background',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='desktop.Background', verbose_name='用户选择的背景'),
        ),
        migrations.AddField(
            model_name='theme',
            name='user',
            field=models.OneToOneField(help_text='用户', null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='用户'),
        ),
        migrations.CreateModel(
            name='Themes',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('logo_size', models.CharField(default='2', max_length=1, verbose_name='图标大小')),
                ('font_size', models.CharField(default='4', max_length=1, verbose_name='文字大小')),
                ('background', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='desktop.Background', verbose_name='用户选择的背景')),
                ('user', models.OneToOneField(help_text='用户', on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='用户')),
            ],
            options={
                'verbose_name': '用户桌面背景',
                'verbose_name_plural': '用户桌面背景',
            },
        ),
    ]
