import redis
from django.contrib.auth.models import User, Group
from rest_framework.views import APIView
from rest_framework.parsers import FormParser, MultiPartParser
from rest_framework.viewsets import ModelViewSet, mixins, GenericViewSet
from rest_framework.decorators import action
from rest_framework.response import Response
from django.forms.utils import ValidationError
from django.http.response import JsonResponse
from django.contrib.auth import authenticate, models
from django.core.exceptions import ObjectDoesNotExist
from django.utils.decorators import method_decorator
from django.db.models import Q
from oauth2_provider.views.generic import ProtectedResourceView
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi
from guardian.shortcuts import get_users_with_perms

from server import settings
from utils.send_websocket import send_notice
from .utils.tokenid import login, delete_token, check_token, unity_logout
from .utils.jwt_generate import jwt_generate
from .utils.oauth import parse_app
from .serializers import OauthUserSerializer, OauthGroupSerializer
from apps.console.models import AttributeType, AttributeKey, AttributeValue
from apps.oauth.models import OAuthApplication
from server.log import s_logger
import django_rq
import re

# @class LoginView 登录视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	登录视图
#   method:POST
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2020/04/23 18:17:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------
from ..application.models import Application
from ..contact.serializers import save_data, NoticeSerializer


class LoginView(APIView):
    parser_classes = [MultiPartParser, FormParser]

    @swagger_auto_schema(
        tags=['单点登录'], operation_summary='登录',
        manual_parameters=[
            openapi.Parameter('username', openapi.IN_FORM, description='用户名',
                              type=openapi.TYPE_STRING),
            openapi.Parameter('password', openapi.IN_FORM, description='密码',
                              type=openapi.TYPE_STRING),
        ],
        responses={
            200: openapi.Response(
                description='登陆成功', schema=openapi.Schema(
                    type=openapi.TYPE_OBJECT, properties={
                        "tokenid": openapi.Schema(
                            type=openapi.TYPE_STRING, description='全局会话', ),
                    }),
                examples={'tokenid': 'abc'}),
        },
    )
    def post(self, request):
        # 1.获取参数
        username = request.POST.get('username')
        password = request.POST.get('password')

        # 2.校验用户凭证
        user = authenticate(username=username, password=password)
        if user:
            token_id = login(user, request)
        else:
            token_id = None

        # 3.1. 获取jwt
        res = {
            'code': 1,
            'msg': '用户名或密码错误'
        }
        status = 401
        if token_id:
            res = {
                'code': 0,
                'msg': 'SUCCESS',
                'tokenid': token_id,
            }
            status = 200

        # 4. 返回响应
        return JsonResponse(res, status=status)


# @class LogOutView 登出视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	登出视图
#   method:GET
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2020/04/23 18:17:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------
class LogoutView(APIView):
    @swagger_auto_schema(
        tags=['单点登录'], operation_summary='登出',
        responses={200: "登出成功"},
    )
    def get(self, request):
        # 1.获取参数
        token_id = request.headers.get('TOKENID')

        # 2.1.通知子系统登出
        logout_urls = OAuthApplication.objects.all().values_list('logout_uri', flat=True)
        queue = django_rq.get_queue('themis')
        queue.enqueue_call(func=unity_logout, args=(token_id, logout_urls))
        # 2.2.登出
        logout_res = delete_token(token_id)

        # 3.构造响应
        if logout_res:
            res = {
                'code': 0,
                'msg': 'SUCCESS'
            }
            status = 200
        else:
            res = {
                'code': 1,
                'msg': "登出错误，您的登录信息可能已失效"
            }
            status = 500

        # 4. 返回响应
        return JsonResponse(res, status=status)


class TokenIDView(APIView):
    @swagger_auto_schema(
        tags=['单点登录'], operation_summary='校验全局会话',
        responses={
            200: openapi.Response(
                description='校验成功', schema=openapi.Schema(
                    type=openapi.TYPE_OBJECT, properties={
                        "jwt": openapi.Schema(
                            type=openapi.TYPE_STRING, description='java_web_token', ),
                    }),
                examples={'jwt': 'abc'}),
        },
    )
    def get(self, request):
        """校验token_id， 返回jwt"""
        # 1.获取参数
        token_id = request.headers.get('TOKENID')
        client_id = request.GET.get('client_id')
        response_type = request.GET.get('response_type')

        # 2.校验参数
        try:
            db_app = OAuthApplication.objects.get(client_id=client_id)
        except ObjectDoesNotExist:
            raise ValidationError(message='不能识别的client_id', code=400)
        if response_type != 'id_token':
            raise ValidationError(message='不支持的响应类型', code=400)
        # 获取用户id和过期时间
        uid, expires = check_token(token_id)
        if not uid:
            raise ValidationError(message='无法认证的token_id', code=401)
        else:
            try:
                user = models.User.objects.get(id=uid)
            except ObjectDoesNotExist:
                raise ValidationError(message='用户已注销', code=401)

        # 3.生成jwt
        user_info = {
            "uid": uid,
            "username": user.username,
            "expires": expires,
        }
        private_key = db_app.private_key
        with open(private_key, 'rb') as k:
            pk = k.read()
            jwt = jwt_generate(user_info, pk)

        # 4.组织响应
        res = {
            'code': 0,
            'msg': 'SUCCESS',
            'jwt': jwt
        }
        return JsonResponse(res)


@method_decorator(name='list', decorator=swagger_auto_schema(
    tags=['OAuth'], operation_summary='获取有访问权限的用户列表'
))
@method_decorator(name='retrieve', decorator=swagger_auto_schema(
    tags=['OAuth'], operation_summary='更具用户名获取用户详情'
))
class OauthUserViewSet(ProtectedResourceView, mixins.ListModelMixin,
                       mixins.RetrieveModelMixin, GenericViewSet):
    serializer_class = OauthUserSerializer
    lookup_field = 'username'
    lookup_url_kwarg = 'username'
    pagination_class = None

    def get_queryset(self):
        """筛选出有访问权限的用户"""
        # 1.获取访问的应用
        request_obj = self.request
        _, access_token = request_obj.headers._store.get('authorization')[1].split()
        operate_app = parse_app(access_token)

        # 2.获取有访问权限的用户
        user_set = get_users_with_perms(operate_app, only_with_perms_in=['view_application'])
        user_set = user_set.exclude(Q(is_staff=True) | Q(username="AnonymousUser"))
        s_logger.glob.info("{}查询用户".format(operate_app.name))
        s_logger.glob.info("用户-{}".format('-'.join([x.username for x in user_set])))
        if re.search(r"users/\w+", self.request.path):
            user_set = models.User.objects.exclude(Q(is_staff=True) | Q(username="AnonymousUser"))
        # 3.返回用户查询集
        return user_set


class OauthGroupViewSet(ModelViewSet):
    required_scopes = ['groups']
    queryset = models.Group.objects.all()
    serializer_class = OauthGroupSerializer

    # permission_classes = [IsAuthenticated, CanUpdateGroup]

    def perform_create(self, serializer):
        # 1.1获取访问中的access_token
        access_token = self.request.auth
        # 1.2.根据access_token查找应用
        db_app = parse_app(access_token)
        app_name = db_app.name
        # 2.给群组名称添加前缀
        serializer.save(name='{}@{}'.format(app_name, serializer.initial_data.get('name')))

    @action(detail=True, methods=['PUT'], serializer_class=OauthGroupSerializer, )
    def users(self, request, pk):
        # 1.调用对象级别的权限校验
        db_group = self.get_object()
        # 2.获取参数
        user_names = request.data.get('user_names')
        # 3.添加用户至群组
        db_users = models.User.objects.filter(username__in=user_names)
        db_group.user_set.add(db_users)
        # 4.返回响应
        serializer = OauthGroupSerializer(db_group)
        return Response(serializer.data)


# @class RegisterView 注册视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	登录视图
#   method:POST
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2020/07/17 12:02:22|杨伟光|创建类|完成|
# -----------------------------------------------------------------------------------------------------
class RegisterView(APIView):
    parser_classes = [MultiPartParser, FormParser]

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.redis_conn = redis.Redis(
            host=settings.REDIS['HOST'],
            port=settings.REDIS['PORT'],
            password=settings.REDIS.get('PASSWORD'),
            db=settings.REDIS["DB"],
            decode_responses=True,
        )

    @swagger_auto_schema(
        tags=['单点登录'], operation_summary='注册',
        manual_parameters=[
            openapi.Parameter('username', openapi.IN_FORM, description='用户名',
                              type=openapi.TYPE_STRING),
            openapi.Parameter('first_name', openapi.IN_FORM, description='姓名',
                              type=openapi.TYPE_STRING),
            openapi.Parameter('phone', openapi.IN_FORM, description='电话',
                              type=openapi.TYPE_STRING),
            openapi.Parameter('password', openapi.IN_FORM, description='密码',
                              type=openapi.TYPE_STRING), ],
        responses={
            200: openapi.Response(
                description='登陆成功', schema=openapi.Schema(
                    type=openapi.TYPE_OBJECT, properties={
                        "code": openapi.Schema(
                            type=openapi.TYPE_INTEGER, description='响应码'),
                        "msg": openapi.Schema(
                            type=openapi.TYPE_STRING, description='响应信息'),
                    }),
                examples={'code': 0,
                          'msg': 'ACCEPTED'}),
        }, )
    def post(self, request):
        # 1.获取参数
        username = request.POST.get('username')
        first_name = request.POST.get('first_name')
        phone = request.POST.get('phone')
        password = request.POST.get('password')

        # 2.处理用户
        res = {'code': 1}
        status = 400
        # 2.1.校验数据
        if User.objects.filter(username=username).count():
            res.update({'msg': '用户名已存在'})
        else:
            # 2.2.保存用户
            reg_user = User.objects.create_user(
                username=username, first_name=first_name,
                password=password, is_active=False)
            attr_type, _ = AttributeType.objects.get_or_create(name="个人信息")
            attr_key, _ = AttributeKey.objects.get_or_create(key="电话")
            AttributeValue.objects.get_or_create(value=phone, key=attr_key, user=reg_user)

            # 加入缓存
            user_key = "themis_reg_user"
            self.redis_conn.sadd(user_key, reg_user.id)

            # 保存并发送通知
            groups = Group.objects.filter(groupdetail__group_type="管理组")
            manage_users = User.objects.filter(Q(groups__in=groups) | Q(is_staff=True)).distinct()
            for user in manage_users:
                serializer_data = {
                    "text": "\"{}\"向云平台申请账号，用户名为\"{}\"".format(reg_user.first_name,
                                                               reg_user.username),
                    "notice_type": "register",
                    "is_choice": True,
                    "sender": reg_user.id,
                    "receiver": user.id,
                    "application": Application.objects.get(name="控制中心").id
                }
                # 保存通知
                notice_data = save_data(NoticeSerializer, serializer_data)
                # 发送通知
                send_notice(
                    user_id=user.id,
                    data=notice_data,
                )

            # 修改响应参数
            status = 200
            res.update({
                "code": 0,
                "msg": "ACCEPTED"
            })

        # 4. 返回响应
        return JsonResponse(res, status=status)
