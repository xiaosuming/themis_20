# Generated by Django 2.2 on 2020-05-06 10:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('oauth', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='oauthapplication',
            name='logout_uri',
            field=models.CharField(max_length=256, null=True, verbose_name='统一登出接口'),
        ),
    ]
