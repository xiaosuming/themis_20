# Generated by Django 2.2 on 2020-05-12 10:45

from django.conf import settings
import django.contrib.auth.models
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('application', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('auth', '0011_update_proxy_permissions'),
        ('oauth', '0002_oauthapplication_logout_uri'),
    ]

    operations = [
        migrations.CreateModel(
            name='OAuthGroup',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=32, verbose_name='群组名称')),
                ('app', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='application.Application', verbose_name='关联应用')),
                ('creator', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL, verbose_name='创建人')),
                ('permissions', models.ManyToManyField(blank=True, to='auth.Permission', verbose_name='权限')),
                ('users', models.ManyToManyField(related_name='oauth_groups', to=settings.AUTH_USER_MODEL, verbose_name='群成员')),
            ],
            options={
                'verbose_name': '应用群组',
                'verbose_name_plural': '应用群组',
            },
            managers=[
                ('objects', django.contrib.auth.models.GroupManager()),
            ],
        ),
    ]
