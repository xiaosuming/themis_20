from django.urls import path, include, re_path
from .views import LoginView, LogoutView, TokenIDView, OauthUserViewSet, OauthGroupViewSet, RegisterView
from rest_framework import routers

# 群组路由
group_routers = routers.DefaultRouter(trailing_slash=False)
group_routers.register('', OauthGroupViewSet, basename='groups')

# 用户路由
user_routers = routers.DefaultRouter(trailing_slash=False)
user_routers.register('', OauthUserViewSet, basename='users')

urlpatterns = [
    path('login/', LoginView.as_view()),
    path('logout/', LogoutView.as_view()),
    path('register/', RegisterView.as_view()),
    # path('token/', TokenView.as_view()),
    path('authorisation/', TokenIDView.as_view()),
    path('users/', include((user_routers.urls, 'user')), ),
    path('groups/', include((group_routers.urls, 'group')), ),
    path('v1/', include('oauth2_provider.urls', namespace='oauth2_provider')),
]
