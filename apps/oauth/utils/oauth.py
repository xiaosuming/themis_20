from django.core.exceptions import ObjectDoesNotExist
from oauth2_provider.models import AccessToken


def parse_app(access_token):
    """接受access_token, 返回相关应用"""
    try:
        db_token = AccessToken.objects.get(token=access_token)
        operate_app = db_token.application.application
    except ObjectDoesNotExist:
        operate_app = None
    return operate_app
