from django.conf import settings
import rsa
import os


def generate_key_pair(app_id):
    """
    接受应用id
    :param app_id:
    :return:
    """
    # app_dir = os.path.join('/home/konkii/program/auth_server', 'media/application', str(app_id))
    relative_path = os.path.join('media/application', str(app_id))  # 相对路径
    app_dir = os.path.join(settings.BASE_DIR, relative_path)  # 绝对路径
    if not os.path.exists(app_dir):
        os.makedirs(app_dir)

    pub, pri = rsa.newkeys(2048)
    pub_str = pub.save_pkcs1()
    pub_path = os.path.join(app_dir, 'public_key.pem')
    rel_pub_path = os.path.join(relative_path, 'public_key.pem')
    with open(pub_path, 'wb') as p_k:
        p_k.write(pub_str)

    pri_str = pri.save_pkcs1()
    pri_path = os.path.join(app_dir, 'private_key.pem')
    rel_pri_path = os.path.join(relative_path, 'private_key.pem')
    with open(pri_path, 'wb') as p_k:
        p_k.write(pri_str)

    return rel_pub_path, rel_pri_path


def cat_download_path(key_path):
    """
    接受密钥文件路径，返回下载连接
    :param key_path: 私钥路径
    :return: 下载连接
    """
    return "http://192.168.50.114:8000/api/files/?path={}".format(key_path)


if __name__ == '__main__':
    a, b = generate_key_pair(1)
    print(os.path.abspath(a), b)
