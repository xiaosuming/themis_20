import jwt


def jwt_generate(payload, private_key):
    """
    接受字典格式的信息和私钥, 加密出JWT后返回
    :param payload: 字典格式的负载信息
    :param private_key: 私钥
    :return: JWT
    """
    token = jwt.encode(payload, private_key, algorithm='RS256',)
    return token.decode('utf8')


if __name__ == '__main__':
    pay_dict = {
        "uid": 1
    }
    with open('/home/konkii/program/auth_server/apps/oauth/utils/media/application/1/private_key.prm', 'rb') as e:
        e = e.read()
        jwt = jwt_generate(pay_dict, e)

    print(jwt)
