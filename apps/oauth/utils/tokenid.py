from django.conf import settings
from django.contrib.auth.signals import user_logged_in
from rest_framework.exceptions import APIException
from redis.exceptions import ConnectionError
from server.log import s_logger
import redis
import hashlib
import datetime
import requests


CONN = redis.Redis(
            host=settings.REDIS['HOST'],
            port=settings.REDIS['PORT'],
            password=settings.REDIS.get('PASSWORD'),
            db=settings.REDIS["DB"],
            decode_responses=True,
        )


def login(user, request, redis_conn=CONN):
    # 实例化hash生成对象
    hashing = hashlib.new('sha256')
    # 计算token_id
    hashing.update(str(user.id).encode('utf8'))
    now_str = datetime.datetime.now().isoformat().encode('utf8')
    hashing.update(now_str)
    token_id = hashing.hexdigest()
    print("新建tokenid", token_id)
    # 获取当前时间
    now_str, expire_time = cal_expire()
    # 将token_id存入redis
    token = {
        "uid": user.id,
        "expire": expire_time.isoformat()
    }
    try:
        for item in token.items():
            redis_conn.hset(token_id, *item)
    except ConnectionError:
        raise APIException(detail='用户数据库连接失败', code=500)
    redis_conn.expireat(token_id, expire_time)

    # 记录登录状态
    user_logged_in.send(sender=user.__class__, request=request, user=user)

    return token_id


def check_token(token_id, redis_conn=CONN):
    if token_id:
        token = redis_conn.hgetall(token_id)
        uid = token.get('uid')
        expires = redis_conn.ttl(token_id)
        # expire = datetime.datetime.fromisoformat(token.get('expire'))
        # 如果过期时间在30min内, 则延长1小时
        if expires < 60*30:
            expire = expires + 60*30
            redis_conn.expire(token_id, expire)
    else:
        uid, expires = None, None
    return uid, expires


def cal_expire():
    """
    根据设置中的过期时间和当前时间，
    计算当前生成的token过期时间
    """
    now_date_time = datetime.datetime.now()  # 获取当前时间
    now_str = now_date_time.isoformat()  # 当前时间的字符串
    expire_time = now_date_time + datetime.timedelta(seconds=settings.TOKEN_EXPIRES)
    # expire_time = now_date_time + datetime.timedelta(seconds=60*60)  # token过期时间
    return now_str, expire_time


def delete_token(token_id, conn=CONN):
    """
    接受全局会话，从缓存中删除
    :param token_id:
    :param conn: redis连接对象
    :return:
    """
    try:
        conn.delete(token_id)
        res = True
    except Exception as e:
        res = False
        s_logger.glob.error(e)
    return res


def unity_logout(token_id, logout_urls):
    """
    依次访问注册应用的登出接口
    :param token_id: 全局会话
    :param logout_urls: 登出路由
    :return:
    """
    headers = {"TOKENID": token_id}
    for logout_url in logout_urls:
        s_logger.glob.info("登出{}, token_id：\n{}".format(logout_url, token_id))
        try:
            requests.get(logout_url, headers=headers)
        except Exception:
            s_logger.glob.info("登出{}异常：\n{}".format(logout_url, Exception))
            continue
