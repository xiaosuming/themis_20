from django.contrib.auth.models import User, Group
from django.conf import settings
from .models import OAuthApplication
from rest_framework import serializers
from .utils.oauth import parse_app
from utils.sendfile import return_download_uri
from .models import OAuthGroup
import os.path as osp

from ..console.serializers import UserPortrait


class BaseGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = OAuthGroup
        fields = ('id', 'name', 'app', 'creator')

    def to_representation(self, instance):
        # 解析请求的应用
        access_token = self.context['request'].auth
        operate_app = parse_app(access_token)
        if instance:
            attribute = super().to_representation(instance)
            # 当请求的应用不是云桌面时, 裁减掉应用名称
            if operate_app.name != 'themis':
                attribute['name'] = attribute['name'].split('@', 1)[1]
        else:
            attribute = instance
        return attribute


class BaseUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'email', 'first_name', 'last_name',)


class OauthGroupSerializer(BaseGroupSerializer):
    users = BaseUserSerializer(source='user_set', many=True, read_only=True, help_text='群组中的用户')

    class Meta:
        model = OAuthGroup
        fields = ('id', 'name', 'creator', 'users')

    def create(self, validated_data):
        access_token = self.context['request'].auth
        operate_app = parse_app(access_token)
        ab_oauth_group = OAuthGroup.objects.create(app=operate_app, **validated_data)
        return ab_oauth_group


class OauthUserSerializer(BaseUserSerializer):
    # groups = BaseGroupSerializer(many=True, read_only=True, help_text='用户所在的群组')
    # group_ids = serializers.ListField(
    #     write_only=True, required=False, help_text='用户加入的群组', allow_empty=True,
    #     child=serializers.IntegerField(help_text='群组id'))
    password = serializers.CharField(max_length=32, help_text='密码', allow_null=True,
                                     write_only=True)
    portrait = UserPortrait(source='user_icon', allow_null=True, read_only=True)

    class Meta:
        model = User
        fields = ('username', 'password', 'email', 'first_name', 'last_name', 'groups',
                  'is_superuser', 'is_staff', 'is_active', "portrait")

    def create(self, validated_data):
        print(validated_data['password'])
        db_user = User.objects.create_user(**validated_data)
        return db_user


class OAuthAppSerializer(serializers.ModelSerializer):
    application = serializers.CharField(source='application.name')

    class Meta:
        model = OAuthApplication
        fields = (
            'id', 'application', 'client_id', 'client_secret', 'client_type',
            'authorization_grant_type', 'skip_authorization', 'public_key', 'private_key',
            'redirect_uris', 'logout_uri')
        read_only_fields = (
            'application_name', 'public_key', 'private_key', 'client_id', 'client_secret')

    def to_representation(self, instance):
        user = self.context['request'].user
        attribute = super().to_representation(instance)
        attribute['public_key'] = return_download_uri(osp.join(settings.BASE_DIR, instance.public_key))
        if user == instance.application.developer:
            attribute['private_key'] = return_download_uri(osp.join(settings.BASE_DIR, instance.private_key))
        else:
            attribute['private_key'] = None
        return attribute
