from django.db import models
from django.conf import settings
from django.contrib.auth.models import User, Permission, GroupManager
from apps.application.models import Application
from oauth2_provider.models import AbstractApplication
import os
import shutil


class OAuthApplication(AbstractApplication):
    application = models.OneToOneField(to=Application, on_delete=models.CASCADE,
                                       verbose_name='关联应用')
    skip_authorization = models.BooleanField(default=True)
    public_key = models.CharField(max_length=128, verbose_name='公钥路径', null=True)
    private_key = models.CharField(max_length=128, verbose_name='私钥路径', null=True)
    logout_uri = models.CharField(max_length=256, verbose_name='统一登出接口', null=True)

    class Meta:
        verbose_name = 'OAuth注册应用'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name

    def return_dir(self):
        """
        返回应用专用数据存储路进
        :return: 路径
        """
        return os.path.join(settings.MEDIA_ROOT, 'application', str(self.id))

    def delete(self, using=None, keep_parents=False):
        """删除应用时，同时删除相关媒体文件"""
        shutil.rmtree(self.return_dir(), ignore_errors=True)
        super(OAuthApplication, self).delete()


class OAuthGroup(models.Model):
    name = models.CharField(max_length=32, verbose_name='群组名称')
    app = models.ForeignKey(to=Application, on_delete=models.CASCADE, verbose_name='关联应用')
    creator = models.ForeignKey(to=User, null=True, on_delete=models.SET_NULL, verbose_name='创建人')
    users = models.ManyToManyField(to=User, related_name='oauth_groups', verbose_name='群成员')
    permissions = models.ManyToManyField(Permission, verbose_name='权限', blank=True)

    objects = GroupManager()

    class Meta:
        verbose_name = '应用群组'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name

    def natural_key(self):
        return self.name,
