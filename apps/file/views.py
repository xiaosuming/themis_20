import hashlib
import shutil
import time

from django.http import HttpResponse
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.exceptions import NotFound, ValidationError
from sendfile import sendfile
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi
import os

# @class SendFileView 发送文件视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	发送文件视图
#   method:GET
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2020/04/13 11:17:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------
from utils.store_server.FileSysInteractor import FileSysInteractor


class SendFileView(APIView):
    @swagger_auto_schema(tags=['文件'], operation_summary='获取文件',
                         manual_parameters=[
                             openapi.Parameter('path', openapi.IN_QUERY, description='文件路径',
                                               type=openapi.TYPE_INTEGER)])
    def get(self, request):
        # 1.获取数据
        path = request.GET.get('path')

        # 2.参数校验
        if os.path.exists(path):
            res = sendfile(request, path, attachment=True)
            return res
        else:
            raise NotFound(detail="请求的资源不存在")


# @class UploadFileView 分片上传文件视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	发送文件视图
#   method:POST
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2020/06/22 17:27:22|杨伟光|创建类|完成|
# -----------------------------------------------------------------------------------------------------
class UploadFileView(APIView):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    @swagger_auto_schema(tags=['文件'], operation_summary='文件切片上传',
                         request_body=openapi.Schema(
                             type=openapi.TYPE_OBJECT, description="分片上传文件",
                             properties={'file': openapi.Schema(type=openapi.TYPE_FILE, description='分片文件'),
                                         'resumableChunkNumber': openapi.Schema(type=openapi.TYPE_STRING,
                                                                                description='分片序号'),
                                         'resumableIdentifier': openapi.Schema(type=openapi.TYPE_STRING,
                                                                               description='文件唯一标识')}))
    def post(self, request):
        # 1.读取数据
        stream = request.data.get("file").file.read()
        seq = request.data.get("resumableChunkNumber")
        uid = request.data.get("resumableIdentifier")

        # 2.参数校验
        if uid is None or uid == '':
            raise ValidationError(detail=u"参数uid缺失")
        if seq is None:
            raise ValidationError(detail=u"序号缺失")

        # 3.记录数据
        path = os.path.join("media/temp/", str(uid))  # 存入临时文件夹 /media/temp/AE123DFC4543
        if not os.path.exists(path):
            os.makedirs(path, exist_ok=True)

        filepath = os.path.join(path, str(seq))  # 临时文件 /media/temp/AE123DFC4543/1

        fp = open(filepath, 'wb+')
        fp.write(stream)
        fp.close()

        # 4.返回应答
        return Response(status=status.HTTP_200_OK)


# @class FileMergeView 文件和片视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	发送文件视图
#   method:POST
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2020/06/22 17:27:22|杨伟光|创建类|完成|
# -----------------------------------------------------------------------------------------------------
class FileMergeView(APIView):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    @swagger_auto_schema(tags=['文件'], operation_summary='文件和片',
                         request_body=openapi.Schema(
                             type=openapi.TYPE_OBJECT, description="分片上传文件",
                             properties={'type': openapi.Schema(type=openapi.TYPE_STRING, description='上传文件用途'),
                                         'filelist': openapi.Schema(type=openapi.TYPE_ARRAY,
                                                                    items=openapi.Schema(
                                                                        type=openapi.TYPE_OBJECT, description="文件信息",
                                                                        properties={
                                                                            "name": openapi.Schema(
                                                                                type=openapi.TYPE_STRING,
                                                                                description='文件名称'),
                                                                            "uid": openapi.Schema(
                                                                                type=openapi.TYPE_STRING,
                                                                                description='文件标识符'),
                                                                            "totalchunk": openapi.Schema(
                                                                                type=openapi.TYPE_STRING,
                                                                                description='分片数量'),

                                                                        }
                                                                    ), description='文件列表[]，格式为字符串')}))
    def post(self, request):
        # 1.获取参数
        file_type = request.data.get('type')
        file_list = request.data.get('filelist', '[]')

        # 2.校验参数
        file_list = eval(file_list)
        data = enclosurepro(file_list, file_type)

        # 3.返回应答
        return Response(data=data)


# 文件和片处理
def enclosurepro(file_list, save_dir):
    """
    合并上传的文件
    :param file_list: 文件切片信息列表
    :param save_dir: 存储路径
    :return:
    """
    if not file_list:
        raise ValidationError(detail=u'上传文件为空')

    enclosures = []
    for item in file_list:
        name = item.get('name')
        uid = item.get('uid')
        total_chunk = item.get('totalchunk')
        total_chunk = int(total_chunk)

        if name is None or uid is None:
            raise ValidationError(detail=u'切片信息缺失')

        code = hashlib.md5(str(time.time()).encode("utf-8")).hexdigest()  # e97e317e5a8c75f
        if '.' in name:
            save_name = code + '.' + name.rsplit('.', 1)[1]
        else:
            save_name = code

        save_path = os.path.join('media', save_dir)  # media/expjson/12/
        if not os.path.exists(save_path):
            # 创建文件夹
            os.makedirs(save_path)

        file_path = os.path.join(save_path, save_name)  # media/expjson/12/e97e317e5a8c75f
        tmpdir = os.path.join('media/temp', str(uid))  # 切片文件临时存储路径：media/temp/uid
        if not os.path.exists(tmpdir):
            raise ValidationError(detail=u'%s上传失败' % name)

        size = 0
        # 读取切片文件存入合成文件
        with open(file_path, 'wb+') as fp:
            for i in range(1, total_chunk + 1):
                # 切片文件路径
                tmp_path = os.path.join(tmpdir, str(i))  # 切片文件： media/temp/uid/1
                # 文件是否存在
                if not os.path.join(tmp_path):
                    continue
                file = open(tmp_path, 'rb').read()
                fp.write(file)
                size += len(file)

        # 删除切片文件临时文件夹
        shutil.rmtree(tmpdir)

        # 组织数据
        enclosure = {
            "name": name.rsplit('.', 1)[0],
            "path": file_path
        }
        enclosures.append(enclosure)

    return {"enclosures": enclosures}
