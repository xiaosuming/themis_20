from django.urls import path
from apps.file.views import SendFileView, UploadFileView, FileMergeView

urlpatterns = [
    path('', SendFileView.as_view()),
    path('upload', UploadFileView.as_view()),
    path('merge', FileMergeView.as_view()),
]
