from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from sendfile import sendfile
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi
import os


# @class KubeMetricView 集群监控响应视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	发送文件视图
#   method:GET
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2020/06/18 09:32:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------

class KubeMetricView(APIView):
    def get(self, request):
        return Response(status=status.HTTP_200_OK)
