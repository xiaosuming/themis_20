from rest_framework.views import APIView
from rest_framework.exceptions import NotFound
from sendfile import sendfile
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi
import os


# @class SendFileView 发送文件视图
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	发送文件视图
#   method:GET
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2020/04/13 11:17:22|张志鹏|创建类|完成|
# -----------------------------------------------------------------------------------------------------

class SendFileView(APIView):
    @swagger_auto_schema(tags=['文件'], operation_summary='获取文件',
                         manual_parameters=[
                             openapi.Parameter('path', openapi.IN_QUERY, description='文件路径',
                                               type=openapi.TYPE_INTEGER)])
    def get(self, request):
        # 1.获取数据
        path = request.GET.get('path')

        # 2.参数校验
        if os.path.exists(path):
            res = sendfile(request, path, attachment=True)
            return res
        else:
            raise NotFound(detail="请求的资源不存在")
