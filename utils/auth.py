import base64
import requests


def _generate_credential(raw_cred_str):
    """
    接受原始凭证字符串，生成基本字节数组
    :param raw_cred_str:
    :return:
    """
    bytes_array = raw_cred_str.encode('utf8')
    b64_array = base64.b64encode(bytes_array)
    return b64_array


# def create_user()


if __name__ == '__main__':
    def request_access_token():
        r_c_s = "f184d7b2-5cc8-4ebf-87dd-4505f665f01a:w0xxbl4XvJq1xU8Bwx5tCEDceAoGX80qRvbgqO6J"
        auth_credential = _generate_credential(r_c_s).decode('utf8')
        auth_string = "Basic {}".format(auth_credential)
        print(auth_string)
        token = requests.post(url="https://auth.konkii.com/oxauth/restv1/token",
                              verify=False,
                              headers={
                                  'Authorization': auth_string,
                                  'Content-Type': 'application/x-www-form-urlencoded'
                              },
                              data={'grant_type': 'client_credentials'})
        print(token.json() if token.status_code == 200 else token.content.decode('utf8'))
