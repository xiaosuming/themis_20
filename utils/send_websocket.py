from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer

from server.log import s_logger


def send_notice(user_id, data, notify_type='notification'):
    """
    发送通知
    :param user_id: 用户id
    :param data: 通知数据
    :param notify_type: 通知类型
    :return:
    """
    # s_logger.glob.info("发送websocket：{}".format(str(user_id)))
    channel_layer = get_channel_layer()
    async_to_sync(channel_layer.group_send)(
        "themis_notification_%s" % user_id,
        {
            'type': notify_type,
            'data': data
        })
