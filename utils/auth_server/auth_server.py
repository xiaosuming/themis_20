from django.conf import settings
from django.forms.utils import ValidationError
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
import requests
import os
import jwt
import redis


# AUTH_HOST = settings.HOSTS.get("AUTH")

SSO_CONF = settings.SSO_CONF
AUTH_ROUTER = SSO_CONF.get('ROUTER')
AUTH_CRED = SSO_CONF.get('CREDENTIAL')


# def fetch_url(path_type):
#     """获取需要的请求路径"""
#     router = AUTH_ROUTER.get(path_type)
#     return os.path.join(AUTH_HOST, router)
#
#
# def redis_conn():
#     conn = redis.Redis(
#         host=settings.REDIS['HOST'],
#         port=settings.REDIS['PORT'],
#         password=settings.REDIS['PASSWORD'],
#         db=settings.REDIS["DB"],
#         decode_responses=True,
#     )
#     return conn
#
#
# def login(**user_credential):
#     """
#     向认证服务器发起登录请求, 获取全局会话
#     :param user_credential: 用户凭证
#     :return:
#     """
#     # 组织登录路径
#     url = fetch_url('LOGIN')
#     # 发起登录请求
#     res = requests.post(url=url, data=user_credential)
#     if res.status_code == 200:
#         res_data = res.json()
#         token_id = res_data.get('token_id')
#     else:
#         token_id = None
#     return token_id
#
#
# def check_token_id(token_id):
#     """
#     接受全局会话, 请求认证服务器, 获取jwt
#     :param token_id:
#     :return:
#     """
#     # 构建请求参数
#     client_id = AUTH_CRED.get('CLIENT_ID')
#     response_type = 'id_token'
#     url = fetch_url("AUTHORISATION")
#     headers = {'TOKENID': token_id}
#     # 请求jwt
#     res = requests.get(
#         url=url,  headers=headers,
#         params={"client_id": client_id, "response_type": response_type},)
#     if res.status_code == 200:
#         res_data = res.json()
#         java_w_t = res_data.get('jwt')
#     else:
#         java_w_t = None
#     return java_w_t
#
#
# def _read_pub_key():
#     pub_key_path = AUTH_CRED.get('PUBLIC_KEY')
#     with open(pub_key_path, 'rb') as k:
#         pub_key = k.read()
#     return pub_key
#
#
# def _parse_jwt(java_web_t, pub_key):
#     try:
#         payload = jwt.decode(java_web_t, pub_key)
#     except ValueError:
#         raise ValidationError('jwt失效')
#     uid = payload.get('uid')
#     username = payload.get('username')
#     expires = payload.get('expires') - 60*60  # 全局会话过期时间减去1小时
#     return uid, username, expires
#
#
# def _mapping_jwt(java_web_t, uid, expires):
#     conn = redis_conn()
#     # 在redis中记录jwt与用户id的映射关系
#     conn.set(java_web_t, uid)
#     conn.expire(java_web_t, expires)
#
#
# def map_user_with_jwt(jave_w_t):
#     """根据收到的jwt, 返回用户对象"""
#     # 读取全局会话
#     pub_key = _read_pub_key()
#     # 解析jwt
#     _, username, expires = _parse_jwt(jave_w_t, pub_key)
#     # 创建用户
#     try:
#         db_user = User.objects.get(username=username)
#     except ObjectDoesNotExist:
#         db_user = User.objects.create_user(username=username)
#     # 将jwt作为局部会话, 映射用户id
#     _mapping_jwt(jave_w_t, db_user.id, expires)
#     # 返回用户对象
#     return db_user
#
#
# def logout(token_id, java_w_t):
#     """
#     接受全局会话, 登出用户
#     :param token_id: 全局会话
#     :param java_w_t: 局部会话
#     :return:
#     """
#     url = fetch_url('LOGOUT')
#     headers = {'TOKENID': token_id}
#     res = requests.get(url, headers=headers)
#     if res.status_code == 200:
#         res = True
#         if java_w_t:
#             conn = redis_conn()
#             conn.delete(java_w_t)
#     else:
#         res = False
#     return res


def parse_res(response):
    """接受响应"""
    if response.status_code in range(200, 210):
        res = response.json()
        error = None
    else:
        error = response.content.decode('utf8')
        res = None
    return res, error


# def create_user(token_id, authorization, username, password, **kwargs):
#     """
#     接受全局会话, 应用认证凭证, 用户信息, 创建用户
#     :param token_id: 全局会话
#     :param authorization: 应用认证凭证, 通常是Access Token
#     :param username: 用户名
#     :param password: 密码
#     :param kwargs: 用户其他属性
#     :return:
#     """
#     url = fetch_url("USER")
#     headers = {"TOKENID": token_id, "Authentication": authorization}
#     data = {"username": username, "password": password, **kwargs}
#     res = requests.post(url=url, headers=headers, data=data)
#     res, error = parse_res(res)
#     return res, error


# def create_group(token_id, authorization, group_name):
#     """
#     接受全局会话, 应用认证凭证, 群组名称, 创建群组
#     :param token_id: 全局会话
#     :param authorization: 应用认证凭证, 通常是Access Token
#     :param group_name: 群组名称
#     :return:
#     """
#     url = fetch_url("GROUPS")
#     headers = {"TOKENID": token_id, "Authentication": authorization}
#     data = {"name": group_name}
#     res = requests.post(url=url, headers=headers, data=data)
#     res, error = parse_res(res)
#     return res, error


# class Application:
#     def __init__(self, token_id, auth):
#         self.token_id = token_id
#         self.auth = auth
#         self.url = fetch_url("APPLICATIONS")
#
#     def create_application(self, **app_kwargs):
#         """
#         接受应用信息, 创建应用
#         :param app_kwargs: 应用信息
#         :return: 新建应用的信息
#         """
#         headers = {"TOKENID": self.token_id}
#         res = requests.post(self.url, headers=headers, data=app_kwargs)
#         res, error = parse_res(res)
#         return res, error
#
#     def applications(self):
#         """请求所有应用信息"""
#         headers = {"TOKENID": self.token_id}
#         res = requests.get(self.url, headers=headers)
#         res, error = parse_res(res)
#         return res, error


# def allow_access(token_id, access_token, username, groups):
#     """
#     授予用户应用访问权限
#     :param token_id: 全局会话
#     :param access_token: 访问令牌
#     :param username: 用户名
#     :param groups: 群组参数字典  {"to_add_groups":[ "themis@users"], "to_rm_groups":[]}
#     :return:
#     """
#     url = os.path.join(fetch_url("USER"), username, 'groups')
#     headers = {'TOKENID': token_id, "Authorization": access_token}
#     res = requests.patch(url, headers=headers, data=groups)
#     res, error = parse_res(res)
#     return res, error
