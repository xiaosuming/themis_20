from django.conf import settings
# from utils.auth_server.auth_server import fetch_url, parse_res
import redis
import requests


# @class OperateToken OauthToken操作
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	操作token
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2020/04/27 15:50:22|张志鹏|创建模块|完成|
# -----------------------------------------------------------------------------------------------------


# 构造redis连接对象
REDIS_CONN = redis.Redis(
        host=settings.REDIS['HOST'],
        port=settings.REDIS['PORT'],
        password=settings.REDIS.get('PASSWORD'),
        db=settings.REDIS["DB"],
        decode_responses=True,
    )
# 单点登录配置
SSO_CONF = settings.SSO_CONF
AUTH_ROUTER = SSO_CONF.get('ROUTER')
AUTH_CRED = SSO_CONF.get('CREDENTIAL')


def get_access_token(token_id, redis_conn=REDIS_CONN):
    """
    接受全局会话和redis连接, 尝试获取access_token
    :param token_id:
    :param redis_conn:
    :return:
    """
    conn = redis_conn
    # 在redis中获取access_token
    access_token = conn.get(AUTH_CRED["ACCESS_TOKEN_KEY"])
    error = None
    # 缓存中没有access_token
    if not access_token:
        # 从缓存中获取refresh_token
        refresh_token = conn.get(AUTH_CRED["REFRESH_TOKEN_KEY"])
        # 缓存中有刷新令牌时, 使用刷新令牌请求访问令牌
        if refresh_token:
            access_token, error = fresh_token(refresh_token, token_id, conn)
        # 刷新令牌实效，重新请求访问令牌
        else:
            access_token, error = request_token(token_id)
    return access_token, error


def fresh_token(refresh_token, token_id, conn=REDIS_CONN):
    """
    接受刷新令牌和redis连接, 以及全局会话
    :param refresh_token: refresh_token字符串
    :param conn: redis连接对象
    :param token_id: 全局会话
    :return:access_token
    """
    # 组织请求参数
    query_data = {
        "refresh_token": refresh_token,
        "client_id": AUTH_CRED["CLIENT_ID"],
        "client_secret": AUTH_ROUTER["CLIENT_SECRET"],
        "grant_type": "refresh_token"
    }
    query_headers = {"TOKENID": token_id}
    # 请求新的access_token
    token_data = requests.post(AUTH_ROUTER["TOKEN_URI"],
                               data=query_data, headers=query_headers)
    if token_data.status_code == 200:
        token_dict = token_data.json()
        # redis缓存访问令牌
        access_token = operate_token(token_dict, conn)
        error = None
    else:
        access_token = None
        error = token_data.content.decode('utf8')
    return access_token


def operate_token(token_dict, conn=REDIS_CONN):
    """
    接受一个token字典，存入缓存并返回access_token
    :param token_dict: {"access_token": "...", "expires_in": ...,
            "token_type": "...", "scope": ".. .. ..", "refresh_token": "..."}
    :param conn:
    :return:
    """
    # 解析字典中的访问令牌/过期时间/令牌类型/刷新令牌
    access_token = token_dict["access_token"]
    expires_in = token_dict["expires_in"]
    token_type = token_dict["token_type"]
    refresh_token = token_dict["refresh_token"]
    # scope = token_dict["scope"]
    # 拼接访问令牌类型与令牌作为之后的认证请求头
    access_token_stored = "{} {}".format(token_type, access_token)
    # 缓存访问令牌
    conn.set(AUTH_CRED["ACCESS_TOKEN_KEY"], access_token_stored)
    # 设置过期时间
    conn.expire(AUTH_CRED["ACCESS_TOKEN_KEY"], expires_in)
    # 缓存刷新令牌
    conn.set(AUTH_CRED["REFRESH_TOKEN_KEY"], refresh_token)
    # 返回可直接作为认证字段的访问令牌 Bearer aBcDeFg
    return access_token_stored


# def request_token(token_id):
#     """
#     请求认证服务器获取token, 存入缓存, 并返回认证请求头格式的token
#     :return: access_token, error
#     """
#     # 构造请求参数
#     query_params = {
#         "response_type": "code",
#         "client_id": AUTH_CRED["CLIENT_ID"]
#     }
#     # 构造请求头
#     headers = {"TOKENID": token_id}
#     # 请求临时授权码, 授权服务器会重定向到本服务器的redirect_uri
#     token_res = requests.get(fetch_url("OAUTH_AUTH"), params=query_params,
#                              headers=headers)
#
#     # 若请求成功, 本服务会自动请求access_token,
#     # 并调用operate_token存入缓存返回认证请求头合适的格式
#     if token_res == 200:
#         res_dict = token_res.json()
#         # 解析access_token
#         if res_dict["code"] == 0:
#             access_token = res_dict["data"]
#             error = None
#         # 解析错误信息
#         else:
#             access_token = None
#             error = res_dict['detail']
#     else:
#         access_token = None
#         error = token_res.content.decode('utf8')
#     return access_token, error


# def remove_access_token(token_id, conn=REDIS_CONN):
#     """删除缓存中的访问令牌/刷新令牌, 请求并存储新的令牌"""
#     conn.delete(AUTH_CRED["ACCESS_TOKEN_KEY"])
#     conn.delete(AUTH_CRED["REFRESH_TOKEN_KEY"])
#     return request_token(token_id)


# def exchange_token(code):
#     """
#     接受临时授权码, 交换访问令牌
#     :param code: 认证服务器返回的临时授权码
#     :return:
#     """
#     url = fetch_url("TOKEN")
#     # 构造请求参数
#     post_data = {
#         "grant_type": "authorization_code",
#         "code": code,
#         "client_id": AUTH_CRED.get('CLIENT_ID'),
#         "client_secret": AUTH_CRED.get('CLIENT_SECRET'),
#         "redirect_uri": AUTH_CRED.get("REDIRECT_URI")
#     }
#     # 请求访问令牌
#     res = requests.post(url, data=post_data)
#     # 解析响应
#     res, error = parse_res(res)
#     return res, error
