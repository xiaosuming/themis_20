import hashlib
import json
import os
from functools import partial
from io import BytesIO

import requests
from rest_framework.exceptions import ValidationError

from server import settings
from server.settings import BASE_DIR

url_file = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                        "router.json")
with open(url_file, 'r') as f:
    URLData = json.load(f)


# @class FileSysInteractor 数据中心交互类
# @details
# -----------------------------------------------------------------------------------------------------
# - 功能描述：
# 	日程视图
#   method:
# -----------------------------------------------------------------------------------------------------
# - 编辑历史:
# |时间|编辑者|说明|状态|
# |----:|:----:|:----:|:----|
# |2020/08/11 09:15:22|杨伟光|创建类|完成|
# -----------------------------------------------------------------------------------------------------


class FileSysInteractor:
    """
    Interact with FileSystem
    """

    def __init__(self, request):
        self.user = request.user
        self.URLData = URLData
        self.headers = {
            "TOKENID": request.headers.get("TOKENID", None)
        }

    def file_download(self, file_id):
        """
        下载数据中心文件
        :param file_id: 文件类型
        :return:
        """
        file_url = os.path.join(settings.HOSTS["STOR"], URLData["FILE_DOWNLOAD"])
        params = {
            "file_id": file_id
        }
        res_data = requests.get(
            url=file_url,
            headers=self.headers,
            params=params
        )
        if res_data.status_code == 200:
            return res_data
        else:
            raise ValidationError(detail={"detail": "请求数据中心出错"})

    def download_file(self, file_id):
        """
        接受文件id,返回文件保存路径
        :param file_id: 文件id
        :return:
        """
        # 1.组织并发送请求
        req_url = os.path.join(settings.HOSTS["STOR"], URLData["FILE_DOWNLOAD"])
        params = {"file_id": file_id}
        res = requests.get(req_url, headers=self.headers, params=params)

        # 2.处理响应
        if str(res.status_code).startswith('2'):
            dst_path = self.save_path('background', file_id, res.content)
            abs_path = os.path.join(BASE_DIR, "media/{}".format(dst_path))
            with open(abs_path, 'wb+') as tfp:
                tfp.write(res.content)
        else:
            raise ValidationError(detail={"detail": "加载数据中心文件出错"})

        return dst_path

    def get_file_name(self, file_id):
        """
        接受数据中心文件id, 返回文件名
        :param file_id: 文件id
        :return:
        """
        # 1.组织并发送请求
        req_url = os.path.join(settings.HOSTS["STOR"], URLData["FILE_DETAILS"])
        params = {"file_id": file_id}
        res = requests.get(req_url, headers=self.headers, params=params)

        # 2.处理响应
        if res.status_code == 200:
            res_data = res.json().get("data")

            file_name = res_data.get("file_name", 'road')
            file_size = res_data.get("file_size", 0)
            if file_size > 500 * 1024:
                raise ValidationError(detail={"detail": "文件超出限制"})

            return file_name
        else:
            raise ValidationError(detail={"detail": "加载数据中心文件名出错"})

    def save_path(self, folder, file_id, file):
        """
        接受数据中心文件id, 返回文件名
        :param folder: 保存文件夹路径
        :param file_id: 文件id
        :param file: 二进制文件
        :return:
        """
        # 1.从数据中心获取文件名
        file_name = self.get_file_name(file_id)

        # 2.生成文件路径
        pic_ext = file_name.rsplit('.')[1]
        hasher = hashlib.md5()
        hasher.update(file)
        pic_path = os.path.join(folder, '{}.{}'.format(hasher.hexdigest(), pic_ext))

        return pic_path
