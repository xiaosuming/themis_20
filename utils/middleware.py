import datetime
import json

from django.utils.deprecation import MiddlewareMixin
from django.conf import settings
from django.contrib.auth.models import User
from django.http.response import JsonResponse
# from apps.oauth.utils.oauth import check_jwt
# from utils.auth_server.auth_server import check_token_id, map_user_with_jwt
from apps.oauth.utils.tokenid import check_token
import logging
import traceback
import re

DEBUG = settings.DEBUG


class GrantUserMiddleware(MiddlewareMixin):
    """调试阶段授予请求虚拟用户"""

    def process_request(self, request):
        print(User.objects.all())
        user_obj = User.objects.get(username='admin')
        request.user = user_obj


class DisableCSRFCheck(MiddlewareMixin):
    def process_request(selfself, request):
        setattr(request, '_dont_enforce_csrf_checks', True)


class PrettyExceptionMiddleWare(MiddlewareMixin):
    """以JSON格式返回异常，并追溯错误代码"""

    def process_exception(self, request, exception):
        if DEBUG:
            traceback.print_exc()
        else:
            logger = logging.getLogger(__name__)
            logger.error(traceback.format_exc())

        if hasattr(exception, 'code'):
            data = {
                'code': 1,
                'msg': 'ERROR',
                'details': str(exception.error)
            }
            return JsonResponse(
                status=exception.code,
                data=data,
            )
        else:
            data = {
                'code': 1,
                'msg': 'ERROR',
                'details': str(exception)
            }
            return JsonResponse(
                status=500,
                data=data,
            )


# class CheckJWTTokenMiddleware(MiddlewareMixin):
#     def process_request(self, request):
#         """校验局部会话的中间件"""
#         # 1. 获取局部会话
#         jwt = request.headers.get('JWT')
#         # 2. 如果有局部会话则在缓存中查找映射的用户
#         if jwt:
#             uid = check_jwt(jwt)
#             if uid:
#                 db_user = User.objects.get(id=uid)
#                 request.user = db_user


# class CheckTokenIdMiddleware(MiddlewareMixin):
#     def process_request(self, request):
#         """
#         当没有有效的局部会话时, 检查全局会话
#         """
#         # 如果没有有效的全局会话, 则校验全局会话
#         if not request.user.is_authenticated:
#             token_id = request.headers.get('TOKENID')
#             jwt = check_token_id(token_id)
#             # 如果返回了有效的jwt
#             if jwt:
#                 db_user = map_user_with_jwt(jwt)
#                 request.user = db_user
#                 request.COOKIES["SET_JWT"] = jwt
#
#     def process_response(self, request, response):
#         # 当请求体中有设置局部会话的参数, 则在响应体中添加局部会话
#         jwt = request.COOKIES.get('SET_JWT')
#         if jwt and response.status_code == 200:
#             response.__setitem__('SET_JWT', jwt)
#         return response


class TokenIDMiddleware(MiddlewareMixin):
    def process_request(self, request):
        token_id = request.headers.get('TOKENID')
        if token_id:
            uid, _ = check_token(token_id)
            if uid:
                db_user = User.objects.get(id=int(uid))
                request.user = db_user


class LoginCheckMiddleware(MiddlewareMixin):
    def process_request(self, request):
        """
        当没有有效的局部会话时, 检查全局会话
        """
        # 用户没有认证且不是访问登录接口
        to_login = re.search('login', request.path)
        to_doc = re.search('swagger', request.path)
        get_file = re.search('files', request.path)
        download_user = re.search('download', request.path) and request.method == "GET"
        to_admin = re.search('admin', request.path)
        to_register = re.search("register", request.path)
        kube_metric = re.search('health$', request.path)

        if to_doc:
            admin_user = User.objects.get(username='admin')
            request.user = admin_user
        elif (not request.user.is_authenticated) and not (to_login or get_file or to_admin
                                                          or kube_metric or to_register or download_user):
            res = {
                'code': 1,
                'msg': '没有有效身份信息'
            }
            return JsonResponse(res, status=401)
        else:
            pass


class ResponseHandle(MiddlewareMixin):
    def process_response(self, request, response):
        """
        统一错误响应格式
        """
        if response.status_code == 400 and re.findall(r"console", request.path):
            raw_response = json.loads(response.content)
            new_res_content = raw_response

            if isinstance(raw_response, list):
                new_res_content = {"detail": raw_response[0]}
            elif isinstance(raw_response, dict):
                if "detail" not in raw_response.keys():
                    for key in raw_response.keys():
                        new_res_content = {"detail": raw_response[key][0]}
                        break

            response.content = json.dumps(new_res_content)

        return response
