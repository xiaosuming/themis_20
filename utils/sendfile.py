from django.conf import settings
import os


def return_download_uri(path):
    uri = os.path.join(settings.HOSTS["FRONTEND"], "api/files", "?path={}".format(path))
    return uri
