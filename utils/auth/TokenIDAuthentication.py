from django.conf import settings
import redis


class Authentication:
    conn = redis.Redis(
        host=settings.REDIS['HOST'],
        port=settings.REDIS['PORT'],
        password=settings.REDIS.get('PASSWORD'),
        db=settings.REDIS["DB"],
        decode_responses=True,
    )

    def get_user(self, request):
        token_id = request.headers.get('TOKENID')

