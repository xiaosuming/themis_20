from server.log import s_logger
from datetime import datetime
import json
import os.path as osp

_BASE_DIR = osp.dirname(osp.dirname(__file__))


class Configuration:
    DATABASE = None
    REDIS = None
    HOSTS = None
    SENDFILE_BACKEND = None
    SSO_CONF = None

    def __init__(self):
        """
        初始化配置文件以及配置项
        """
        self.costum_config = osp.join(_BASE_DIR, 'configuration',
                                      'config.json')
        self.general_config = osp.join(_BASE_DIR, 'configuration',
                                       'general.json')
        self.config()

    def load(self):
        """读取配置文件，返回配置内容"""
        with open(self.costum_config, 'rb') as cf:
            configuration = json.load(cf)
            with open(self.general_config, 'rb') as gf:
                general_conf = json.load(gf)
            configuration.update(general_conf)
        return configuration

    def config(self):
        """解析配置内容, 设置可用配置项"""
        configuration = self.load()
        # 数据库配置
        self.DATABASE = {
            'default': {
                'ENGINE': 'django.db.backends.mysql',
            }
        }
        self.DATABASE['default'].update(configuration.get('DATABASE').get('MYSQL'))

        self.REDIS = configuration.get('REDIS')
        self.FRONTEND = configuration.get('FRONTEND')
        self.HOSTS = configuration.get('HOSTS')
        self.HOSTS.update({'FRONTEND': self.FRONTEND})
        self.DEBUG = configuration.get("DEBUG")
        self.SENDFILE_BACKEND = configuration.get("SENDFILE_BACKEND") or \
                                'sendfile.backends.development'
        self.SSO_CONF = configuration.get('SSO')

        if self.REDIS.get('PASSWORD'):
            channel_layer_host = "redis://:{}@{}:{}/{}".format(
                self.REDIS['PASSWORD'], self.REDIS["HOST"], self.REDIS["PORT"],
                self.REDIS["DB"])
        else:
            self.REDIS.update({'PASSWORD': None})
            channel_layer_host = "redis://{}:{}/{}".format(
                self.REDIS["HOST"], self.REDIS["PORT"], self.REDIS["DB"])
        self.CHANNEL_LAYERS = {
            "default": {
                "BACKEND": "channels_redis.core.RedisChannelLayer",
                "CONFIG": {
                    "hosts": [channel_layer_host, ]
                }
            }
        }
