aioredis==1.3.1
appdirs==1.4.3
asgiref==3.2.7
async-timeout==3.0.1
attrs==19.3.0
autobahn==20.4.3
Automat==20.2.0
CacheControl==0.12.6
certifi==2020.4.5.1
cffi==1.14.0
channels==2.2.0
channels-redis==2.4.0
chardet==3.0.4
click==7.1.2
colorama==0.4.3
constantly==15.1.0
contextlib2==0.6.0
coreapi==2.3.3
coreschema==0.0.4
cryptography==2.9
daphne==2.5.0
distlib==0.3.0
distro==1.4.0
Django==2.2
django-cors-headers==3.2.1
django-filter==2.2.0
django-guardian==2.2.0
django-oauth-toolkit==1.3.2
django-rq==2.3.1
django-sendfile==0.3.11
djangorestframework==3.11.0
djangorestframework-csv==2.1.0
drf-yasg==1.17.1
ecdsa==0.15
hiredis==1.1.0
html5lib==1.0.1
hyperlink==19.0.0
idna==2.9
incremental==17.5.0
inflection==0.4.0
ipaddr==2.2.0
itypes==1.1.0
Jinja2==2.11.1
lockfile==0.12.2
MarkupSafe==1.1.1
msgpack==0.6.2
mysqlclient==1.4.6
oauthlib==3.1.0
packaging==20.3
pep517==0.8.2
Pillow==7.2.0
progress==1.5
pyasn1==0.4.8
pyasn1-modules==0.2.8
pycparser==2.20
pycrypto==2.6.1
PyHamcrest==2.0.2
PyJWT==1.7.1
pyOpenSSL==19.1.0
pyparsing==2.4.7
python-logstash==0.4.6
pytoml==0.1.21
pytz==2019.3
redis==3.4.1
requests==2.23.0
retrying==1.3.3
rq==1.3.0
rsa==4.0
ruamel.yaml==0.16.10
ruamel.yaml.clib==0.2.0
service-identity==18.1.0
six==1.14.0
sqlparse==0.3.1
toml==0.10.1
Twisted==20.3.0
txaio==20.4.1
unicodecsv==0.14.1
uritemplate==3.0.1
urllib3==1.25.8
#uWSGI==2.0.18
webencodings==0.5.1
zope.interface==5.1.0
